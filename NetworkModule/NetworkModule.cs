﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using SerializablePackets;
using SerializablePackets.CommandType;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace ClientNetworkModule
{
    class NetworkModule : INetworkModule
    {
        TcpClient _clientSocket;
        SslStream _sslStream;
        private static Hashtable certificateErrors = new Hashtable();

        public NetworkModule()
        {
            ManageCertificate();
        }

        /// The following method is invoked by the RemoteCertificateValidationDelegate. 
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool result = false;

            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                result = true;
                // Allows the client to communicate with this servers if there is no error. 
            }
            else
            {
                Console.WriteLine("Certificate error: {0}", sslPolicyErrors);
            }
            return result;
        }



        /// <summary>
        /// This function inits the clientSocket and create the connection with the server
        /// </summary>
        /// <param name="IPAddress">is the IPaddress of the server</param>
        /// <param name="port">is the port of the server</param>
        /// <param name="serverName">is the name of the server</param>
        /// <param name="CertificateName">is the client Trusted Root CA used to accept or refuse the server's authenticitie</param>
        /// <returns></returns>
        public bool InitSocket(IPAddress IPAddress, int port, String serverName, String CertificateName)
        {
            _clientSocket = new TcpClient();
            _clientSocket.Connect(IPAddress, port);

            _sslStream = new SslStream(_clientSocket.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);


            try
            {
                _sslStream.AuthenticateAsClient(serverName);
            }

            catch (AuthenticationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                _clientSocket.Close();
                return false;
            }
            return _clientSocket.Connected;

        }

        public ACommand Receive()
        {
            SslStream stream = null;
            ACommand receivedCommand = new ACommand();

            while (_clientSocket.Available == 0) ;
            if ((null != _clientSocket) && (true == _clientSocket.Connected))
            {

                if (_clientSocket.Available > 0)
                {
                    stream = _sslStream;
                    IFormatter formatter = new BinaryFormatter();
                    receivedCommand = (ACommand)formatter.Deserialize(stream);
                }
            }
            return receivedCommand;
        }

        private bool Send(ACommand packet)
        {
            SslStream stream = null;

            IFormatter myforma = new BinaryFormatter();

            stream = _sslStream;

            myforma.Serialize(stream, packet);
            Console.WriteLine("Writing to the server :" + _clientSocket.GetHashCode());
            return (true);
        }

        /// <summary>
        /// This function is used by the client when he wants to etablish a connection with the server
        /// </summary>
        /// <param name="Username">is the username of the client</param>
        /// <param name="Password">is the password of the client. The Password is never send to the server</param>
        /// <returns>returns a Result class. This result class contains a boolean who determines the result of the process.
        /// In case of a fail, it returns the reason.</returns>
        public Result StartAuthentificationProtocol(String Username, String Password)
        {
            Result result = new Result();
            result.result = false;
            result.reason = "";

            SendUser(Username);

            ACommand Saltcommand = Receive();

            Salts salts = (Salts)Saltcommand;

            if (salts.Result == true)
            {
                SendAuthentificationToken(salts.AccountSalt, salts.SessionSalt, Username, Password);
                ACommand ResultCommand = Receive();
                result = (Result)ResultCommand;
            }
            else
            {
                result.reason = "Wrong Username";
            }
            return result;
        }

        /// <summary>
        /// This function sends the username of the client to the server.
        /// The Server send back the password's salts linked with the username and the current server's session.
        /// </summary>
        /// <param name="Username">is the username of the client</param>
        private void SendUser(String Username)
        {
            SslStream stream = null;
            AskForAuthentification User = new AskForAuthentification(Username);

            IFormatter myforma = new BinaryFormatter();

            stream = _sslStream;
            myforma.Serialize(stream, User);
            Console.WriteLine("Writing to the server :" + _clientSocket.GetHashCode());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="salt"></param>
        /// <param name="SessionSalt">is the current salt generated when the client's socket etablished a connexion with the server</param>
        /// <param name="Username">is the Username of the client</param>
        /// <param name="Password">is the password of the client</param>
        private void SendAuthentificationToken(String salt, String SessionSalt, String Username, String Password)
        {
            SslStream stream = null;
            AuthentificationToken User = new AuthentificationToken(Username, false);
            SHA1 sha = new SHA1CryptoServiceProvider();

            String tmpHashedPassword = Password;
            byte[] byteArrayPassword = System.Text.Encoding.Default.GetBytes(tmpHashedPassword);
            byte[] byteArrayHashedPassword = sha.ComputeHash(byteArrayPassword);
            tmpHashedPassword = BitConverter.ToString(byteArrayHashedPassword).Replace("-", "");

            tmpHashedPassword += salt;
            byte[] accountSaltedPassword = System.Text.Encoding.Default.GetBytes(tmpHashedPassword);
            byte[] accountHashedSaltedPassword = sha.ComputeHash(accountSaltedPassword);

            tmpHashedPassword = BitConverter.ToString(accountHashedSaltedPassword).Replace("-", "");

            tmpHashedPassword += SessionSalt;
            byte[] bASessionSaltPassword = System.Text.Encoding.Default.GetBytes(tmpHashedPassword);
            byte[] hashData3 = sha.ComputeHash(bASessionSaltPassword);
            User._token = BitConverter.ToString(hashData3).Replace("-", "");

            IFormatter myforma = new BinaryFormatter();
            stream = _sslStream;
            myforma.Serialize(stream, User);
        }
        public void UpdateUserData(String Username, String LastName, String Password)
        {
            UpdateUserData Command = new UpdateUserData();
            Command.userName = Username;
            Command.lastName = LastName;
            Command.password = Password;

            Send(Command);
        }

        public void Close()
        {
            _clientSocket.Close();
        }

        private void ManageCertificate()
        {
            bool isCertificateFound = false;
            X509Store store = new X509Store(StoreName.Root, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadWrite);

            X509Certificate2Collection collection = new X509Certificate2Collection();
            X509Certificate2Collection storecollection = (X509Certificate2Collection)store.Certificates;

            foreach (X509Certificate2 x509 in storecollection)
            {
                if (x509.Subject == "CN=FakeServerName")
                {
                    isCertificateFound = true;
                }
            }

            if (false == isCertificateFound)
            {
                X509Certificate2 certificate = new X509Certificate2("RootCATest.cer");
                collection.Add(certificate);
                store.AddRange(collection);
                Console.WriteLine("The certificate as been added " + certificate.Subject);
            }
            else
            {
                Console.WriteLine("Ok");
            }
            store.Close();
        }
    }
}
