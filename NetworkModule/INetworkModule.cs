﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using SerializablePackets;
using SerializablePackets.CommandType;


namespace ClientNetworkModule
{
    interface INetworkModule
    {

        bool InitSocket(IPAddress IPAddress, int port, String serverName, String CertificateName);

        ACommand Receive();
        // bool Send(ACommand packet);


        Result StartAuthentificationProtocol(String Username, String Password);
        void UpdateUserData(String Username, String LastName, String Password);


        void Close();
    }
}
