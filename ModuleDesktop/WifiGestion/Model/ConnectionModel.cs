﻿using NativeWifi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ModuleDesktop.WifiGestion.Model
{
    class ConnectionModel
    {


        public Wlan.Dot11AuthAlgorithm Security;

        public BitmapImage SignalImg;

        public BitmapImage VSignalImg
        {
            get { return this.SignalImg; }
            set
            {
                this.SignalImg = value;
                //this.OnPropertyChanged("Company");
            }
        }

        public string VStatus
        {
            get { string ret = ""; if (IsConnected) ret += "Connecté, "; if (Security == Wlan.Dot11AuthAlgorithm.IEEE80211_Open) ret += "Ouvert"; else ret += "Sécurisé";  return ret;   }
        }

        public string Name;
        public string VName
        {
            get { if (this.Name == "") return "Réseau masqué"; return this.Name; }
            set
            {
                this.Name = value;
                //this.OnPropertyChanged("Company");
            }
        }
        public string SSIDstring;
        public string VSSIDstring
        {
            get { return this.SSIDstring; }
            set
            {
                this.SSIDstring = value;
                //this.OnPropertyChanged("Company");
            }
        }
        public string ProfileName;
        public string VProfileName
        {
            get { return this.ProfileName; }
            set
            {
                this.ProfileName = value;
                //this.OnPropertyChanged("Company");
            }
        }
        public uint wlanSignalQuality;
        public uint VwlanSignalQuality
        {
            get { return this.wlanSignalQuality; }
            set
            {
                this.wlanSignalQuality = value;
                //this.OnPropertyChanged("Company");
            }
        }
        public bool IsConnected;
        public string VIsConnected
        {
            get { return IsConnected ? "Oublier" : "Connect"; }
            set
            {
                this.IsConnected = true;
                //this.OnPropertyChanged("Company");
            }
        }
        public byte[] SSID;
        public byte[] VSSID
        {
            get { return this.SSID; }
            set
            {
                this.SSID = value;
                //this.OnPropertyChanged("Company");
            }
        }
        public  string ProfileXML;
        public string VProfileXML
        {
            get { return this.ProfileXML; }
            set
            {
                this.ProfileXML = value;
                //this.OnPropertyChanged("Company");
            }
        }
        public bool IsRemembered;
        public bool VIsRemembered
        {
            get { return this.IsRemembered; }
            set
            {
                this.IsRemembered = value;
                //this.OnPropertyChanged("Company");
            }
        }

        public WlanClient.WlanInterface CurrentWlan;

      public   ConnectionModel(String _SSIDString, String _ProfileName, uint _WlanSignalQuality, bool _IsConnected, byte[] _SSID, String _ProfileXML, bool _IsRemember, WlanClient.WlanInterface _CurrentWlan, string _Name, Wlan.Dot11AuthAlgorithm _Security, uint ByteArrayLenght)
        {
            SSIDstring = _SSIDString;
            ProfileName = _ProfileName;
            wlanSignalQuality = _WlanSignalQuality;
            IsConnected = _IsConnected;
            ProfileXML = _ProfileXML;
            IsRemembered = _IsRemember;
            CurrentWlan = _CurrentWlan;
            Name = _Name;
            Security = _Security;
            byte[] tmp = new byte[ByteArrayLenght];
            Array.Copy(_SSID, tmp, ByteArrayLenght);
            SSID = tmp;
            var assembly = Assembly.GetExecutingAssembly();
            if (VwlanSignalQuality <= 30 && VwlanSignalQuality >= 0)
            {
                var imageStream = assembly.GetManifestResourceStream(
                    "ModuleDesktop.WifiGestion.Images.wifi-signal-low-512.png");
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = imageStream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();

                SignalImg = bitmap;
            }
            else if (VwlanSignalQuality <= 60 && VwlanSignalQuality >= 30)
            {
                var imageStream = assembly.GetManifestResourceStream(
                    "ModuleDesktop.WifiGestion.Images.wifi-signal-medium-512.png");
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = imageStream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();

                SignalImg = bitmap;
            }
            else
            {
                var imageStream = assembly.GetManifestResourceStream(
                    "ModuleDesktop.WifiGestion.Images.wifi-signal-high-512.png");
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = imageStream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();

                SignalImg = bitmap;
            }
        }

        public ConnectionModel(String _SSIDString, String _ProfileName, uint _WlanSignalQuality, bool _IsConnected, byte[] _SSID, String _ProfileXML)
        {

        }
    }
}
