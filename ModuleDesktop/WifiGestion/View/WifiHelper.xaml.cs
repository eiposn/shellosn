﻿using CommonModule.BaseTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NativeWifi;
using ModuleDesktop.WifiGestion.Model;
using System.Collections.ObjectModel;
using System.Net.NetworkInformation;

namespace ModuleDesktop.View
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class WifiHelper : WorkspaceViewBase
    {

        List<string> Ssid;
        ObservableCollection<ConnectionModel> AvaliableConnection;
        public WifiHelper()
        {
            InitializeComponent();
            Ssid = new List<string>();
            AvaliableConnection = new ObservableCollection<ConnectionModel>();
            PopulateSSid();
           // SSID.ItemsSource = Ssid;
           SSID.ItemsSource = AvaliableConnection;
            Loaded += MyWindow_Loaded;
            InitTimer();
        }

        void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            VisualTreatment();
        }

        void VisualTreatment()
        {
            foreach (object lvi in SSID.Items)
            {
               /* ListViewItem lvit = (ListViewItem)SSID.ItemContainerGenerator.ContainerFromItem(lvi);
                TextBlock tb = FindByName("PASSWORD", lvit) as TextBlock;
                tb.Visibility = Visibility.Collapsed;*/
            }
        }

        private System.Windows.Forms.Timer timer1;
        public void InitTimer()
        {
                timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 2000; // in miliseconds
            timer1.Start();
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (SSID.SelectedItem == null)
            {
                PopulateSSid();
            }
        }

        private void PopulateSSid()
        {
                AvaliableConnection.Clear();

            try
            {
                WlanClient client = new WlanClient();
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {

                wlanIface.WlanConnectionNotification -= wlanConnectionChangeHandler;
                wlanIface.WlanConnectionNotification += wlanConnectionChangeHandler;
                // Lists all networks
                Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(0);
                foreach (Wlan.WlanAvailableNetwork network in networks)
                {

                    if (network.dot11DefaultAuthAlgorithm == Wlan.Dot11AuthAlgorithm.IEEE80211_Open ||  network.dot11DefaultAuthAlgorithm == Wlan.Dot11AuthAlgorithm.RSNA_PSK)
                    {
                        ConnectionModel model = new ConnectionModel(Convert.ToBase64String(network.dot11Ssid.SSID), network.profileName, network.wlanSignalQuality, network.flags.HasFlag(Wlan.WlanAvailableNetworkFlags.Connected), network.dot11Ssid.SSID, network.flags.HasFlag(Wlan.WlanAvailableNetworkFlags.HasProfile)? wlanIface.GetProfileXml(network.profileName) : "", network.flags == Wlan.WlanAvailableNetworkFlags.HasProfile? true:false, wlanIface, GetStringForSSID(network.dot11Ssid), network.dot11DefaultAuthAlgorithm, network.dot11Ssid.SSIDLength);
                        AvaliableConnection.Add(model);
                   Ssid.Add(GetStringForSSID(network.dot11Ssid));
                    }
                }
                }
            }
                        catch
            {
            }

        }


        static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            foreach (object lvi in SSID.Items)
            {
                ListViewItem lvit = (ListViewItem)SSID.ItemContainerGenerator.ContainerFromItem(lvi);
                Grid tb = FindByName("MDP", lvit) as Grid;
                tb.Visibility = Visibility.Collapsed;
            }
            if (item != null && item.IsSelected)
            {
                object o = SSID.SelectedItem;
                ListViewItem lvi = (ListViewItem)SSID.ItemContainerGenerator.ContainerFromItem(o);
                Grid tb = FindByName("MDP", lvi) as Grid;
                tb.Visibility = Visibility.Visible;
            }
        }

        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);

            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop();
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int i = 0; i < count; ++i)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, i);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }

            return null;
        }

        private void ConnectWPA(ConnectionModel SelectedConnection, string Passwd)
        {
            string profileName = SelectedConnection.ProfileName;
            if (profileName == "")
            {
                profileName = SelectedConnection.Name;
                SelectedConnection.ProfileName = profileName;
            }
            string key = Passwd;

            string ssidHex = BitConverter.ToString(SelectedConnection.SSID);
            ssidHex = ssidHex.Replace("-", "");

            string profile = string.Format("<?xml version =\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\"><name>{0}</name><SSIDConfig><SSID><hex>{1}</hex><name>{0}</name></SSID></SSIDConfig><connectionType>ESS</connectionType><connectionMode>auto</connectionMode><MSM><security><authEncryption><authentication>WPA2PSK</authentication><encryption>AES</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>false</protected><keyMaterial>{2}</keyMaterial></sharedKey></security></MSM><MacRandomization xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v3\"><enableRandomization>false</enableRandomization></MacRandomization></WLANProfile>",
                    profileName, ssidHex, key);
            SelectedConnection.ProfileXML = profile;
            SelectedConnection.CurrentWlan.SetProfile(Wlan.WlanProfileFlags.AllUser, SelectedConnection.ProfileXML, true);
            SelectedConnection.CurrentWlan.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, SelectedConnection.ProfileName);
        }

        private void ConnectWEP(ConnectionModel SelectedConnection)
        {
            string profileName = SelectedConnection.ProfileName;
            if (profileName == "")
            {
                profileName = SelectedConnection.Name;
                SelectedConnection.ProfileName = profileName;
            }
            string key = "IsThisPasswordField?";

            string ssidHex = BitConverter.ToString(SelectedConnection.SSID);
            ssidHex = ssidHex.Replace("-", "");

            string profile = string.Format("<?xml version=\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\"><name>{0}</name><SSIDConfig><SSID><hex>{1}</hex><name>{0}</name></SSID></SSIDConfig><connectionType>ESS</connectionType><MSM><security><authEncryption><authentication>open</authentication><encryption>WEP</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>networkKey</keyType><protected>false</protected><keyMaterial>{2}</keyMaterial></sharedKey><keyIndex>0</keyIndex></security></MSM></WLANProfile>",
                profileName, ssidHex, key);
            SelectedConnection.ProfileXML = profile;
            SelectedConnection.CurrentWlan.SetProfile(Wlan.WlanProfileFlags.AllUser, SelectedConnection.ProfileXML, true);
            SelectedConnection.CurrentWlan.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, SelectedConnection.ProfileName);
        }

        private void ConnectOpen(ConnectionModel SelectedConnection)
        {
            string profileName = SelectedConnection.ProfileName;
            if (profileName == "")
            {
                profileName = SelectedConnection.Name;
                SelectedConnection.ProfileName = profileName;
            }
            string ssidHex = BitConverter.ToString(SelectedConnection.SSID);
            ssidHex = ssidHex.Replace("-", "");
            string profile = string.Format("<?xml version =\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\"><name>{0}</name><SSIDConfig><SSID><hex>{1}</hex><name>Ionis Portal</name></SSID>\r\n\t</SSIDConfig><connectionType>ESS</connectionType><connectionMode>manual</connectionMode><MSM><security><authEncryption><authentication>open</authentication><encryption>none</encryption><useOneX>false</useOneX></authEncryption></security></MSM><MacRandomization xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v3\"><enableRandomization>false</enableRandomization></MacRandomization></WLANProfile>", profileName, ssidHex);
            SelectedConnection.ProfileXML = profile;
            SelectedConnection.CurrentWlan.SetProfile(Wlan.WlanProfileFlags.AllUser, SelectedConnection.ProfileXML, true);
            SelectedConnection.CurrentWlan.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, SelectedConnection.ProfileName);
        }

        public void wlanConnectionChangeHandler(Wlan.WlanNotificationData notifyData, Wlan.WlanConnectionNotificationData connNotifyData)
        {
            string msg = String.Empty;

            switch (notifyData.notificationSource)
            {
                case Wlan.WlanNotificationSource.ACM:

                    switch ((Wlan.WlanNotificationCodeAcm)notifyData.notificationCode)
                    {

                        case Wlan.WlanNotificationCodeAcm.ConnectionAttemptFail:
                            msg = "La clef de sécurité reseau est incorrect" + notifyData.notificationCode;
                            MessageBox.Show(msg + " pour la connection:" + connNotifyData.profileName);
                            break;

                    }
                    //MessageBox.Show(notifyData.notificationCode.ToString() + " pour la connection:" + connNotifyData.profileName);
                    break;

                default:
                    //MessageBox.Show("irrelevant notification. Ignore");
                    break;
            }
        }


        WlanClient.WlanInterface.WlanConnectionNotificationEventHandler ConnectionNotification()
        {

            var client = new WlanClient();
            foreach (var wifiInterface in client.Interfaces)
            {
                Console.WriteLine("{0} {1}", wifiInterface.InterfaceName,
                                             wifiInterface.InterfaceState);
            }
            
            return null;
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {

            ConnectionModel SelectedConnection = SSID.SelectedItem as ConnectionModel;
            ListViewItem lvit = (ListViewItem)SSID.ItemContainerGenerator.ContainerFromItem(SelectedConnection);
            TextBox tb = FindByName("PASSWORD", lvit) as TextBox;
            if (SelectedConnection.IsConnected == false)
            {
                if (SelectedConnection.IsRemembered == true)
                {
                    SelectedConnection.CurrentWlan.SetProfile(Wlan.WlanProfileFlags.AllUser, SelectedConnection.ProfileXML, true);
                    SelectedConnection.CurrentWlan.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, SelectedConnection.ProfileName);
                }
                else
                {
                    switch (SelectedConnection.Security)
                    {
                        case Wlan.Dot11AuthAlgorithm.IEEE80211_Open:
                            ConnectOpen(SelectedConnection);
                            break;
                        case Wlan.Dot11AuthAlgorithm.RSNA_PSK:
                            ConnectWPA(SelectedConnection, tb.Text);
                           break;
                            
                    }
                }
            }
            else
            {
                SelectedConnection.CurrentWlan.SetProfile(Wlan.WlanProfileFlags.AllUser, SelectedConnection.ProfileXML, true);
                SelectedConnection.CurrentWlan.DeleteProfile(SelectedConnection.ProfileName);
            }
            PopulateSSid();
        }
    }
}
