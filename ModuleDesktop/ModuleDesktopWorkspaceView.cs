﻿using System;
using Microsoft.Practices.Prism.Logging;
using Microsoft.Practices.Prism.Modularity;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Practices.Prism.Regions;
using ModuleDesktop;
using Microsoft.Practices.Unity;
using System.Configuration;
using ModuleDesktop.View;

namespace OSN
{
    public class ModuleDesktopWorkspaceView : IModule
    {
        private IModuleManager moduleManager;
        private IModuleCatalog catalog;
        private IUnityContainer container;
        private readonly IRegionManager regionManager;
        private TaskBarRegistry taskBarRegistry;

        public ModuleDesktopWorkspaceView(IModuleManager _moduleManager, IModuleCatalog _catalog, IRegionManager _regionManager, IUnityContainer _container, TaskBarRegistry _TaskBarRegistry)
         {
             this.moduleManager = _moduleManager;
             this.catalog = _catalog;
             this.regionManager = _regionManager;
             this.container = _container;
             this.taskBarRegistry = _TaskBarRegistry;
         }

        public void Initialize()
        {

            string ribbonRegionName = ConfigurationManager.AppSettings["RibbonRegionName"];
            string helperRegionName = ConfigurationManager.AppSettings["HelperRegionName"];
            string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];

            regionManager.Regions[workspaceRegionName].Add(new DesktopView(catalog, moduleManager, regionManager, taskBarRegistry), Settings.Default.WorkspaceViewName);

            regionManager.Regions[helperRegionName].Add(new WifiHelper(), Settings.Default.HelperViewName);


            var moduleAView = new Uri("MusicView", UriKind.Relative);
        }


    }
}
