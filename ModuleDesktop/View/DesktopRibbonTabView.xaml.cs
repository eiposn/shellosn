﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Ribbon;
using CommonModule.BaseTypes;

namespace ModuleDesktop.View
{
    /// <summary>
    /// Logique d'interaction pour DesktopRibbonTabView.xaml
    /// </summary>
    public partial class DesktopRibbonTabView : RibbonTabViewBase
    {
        
        public DesktopRibbonTabView()
        {
            _ViewName = Settings.Default.RibbonTabViewName;
            _PairedWorkspaceViewName = Settings.Default.WorkspaceViewName;

            InitializeComponent();
        }
    }
}
