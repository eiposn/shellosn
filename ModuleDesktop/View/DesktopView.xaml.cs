﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using CommonModule.BaseTypes;
using OSN;
using System.Drawing;
using System.Reflection;

namespace ModuleDesktop
{
    /// <summary>
    /// Logique d'interaction pour DesktopView.xaml
    /// </summary>
    public partial class DesktopView : WorkspaceViewBase
    {
        IEnumerable<ModuleInfo> modules;
        IModuleManager moduleManager;
        IRegionManager regionManager;
        IUnityContainer container;
        List<ModuleInform> moduleInform;

        private void populateModuleinform()
        {
            foreach (ModuleInfo cur in modules)
            {
                if (cur.ModuleName != "ModuleDesktopWorkspaceView")
                {
                    string imgPath = cur.Ref;
                    imgPath = System.IO.Path.ChangeExtension(imgPath, "png");
                    Console.WriteLine(imgPath);
                    moduleInform.Add(new ModuleInform("", cur.ModuleName, imgPath));
                }
            }
        }

        public DesktopView(IModuleCatalog _catalog, IModuleManager _moduleManager, IRegionManager _regionManager, TaskBarRegistry _TaskBarRegistry)
        {
            this.moduleInform = new List<ModuleInform>();
            this.modules = _catalog.Modules;
            this.moduleManager = _moduleManager;
            InitializeComponent();
            Modules.ItemsSource = moduleInform;
            this.regionManager = _regionManager;
            Console.WriteLine((AppDomain.CurrentDomain.BaseDirectory) + "Desktop.png");
            _TaskBarRegistry.RegisterMenuItem(Settings.Default.WorkspaceViewName, "Desktop", (AppDomain.CurrentDomain.BaseDirectory) + "Desktop.png");
            populateModuleinform();
            
        }

        private void OpenModule(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (Modules.SelectedItem != null)
            {
                ModuleInform select = (ModuleInform)Modules.SelectedItem;
                foreach (ModuleInfo cur in modules)
                { 
                    if (cur.ModuleName == select.ModuleName)
                        if (cur.State == ModuleState.Initialized)
                        {
                            regionManager.RequestNavigate("b", select.Uri);
                            return;
                        }
                }
                moduleManager.LoadModule(select.ModuleName);
            }
        }      
    }
}
