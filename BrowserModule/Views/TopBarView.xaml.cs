﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrowserModule.Views
{
    /// <summary>
    /// Interaction logic for TopBarView.xaml
    /// </summary>
    public partial class TopBarView : UserControl
    {
        private string _url;
        public string url { get { return _url; } set { _url = value; input_url.Text = value; } }

        public event RoutedEventHandler GotoPage;
        public event RoutedEventHandler GotoNewTab;
        public event RoutedEventHandler GotoHistory;
        public event RoutedEventHandler GotoFavorite;
        public event RoutedEventHandler GotoAddFavorite;
        

        public TopBarView()
        {
            InitializeComponent();
        }

        private void Validate_url(RoutedEventArgs e)
        {
            this.url = this.input_url.Text;
            GotoPage(this, e);
        }

        private void Button_Click_Go(object sender, RoutedEventArgs e)
        {
            Validate_url(e);
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Validate_url(e);
            }
        }

        private void Button_Click_NewTab(object sender, RoutedEventArgs e)
        {
            GotoNewTab(this, e);
        }

        private void Button_Click_History(object sender, RoutedEventArgs e)
        {
            GotoHistory(this, e);
        }

        private void Button_Click_Favorite(object sender, RoutedEventArgs e)
        {
            GotoFavorite(this, e);
        }

        private void Button_Click_AddFavorite(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(url))
            GotoAddFavorite(this, e);
        }

    }
}
