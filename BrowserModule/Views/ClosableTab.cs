﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrowserModule.Views;
using System.Windows.Controls;
using System.Windows;

namespace BrowserModule
{
    enum ItemType { BrowserTab, HistoryTab, FavoryTab, SettingsTab };
    class ClosableTab : TabItem
    {
        

        public ItemType itemtype;
        public ClosableTab(ItemType itemtype)
        {
            this.itemtype = itemtype;
            // Create an instance of the usercontrol
            CloseableHeader closableTabHeader = new CloseableHeader();
            // Assign the usercontrol to the tab header
            this.Header = closableTabHeader;

            closableTabHeader.label_TabTitle.SizeChanged += new SizeChangedEventHandler(label_TabTitle_SizeChanged);
            closableTabHeader.button_close.Click += new RoutedEventHandler(button_close_Click);
        }

        public string Title
        {
            set
            {
                ((CloseableHeader)this.Header).label_TabTitle.Content = value;
            }
        }

        void button_close_Click(object sender, RoutedEventArgs e)
        {
            ((TabControl)this.Parent).Items.Remove(this);
        }

        void label_TabTitle_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ((CloseableHeader)this.Header).button_close.Margin = new Thickness(((CloseableHeader)this.Header).label_TabTitle.ActualWidth + 5, 3, 4, 0);
        }
    }
}
