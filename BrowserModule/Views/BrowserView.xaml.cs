﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NhibernateORM;

namespace BrowserModule.Views
{
    /// <summary>
    /// Interaction logic for BrowserView.xaml
    /// </summary>
    /// 
    public partial class BrowserView : UserControl
    {
        WebBrowser currentBrowser;
        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
        public string home;
         public NHinerbanteDatabaseHandler BrowserDAO { get; set; }   

        public BrowserView()
        {
            InitializeComponent();
            currentBrowser = InitialBrowser;
            BrowserDAO = NHinerbanteDatabaseHandler.getInstance();
            NavigateToHome();
        }
        
        private void LeftBar_GotoHome(object sender, RoutedEventArgs e)
        {
            NavigateToHome();
        }

        private void NavigateToHome()
        {
            BrowserSettings homePage = BrowserDAO.GetHome();
            home = homePage.Value;
            if (currentBrowser != null)
            {
                if (String.IsNullOrEmpty(home)) return;
                if (home.Equals("about:blank")) return;
                if (!home.StartsWith("http://") &&
                    !home.StartsWith("https://"))
                {
                    home = "http://" + home;
                }
                try
                {
                    currentBrowser.Navigate(new Uri(home));
                }
                catch (System.UriFormatException)
                {
                    return;
                }
            }
        }
        private void LeftBar_ActualizePage(object sender, RoutedEventArgs e)
        {
            if (currentBrowser != null)
            {
                if (currentBrowser.Source != null)
                    currentBrowser.Refresh();
            }
        }

        private void DeleteTab(object sender, RoutedEventArgs e)
        {
            ((TabControl)this.Parent).Items.Remove(this);
            currentBrowser = InitialBrowser;
        }

        private void WebPage_Completed(object sender, NavigationEventArgs e)
        {
            if (currentBrowser != null && currentBrowser.Document != null)
            {
                IServiceProvider serviceProvider = (IServiceProvider)currentBrowser.Document;
                Guid serviceGuid = SID_SWebBrowserApp;
                Guid iid = typeof(SHDocVw.IWebBrowser2).GUID;
                SHDocVw.IWebBrowser2 myWebBrowser2 = (SHDocVw.IWebBrowser2)serviceProvider.QueryService(ref serviceGuid, ref iid);
                SHDocVw.DWebBrowserEvents_Event wbEvents = (SHDocVw.DWebBrowserEvents_Event)myWebBrowser2;
                wbEvents.NewWindow -= new SHDocVw.DWebBrowserEvents_NewWindowEventHandler(OnWebBrowserNewWindow);
                wbEvents.NewWindow += new SHDocVw.DWebBrowserEvents_NewWindowEventHandler(OnWebBrowserNewWindow);
                if (currentBrowser.Source != null)
                    TopBar.url = currentBrowser.Source.ToString();
                else
                    TopBar.url = "";
                if (currentBrowser == InitialBrowser)
                {
                    TabItem ti = tabs.SelectedItem as TabItem;
                    currentBrowser = (WebBrowser)ti.Content;
                    try
                    {
                        ti.Header = (string)currentBrowser.InvokeScript("eval", "document.title.toString()");
                    }
                    catch
                    {
                        ti.Header = "Unknown";
                    }
                }
                else
                {
                    ClosableTab ti = tabs.SelectedItem as ClosableTab;
                    currentBrowser = (WebBrowser)ti.Content;
                    if (currentBrowser.Document != null)
                        try
                        {
                            ti.Title = (string)currentBrowser.InvokeScript("eval", "document.title.toString()");
                        }
                        catch
                        {
                            ti.Title = "Unknown";
                        }
                }
            }
        }
        void OnWebBrowserNewWindow(string URL, int Flags, string TargetFrameName, ref object PostData, string Headers, ref bool Processed)
        {
            Processed = true;
            WebBrowser newbrowser = new WebBrowser();
            ClosableTab tabitem = new ClosableTab(ItemType.BrowserTab);

            tabitem.Title = "Nouvel Onglet";
            tabs.Items.Add(tabitem);
            tabitem.Content = newbrowser;
            currentBrowser = newbrowser;
            newbrowser.LoadCompleted += WebPage_Completed;
            newbrowser.Navigated += WebBrowser_Navigated;
            tabs.SelectedItem = tabitem;
            currentBrowser.Navigate(URL);
        }
        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        void WebBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            if (currentBrowser != null && currentBrowser.Source != null)
            {
                HideJsScriptErrors((WebBrowser)sender);
                History history = new History();
                history.Date = DateTime.Now;
                history.Url = currentBrowser.Source.ToString();

                BrowserDAO.SaveNewHistory(history);
            }
        }

        private void LeftBar_GotoBack(object sender, RoutedEventArgs e)
        {
            if (currentBrowser != null)
            {
                if (currentBrowser.CanGoBack)
                {
                    currentBrowser.GoBack();
                }
            }     
        }

        private void LeftBar_GotoForward(object sender, RoutedEventArgs e)
        {
            if (currentBrowser != null)
            {
                if (currentBrowser.CanGoForward)
                {
                    currentBrowser.GoForward();
                }
            }
        }

        private void LeftBar_GotoSettings(object sender, RoutedEventArgs e)
        {
            ClosableTab tabitem = new ClosableTab(ItemType.SettingsTab);
            tabitem.Title = "Paramètres";
            tabitem.Content = new SettingsTabView();
            tabs.Items.Add(tabitem);
            currentBrowser = null;
            tabs.SelectedItem = tabitem;
        }
        private void TopBar_GotoPage(object sender, RoutedEventArgs e)
        {
            if (currentBrowser != null)
            {
                if (String.IsNullOrEmpty(TopBar.url)) return;
                if (TopBar.url.Equals("about:blank")) return;
                if (!TopBar.url.StartsWith("http://") &&
                    !TopBar.url.StartsWith("https://"))
                {
                    TopBar.url = "http://" + TopBar.url;
                }
                try
                {
                    currentBrowser.Navigate(new Uri(TopBar.url));
                }
                catch (System.UriFormatException)
                {
                    return;
                }
            }
        }

        private void TopBar_GotoAddFavorite(object sender, RoutedEventArgs e)
        {
            if (currentBrowser != null)
            {
                Favorite favorite = new Favorite();
                favorite.Url = currentBrowser.Source.ToString();
                BrowserDAO.SaveNewFavorite(favorite);
            }       
        }

        void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {

                    TabItem ti = tabs.SelectedItem as TabItem;
                    if (ti != null)
                    {
                        if (ti.Content.GetType() == typeof(WebBrowser))
                        {
                            currentBrowser = (WebBrowser)ti.Content;
                            if (currentBrowser.Source != null)
                                TopBar.url = currentBrowser.Source.ToString();
                            else
                                TopBar.url = "";
                        }
                        else if (ti.Content.GetType() == typeof(FavoryTabView))
                        {
                                TopBar.url = "Favoris";
                                currentBrowser = null;
                        }
                        else if (ti.Content.GetType() == typeof(HistoryTabView))
                        {
                                TopBar.url = "Historique";
                                currentBrowser = null;
                        }
                    }
            }
        }

        private void TopBar_GotoNewTab(object sender, RoutedEventArgs e)
        {
            WebBrowser newbrowser = new WebBrowser();
            ClosableTab tabitem = new ClosableTab(ItemType.BrowserTab);
         
            tabitem.Title = "Nouvel Onglet";
            tabs.Items.Add(tabitem);
            tabitem.Content = newbrowser;
            currentBrowser = newbrowser;
            newbrowser.LoadCompleted += WebPage_Completed;
            newbrowser.Navigated += WebBrowser_Navigated;
            tabs.SelectedItem = tabitem;
        }

        private void New_Tab_FromHistoryorFavorite(object sender, RequestNavigateEventArgs e)
        {
            WebBrowser newbrowser = new WebBrowser();
            ClosableTab tabitem = new ClosableTab(ItemType.BrowserTab);

            tabitem.Title = "Nouvel Onglet";
            tabs.Items.Add(tabitem);
            tabitem.Content = newbrowser;
            currentBrowser = newbrowser;
            newbrowser.LoadCompleted += WebPage_Completed;
            newbrowser.Navigated += WebBrowser_Navigated;
            tabs.SelectedItem = tabitem;
            currentBrowser.Navigate(e.Uri.ToString());
        }

        public void HideJsScriptErrors(WebBrowser wb)
        {
            FieldInfo fld = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fld == null)
                return;
            object obj = fld.GetValue(wb);
            if (obj == null)
                return;
            obj.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, obj, new object[] { true });
        }

        private void TopBar_GotoFavorite(object sender, RoutedEventArgs e)
        {
            ClosableTab tabitem = new ClosableTab(ItemType.FavoryTab);
            tabitem.Title = "Favoris";
            tabitem.Content = new FavoryTabView();
            tabs.Items.Add(tabitem);
            currentBrowser = null;
            tabs.SelectedItem = tabitem;
            FavoryTabView favorite = tabitem.Content as FavoryTabView;
            favorite.NavigateToFavorite += New_Tab_FromHistoryorFavorite;
        }

        private void TopBar_GotoHistory(object sender, RoutedEventArgs e)
        {
            ClosableTab tabitem = new ClosableTab(ItemType.HistoryTab);

            tabitem.Title = "Historique";
            tabitem.Content = new HistoryTabView();
            tabs.Items.Add(tabitem);
            currentBrowser = null;
            tabs.SelectedItem = tabitem;
            HistoryTabView historic = tabitem.Content as HistoryTabView;
            historic.NavigateToHistory += New_Tab_FromHistoryorFavorite;
        }

    }
}