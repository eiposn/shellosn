﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NhibernateORM;
using System.Collections.ObjectModel;

namespace BrowserModule.Views
{
    /// <summary>
    /// Interaction logic for HistoryTabView.xaml
    /// </summary>
    public partial class HistoryTabView : UserControl
    {
        public ObservableCollection<History> historylist { get; set; }
        public List<History> history { get; set; }
        public NHinerbanteDatabaseHandler BrowserDAO { get; set; }
        public RequestNavigateEventHandler NavigateToHistory;
        public HistoryTabView()
        {
            InitializeComponent();
            this.DataContext = this;
            BrowserDAO = NHinerbanteDatabaseHandler.getInstance();
            history = BrowserDAO.GetHistory();
            historylist = new ObservableCollection<History>(history);
        }

        private void Button_Click_DeleteHistory(object sender, RoutedEventArgs e)
        {
                BrowserDAO.DeleteAllHistory();
                historylist.Clear(); 
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            NavigateToHistory(this, e);
        }

        private void Button_Click_DeleteSelected(object sender, RoutedEventArgs e)
        {
            List<int> ToDelete = new List<int>();

            foreach (var item in  ListBoxHistory.SelectedItems)
            {
                History history = item as History;
                ToDelete.Add(history.Id); 
                BrowserDAO.DeleteHistory(history);
            }
            for (int j = 0; j < ToDelete.Count; j++ )
            {
                for (int i = 0; i < historylist.Count; i++)
                {
                    if (historylist[i].Id == ToDelete[j])
                    {
                        historylist.Remove(historylist[i]);
                    }
                }
            }
               
        }


    }
}
