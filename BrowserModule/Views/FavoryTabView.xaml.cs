﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NhibernateORM;
using System.Collections.ObjectModel;

namespace BrowserModule.Views
{
    /// <summary>
    /// Interaction logic for FavoryTabView.xaml
    /// </summary>
    public partial class FavoryTabView : UserControl
    {
        public ObservableCollection<Favorite> favoriteslist {get; set;}
        public List<Favorite> favorites { get; set; }
        public RequestNavigateEventHandler NavigateToFavorite;


        public NHinerbanteDatabaseHandler BrowserDAO { get; set; } 
        public FavoryTabView()
        {
            InitializeComponent();
            this.DataContext = this;
             BrowserDAO = NHinerbanteDatabaseHandler.getInstance();
            favorites = BrowserDAO.GetFavorite();
           favoriteslist = new ObservableCollection<Favorite>(favorites);
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            NavigateToFavorite(this, e);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<int> ToDelete = new List<int>();

            foreach (var item in ListBoxFavorite.SelectedItems)
            {
                Favorite favorite = item as Favorite;
                ToDelete.Add(favorite.Id);
                BrowserDAO.DeleteFavorite(favorite);
            }
            for (int j = 0; j < ToDelete.Count; j++)
            {
                for (int i = 0; i < favoriteslist.Count; i++)
                {
                    if (favoriteslist[i].Id == ToDelete[j])
                    {
                        favoriteslist.Remove(favoriteslist[i]);
                    }
                }
            }
        }
    }
}
