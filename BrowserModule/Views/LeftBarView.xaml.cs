﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrowserModule.Views
{
    /// <summary>
    /// Interaction logic for LeftBarView.xaml
    /// </summary>
    public partial class LeftBarView : UserControl
    {
        public event RoutedEventHandler ActualizePage;
        public event RoutedEventHandler GotoForward;
        public event RoutedEventHandler GotoBack;
        public event RoutedEventHandler GotoHome;
        public event RoutedEventHandler GotoSettings;

        public LeftBarView()
        {
            InitializeComponent();
        }

        private void Button_Click_Actualize(object sender, RoutedEventArgs e)
        {
            ActualizePage(this, e);
        }

        private void Button_Click_Back(object sender, RoutedEventArgs e)
        {
            GotoBack(this, e);
        }

        private void Button_Click_Forward(object sender, RoutedEventArgs e)
        {
            GotoForward(this, e);
        }

        private void Button_Click_Home(object sender, RoutedEventArgs e)
        {
            GotoHome(this, e);
        }

        private void Setting_Click(object sender, RoutedEventArgs e)
        {
            GotoSettings(this, e);
        }
    }
}
