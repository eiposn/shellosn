﻿using NhibernateORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrowserModule.Views
{
    /// <summary>
    /// Interaction logic for SettingsTabView.xaml
    /// </summary>
    public partial class SettingsTabView : UserControl
    {
        public NHinerbanteDatabaseHandler BrowserDAO { get; set; }
        public SettingsTabView()
        {
            InitializeComponent();
            this.DataContext = this;
            BrowserDAO = NHinerbanteDatabaseHandler.getInstance();
            BrowserSettings homePage = BrowserDAO.GetHome();
            input_home.Text = homePage.Value;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BrowserDAO = NHinerbanteDatabaseHandler.getInstance();
            BrowserSettings newHomePage = new BrowserSettings();
            newHomePage.Value = input_home.Text;
            BrowserDAO.SaveHome(newHomePage);
        }


    }
}
