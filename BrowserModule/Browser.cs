﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using OSN;
using BrowserModule.Views;
using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.PubSubEvents;

namespace BrowserModule
{
    [Module(ModuleName = "Internet", OnDemand = true)]
    public class Browser : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public Browser(IRegionManager _regionManager, IUnityContainer _container, TaskBarRegistry _taskbar, IModuleCatalog _catalog)
        {
            this.regionManager = _regionManager;
            this.container = _container;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            container.RegisterType<Object, BrowserView>("BrowserView");
            IEnumerable<ModuleInfo> module = _catalog.Modules;
            string Imgpath = "";
            foreach (ModuleInfo cur in module)
            {
                if (cur.ModuleName == "Internet")
                    Imgpath = cur.Ref;
            }
            Imgpath = System.IO.Path.ChangeExtension(Imgpath, "png");
            _taskbar.RegisterMenuItem("BrowserView", "Internet", Imgpath);
        }

        void LoadViewInRegion<TViewType>(string regionName)
        {
            IRegion region = regionManager.Regions[regionName];
            string viewName = typeof(TViewType).Name;

            object view = region.GetView(viewName);
            if (view == null)
            {
                view = container.Resolve<TViewType>();

                region.Add(view, viewName);
            }
        }
        public void Initialize()
        {
            string ribbonRegionName = ConfigurationManager.AppSettings["RibbonRegionName"];
            string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];

            var moduleAView = new Uri("BrowserView", UriKind.Relative);
            regionManager.RequestNavigate("b", moduleAView);
        }
    }
}
