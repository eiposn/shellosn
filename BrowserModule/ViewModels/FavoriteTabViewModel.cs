﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhibernateORM;
using System.Collections.ObjectModel;

namespace BrowserModule.ViewModels
{
    public class FavoriteTabViewModel : ViewModelBase
    {
        public NHinerbanteDatabaseHandler BrowserDAO { get; set; }
        public ObservableCollection<Favorite> favoriteslist
        {
            get
            {
                return new ObservableCollection<Favorite>(BrowserDAO.GetFavorite());
            }
        }

        public FavoriteTabViewModel()
        {
            BrowserDAO = NHinerbanteDatabaseHandler.getInstance();
        }
    }
}
