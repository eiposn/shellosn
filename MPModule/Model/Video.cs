﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPModule.Model
{
    class Video
    {
        public string Name { get; set; }
        public string path { get; set; }

       public  Video(string _name, string _path)
        {
            this.Name = _name;
            this.path = _path;
        }

    }
}
