﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MPModule.Model
{
   public class Album
    {
        TagLib.IPicture pic;
        BitmapImage bitmap;

        String name;
        String artist;
        List<Musique> song;


        public List<Musique> Song
        {
            get { return song; }
        }

        public BitmapImage Bitmap
        {
            get { return bitmap; }
        }

        public TagLib.IPicture Pic
        {
            get { return pic; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Artist
        {
            get { return artist; }
        }

        public Album(String name, String artist, TagLib.IPicture pic)
        {
            this.name = name;
            this.artist = artist;
            this.pic = pic;
            if (pic != null)
            {
                MemoryStream ms = new MemoryStream(pic.Data.Data);
                ms.Seek(0, SeekOrigin.Begin);

                // ImageSource for System.Windows.Controls.Image
                bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = ms;
                bitmap.EndInit();
            }
            song = new List<Musique>();
            }

        public void AddSong(Musique song)
        {
            this.song.Add(song);
        }
    }
}
