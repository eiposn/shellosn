﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPModule.Model
{
    public class Musique
    {
        string songName;
        string artist;
        string album;
        int lenght;
        int date;
        public string path;

        public string SongName
        {
            get { return songName; }
        }

        public string Artist
        {
            get { return artist; }
        }

        public string Album
        {
            get { return album; }
        }

        public string Path
        {
            get { return path; }
        }

       public  Musique(string _name, string _artist, string _album, string _path)
        {
            this.songName = _name;
            this.artist = _artist;
            this.album = _album;
            this.path = _path;
        }

       public override string ToString()
       {
           return this.path;
       }
    }
}
