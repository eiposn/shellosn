﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using OSN;
using System.Configuration;
using MPModule.Views;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.PubSubEvents;

namespace MPModule
{
    [Module(ModuleName = "Multimédia", OnDemand = true)]
    public class MPModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public MPModule(IRegionManager _regionManager, IUnityContainer _container, TaskBarRegistry _taskbar, IModuleCatalog _catalog)
        {
           
            this.regionManager = _regionManager;
            this.container = _container;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            container.RegisterType<Object, MPViewBase>("MPViewBase");
            IEnumerable<ModuleInfo> module = _catalog.Modules;
            string Imgpath = "";
            foreach (ModuleInfo cur in module)
            {
                if (cur.ModuleName == "Multimédia")
                    Imgpath = cur.Ref;

            }

            Imgpath = System.IO.Path.ChangeExtension(Imgpath, "png");
            _taskbar.RegisterMenuItem("MPViewBase", "Multimédia", Imgpath);
        }


        /// <summary>
        /// You must load the views in order for the navigation to resolve
        /// </summary>
        void LoadViewInRegion<TViewType>(string regionName)
        {
            IRegion region = regionManager.Regions[regionName];
            string viewName = typeof(TViewType).Name;

            object view = region.GetView(viewName);
            if (view == null)
            {
                view = container.Resolve<TViewType>();

                region.Add(view, viewName);
            }
        }

        public void Initialize()
        {
            string ribbonRegionName = ConfigurationManager.AppSettings["RibbonRegionName"];
            string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];

           var moduleAView = new Uri("MPViewBase", UriKind.Relative);
            regionManager.RequestNavigate("b", moduleAView);
        /*    regionManager.RegisterViewWithRegion("MPViewBase", typeof (MPControl));*/

        }
    }
}
