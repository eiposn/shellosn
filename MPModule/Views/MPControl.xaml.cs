﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using Microsoft.Win32;
using CommonModule.BaseTypes;
using System.Media;
using MPModule.Model;
using OpenFileExplorer;

namespace MPModule.Views
{
    /// <summary>
    /// Logique d'interaction pour ExplorerView.xaml
    /// </summary>
    public partial class MPControl : WorkspaceViewBase
    {
        private bool mediaPlayerIsPlaying = false;
        private bool userIsDraggingSlider = false;
        public MediaPlayer player;
        VideoDrawing aVideoDrawing = new VideoDrawing();
        List<Musique> MusicFile;
        List<Video>   VideoFile;

        public MPControl()
        {
            InitializeComponent();
            player = new MediaPlayer();
            MusicFile = new List<Musique>();
            VideoFile = new List<Video>();
            MusiqueLst.ItemsSource = MusicFile;
            VideoLst.ItemsSource = VideoFile;
        }

        private void Open_Video(object sender, System.Windows.RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
          //  ofd.Filter = "MP3 Files (*.mp3)|*.mp3|WMA Files (*.wma)|*.wma"; 
            ofd.Multiselect = false;
            ofd.ShowDialog();
            if (ofd.CheckFileExists == true)
            {
                player.Open(new Uri(ofd.FileName));
                aVideoDrawing.Rect = new Rect(0, 0, 100, 100);
                aVideoDrawing.Player = player;
                DrawingBrush db = new DrawingBrush(aVideoDrawing);
                VideoBrush.Background = db;
                VideoFile.Add(new Video(ofd.FileName, ofd.FileName));
            }
        }

        private void Open_Music(object sender, System.Windows.RoutedEventArgs e)
        {
            FileOpen ofd = new FileOpen("toto");
            ofd.Filter = "MP3 Files (*.mp3)|*.mp3|WMA Files (*.wma)|*.wma";
            if (ofd.ShowDialog() == true)
            {
                Musique tmp = new Musique(ofd.FileName, "", "", ofd.FileName);
                player.Open(new Uri(ofd.FileName));
                MusicFile.Add(tmp);
                player.Play();
            }
        }

        private void Open_Photo(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO : ajoutez ici l'implémentation du gestionnaire d'événements.
        }

        private void Play(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Play();
        }

        private void Pause(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Pause();
        }

        private void Stop(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Stop();
        }

        private void SelectMusic(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Musique selected = (Musique) MusiqueLst .SelectedItem;
            player.Stop();
            player.Open(new Uri(selected.path));
            player.Play();
        }
    }
}