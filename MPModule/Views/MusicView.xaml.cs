﻿using CommonModule.BaseTypes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Win32;
using MPModule.Model;
using OpenFileExplorer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TagLib;

namespace MPModule.Views
{
    /// <summary>
    /// Logique d'interaction pour MusicView.xaml
    /// </summary>
    public partial class MusicView : WorkspaceViewBase
    {
        private readonly IRegionManager regionManager;
        private MediaPlayer player;
        private readonly string MusicPath;
        List<string> allFiles = new List<string>();
        List<string> mediaExtensions = new List<string> { ".mp3", ".mp4", ".flac" };
        List<Album> Song = new List<Album>();
        List<Musique> musicLst;
        ObservableCollection<Musique> playlistlst;
        private bool userIsDraggingSlider = false;
        Musique currentplay;

        public MusicView(IRegionManager _regionManager, MediaPlayer _player)
        {
            InitializeComponent();
            this.regionManager = _regionManager;
            this.player = _player;
            this.MusicPath = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
            musicLst = new List<Musique>();
            playlistlst = new ObservableCollection<Musique>();
            RecursiveSearch(MusicPath);
            ConvertFileToMusiqueObject();
            trvFamilies.ItemsSource = Song;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
            SoundSlider.Minimum = 0f;
            SoundSlider.Maximum = 1.0f;
            SoundSlider.Value = 1.0f;
        }

        /// timer_tick change and move the slider progress bar
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            if ((player.Source != null) && (player.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                MediaSlider.Minimum = 0;
                MediaSlider.Maximum = player.NaturalDuration.TimeSpan.TotalSeconds;
                MediaSlider.Value = player.Position.TotalSeconds;
            }
        }


        private void Open_Music(object sender, System.Windows.RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "MP3 Files (*.mp3)|*.mp3|WMA Files (*.wma)|*.wma | FLAC Files (*.flac)|*.flac";
            if (ofd.ShowDialog() == true)
            {
                if (ofd.FileName != "")
                {
                    Musique tmp = new Musique(ofd.FileName, "", "", ofd.FileName);
                    player.Open(new Uri(ofd.FileName));
                    currentplay = tmp;
                }
            }
        }

        private void RecursiveSearch(string dir)
        {
            allFiles.AddRange(System.IO.Directory.GetFiles(dir));
            foreach (string d in System.IO.Directory.GetDirectories(dir))
            {
                RecursiveSearch(d);
            }
        }

        private void ConvertFileToMusiqueObject()
        {
            foreach (string d in allFiles)
            {
                try
                {
                    if (mediaExtensions.Contains(System.IO.Path.GetExtension(d).ToLower()))
                {
                    FileStream toto = new FileStream(d, FileMode.Open);
                    //Create a simple file and simple file abstraction
                    var simpleFile = new SimpleFile(d, toto);
                    var simpleFileAbstraction = new SimpleFileAbstraction(simpleFile);
                    /////////////////////

                    //Create a taglib file from the simple file abstraction
                    var mp3File = TagLib.File.Create(simpleFileAbstraction);

                    //Get all the tags
                    TagLib.Tag tags = mp3File.Tag;

                    //Save and close
                    mp3File.Save();
                    mp3File.Dispose();
                    if (tags.Title == "" || tags.Title == null)
                        tags.Title = System.IO.Path.GetFileNameWithoutExtension(d);

                    Album tmp = null;
                    if (Song.Exists(x => x.Name == (tags.Album)))
                        tmp = Song.First(x => x.Name == (tags.Album));
                    if (tmp == null)
                    {
                        IPicture pic = null;
                        if (tags.Pictures.Length > 0)
                            pic = tags.Pictures[0];
                           tmp = new Album(tags.Album, tags.FirstAlbumArtist, pic);
                        tmp.AddSong(new Musique(tags.Title, tags.FirstAlbumArtist, tags.Album, d));
                        Song.Add(tmp);
                    }
                    else
                        tmp.AddSong(new Musique(tags.Title, tags.FirstAlbumArtist, tags.Album, d));
                    musicLst.Add(new Musique(tags.Title, tags.FirstAlbumArtist, tags.Album, d));
                }
                }
                catch
                {
                }
            }
        }

        private void Play(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Play();
        }

        private void Pause(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Pause();
        }

        private void Stop(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Stop();
        }

        /// Check if the user is dragging the slider
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediaSlider_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        /// Change the timer when the user finish to move the slider
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SoundSlider_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            player.Volume = SoundSlider.Value;
        }


        /// Change the timer when the user finish to move the slider
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediaSlider_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            player.Position = TimeSpan.FromSeconds(MediaSlider.Value);
        }

        /// Change the value of the Slider bar
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediaSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblProgressStatus.Text = TimeSpan.FromSeconds(MediaSlider.Value).ToString(@"hh\:mm\:ss");
            //CurrentPlay.Text = TimeSpan.FromSeconds(player.NaturalDuration.TimeSpan.TotalSeconds).ToString(@"hh\:mm\:ss") + currentplay.SongName ;
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var treeItem = trvFamilies as ListView;
            if (treeItem.SelectedItem is Album)
            {
                Album cur = (Album)treeItem.SelectedItem;
                player.Stop();
                player.Open(new Uri(cur.Song[0].Path));
                currentplay = cur.Song[0];
                player.Play();
            }
            else
            {
                Musique cur = (Musique)treeItem.SelectedItem;
                currentplay = cur;
                player.Stop();
                player.Open(new Uri(cur.Path));
                player.Play();
            }

        }

        private void AlbumSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }

        private void MusiqueSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListView cur = (ListView)sender;
            Musique curM = (Musique)cur.SelectedItem;
            currentplay = curM;
            player.Stop();
            player.Open(new Uri(curM.Path));
            player.Play();
        }
    }
}
