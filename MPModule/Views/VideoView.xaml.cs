﻿using CommonModule.BaseTypes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Win32;
using MPModule.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MPModule.Views
{
    /// <summary>
    /// Logique d'interaction pour VideoView.xaml
    /// </summary>
    public partial class VideoView : WorkspaceViewBase
    {
        bool userIsDraggingSlider;
        private readonly IRegionManager regionManager;
        private MediaPlayer player;
        private VideoDrawing video;
        private String VideoPath;
        List<String> allFiles = new List<String>();
        List<string> mediaExtensions = new List<string> { ".mp3", ".mp4", ".avi" };
        List<Video> Videos;
        public VideoView(IRegionManager _regionManager, MediaPlayer _player)
        {
            InitializeComponent();
            this.regionManager = _regionManager;
            this.player = _player;
            video = new VideoDrawing();
            this.VideoPath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
            Videos = new List<Video>();
            RecursiveSearch(VideoPath);
            ConvertFileToVideoObject();
            VideoLst.ItemsSource = Videos;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
            SoundSlider.Minimum = 0f;
            SoundSlider.Maximum = 1.0f;
            SoundSlider.Value = 1.0f;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if ((player.Source != null) && (player.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                MediaSlider.Minimum = 0;
                MediaSlider.Maximum = player.NaturalDuration.TimeSpan.TotalSeconds;
                MediaSlider.Value = player.Position.TotalSeconds;
            }
        }

        private void ConvertFileToVideoObject()
        {
            foreach (string d in allFiles)
            {
                if (mediaExtensions.Contains(System.IO.Path.GetExtension(d).ToLower()))
                {
                    this.Videos.Add(new Video(System.IO.Path.GetFileNameWithoutExtension(d), d));
                }
            }
        }

        private void RecursiveSearch(string dir)
        {
            allFiles.AddRange(Directory.GetFiles(dir));
            foreach (string d in Directory.GetDirectories(dir))
            {
                RecursiveSearch(d);
            }
        }

        private void Open_Video(object sender, System.Windows.RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            //  ofd.Filter = "MP3 Files (*.mp3)|*.mp3|WMA Files (*.wma)|*.wma"; 
            ofd.Multiselect = false;
            ofd.ShowDialog();
             if (ofd.CheckFileExists == true)
            {
                if (ofd.FileName != null && ofd.FileName != "")
                {
                    player.Open(new Uri(ofd.FileName));
                video.Player = player;
                DrawingBrush DBrush = new DrawingBrush(video);
                VideoDraw.Background = DBrush;
                player.Play();
                }
            }
        }

        private void Play(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Play();
        }

        private void Pause(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Pause();
        }

        private void Stop(object sender, System.Windows.RoutedEventArgs e)
        {
            player.Stop();
        }


        /// Check if the user is dragging the slider
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediaSlider_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        /// Change the timer when the user finish to move the slider
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SoundSlider_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            player.Volume = SoundSlider.Value;
        }


        /// Change the timer when the user finish to move the slider
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediaSlider_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            player.Position = TimeSpan.FromSeconds(MediaSlider.Value);
        }

        /// Change the value of the Slider bar
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediaSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblProgressStatus.Text = TimeSpan.FromSeconds(MediaSlider.Value).ToString(@"hh\:mm\:ss");
            //CurrentPlay.Text = TimeSpan.FromSeconds(player.NaturalDuration.TimeSpan.TotalSeconds).ToString(@"hh\:mm\:ss") + currentplay.SongName ;
        }

        private void VideoSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ListView cur = (ListView)sender;
            Video curM = (Video)cur.SelectedItem;
            player.Stop();
            player.Open(new Uri(curM.path));
            video.Player = player;
            video.Rect = new Rect(0, 0, 100, 100);
            DrawingBrush DBrush = new DrawingBrush(video);
            VideoDraw.Background = DBrush;
            player.Play();
        }
    }
}
