﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MPModule.Views
{
    /// <summary>
    /// Logique d'interaction pour PhotoView.xaml
    /// </summary>
    public partial class PhotoView : UserControl
    {

        private readonly IRegionManager regionManager;
        public PhotoView(IRegionManager _regionManager)
        {
            this.regionManager = _regionManager;
            InitializeComponent();
        }

     /*   override public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            regionManager.Regions["MPViewBase"].Remove(this);
        }*/
    }
}
