﻿using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MPModule.Views
{
    /// <summary>
    /// Logique d'interaction pour MPViewBase.xaml
    /// </summary>
    public partial class MPViewBase : UserControl
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;
        public MediaPlayer player;

        public MPViewBase(IRegionManager _regionManager)
        {
            this.regionManager = _regionManager;
            InitializeComponent();
            this.player = new MediaPlayer();
           // regionManager.RegisterViewWithRegion("MPViewBase", typeof(MusicView));
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            this.container.RegisterInstance<MediaPlayer>(player);
            container.RegisterType<Object, MusicView>("MusicView");
            container.RegisterType<Object, VideoView>("VideoView");
        }

        private void OpenMusicView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("MusicView", UriKind.Relative);
            regionManager.RequestNavigate("MPViewBase", moduleAView);
        }

        private void OpenVideoView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("VideoView", UriKind.Relative);
            regionManager.RequestNavigate("MPViewBase", moduleAView);
        }
    }
}
