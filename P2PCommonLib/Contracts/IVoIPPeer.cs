﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib.VoIP;
using Hik.Communication.ScsServices.Service;

namespace P2PCommonLib.Contracts
{
    [ScsService(Version = "1.0.0.0")]
    public interface IVoIPPeer
    {
        void OnSendTextMessage(UserIdentifier userId, String Message);

        bool StartCallProtocol(String username);

        void StopCallProtocol(String username);
    }
}
