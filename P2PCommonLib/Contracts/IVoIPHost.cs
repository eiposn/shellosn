﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib.VoIP;
using Hik.Communication.ScsServices.Service;

namespace P2PCommonLib.Contracts
{
    [ScsService(Version = "1.0.0.0")]
    public interface IVoIPHost
    {
        void Identification(UserIdentifier userId);

        void SendTextMessage(UserIdentifier userId, String Message);

        void Close();

        bool StartCallProtocol(String Username);

        void StopCallProtocol(String username);
    }
}
