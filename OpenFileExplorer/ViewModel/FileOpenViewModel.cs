﻿using OpenFileExplorer.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenFileExplorer.ViewModel
{
    class FileOpenViewModel
    {
        private string username;

        public ObservableCollection<Drive> Drives
        {
            get;
            private set;
        }
       public FileOpenViewModel(string _username)
        {
            string startupPath = System.IO.Directory.GetCurrentDirectory();
            this.username = _username;
            this.Drives = new ObservableCollection<Drive>();
            startupPath += "\\" + username + "\\";
            string musicPath = startupPath + "Musics";
            string videoPath = startupPath + "Videos";
            string DocumentPath = startupPath + "Documents";
            string PicturePath = startupPath + "Pictures";
            Console.WriteLine(startupPath);
            foreach (DriveInfo driveInfo in System.IO.DriveInfo.GetDrives())
            {
                if (driveInfo.DriveType == DriveType.Removable)
                    this.Drives.Add(new Drive(driveInfo.Name, driveInfo.IsReady));
            }
            if (!System.IO.Directory.Exists(startupPath))
                System.IO.Directory.CreateDirectory(startupPath);
            if (!System.IO.Directory.Exists(musicPath))
                System.IO.Directory.CreateDirectory(musicPath);
            if (!System.IO.Directory.Exists(videoPath))
                System.IO.Directory.CreateDirectory(videoPath);
            if (!System.IO.Directory.Exists(DocumentPath))
                System.IO.Directory.CreateDirectory(DocumentPath);
            if (!System.IO.Directory.Exists(PicturePath))
                System.IO.Directory.CreateDirectory(PicturePath);

            this.Drives.Add(new Drive(musicPath, true));
            this.Drives.Add(new Drive(videoPath, true));
            this.Drives.Add(new Drive(DocumentPath, true));
            this.Drives.Add(new Drive(PicturePath, true));
        }

       public void LoadChildren(Drive d)
       {
           foreach (string directory in System.IO.Directory.GetDirectories(d.Name))
           {
               DirectoryInfo directoryInfo = new DirectoryInfo(directory);
               d.Children.Add(new Directory(directory, directoryInfo.Name));
           }
           foreach (string file in System.IO.Directory.GetFiles(d.Name))
           {
               FileInfo fileInfo = new FileInfo(file);
               d.Children.Add(new File(file, fileInfo.Name));
           }
       }

       public void LoadChildren(Directory d)
       {
           try
           {
               foreach (string directory in System.IO.Directory.GetDirectories(d.FullPath))
               {
                   DirectoryInfo directoryInfo = new DirectoryInfo(directory);
                   d.Children.Add(new Directory(directory, directoryInfo.Name));
               }
               foreach (string file in System.IO.Directory.GetFiles(d.FullPath))
               {
                   FileInfo fileInfo = new FileInfo(file);
                   d.Children.Add(new File(file, fileInfo.Name));
               }
           }
        catch
             {
              }
       }

       public  Stream OpenFile(string path)
       {
           FileStream InputBin = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
           return InputBin;
       }
    }
}
