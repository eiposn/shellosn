﻿using OpenFileExplorer.Model;
using OpenFileExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenFileExplorer
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class FileOpen : Window
    {
        public  string FileName;
        public  string Filter;
        private FileOpenViewModel fopvm;
        public FileOpen(String Username)
        {
            FileName = "";
            fopvm = new FileOpenViewModel(Username);
            InitializeComponent();
            BaseList.ItemsSource = fopvm.Drives;
            fopvm.LoadChildren(fopvm.Drives[0]);
            ChildList.ItemsSource = fopvm.Drives[0].Children;
        }

        public string  OpenFilePath()
        {
            return this.FileName;
        }

        public Stream OpenFile()
        {
            if (FileName != null)
                return fopvm.OpenFile(FileName);
            else
                return null;
        }

        private void ChildList_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (System.Windows.Application.Current.MainWindow.IsInitialized == true)
            {
              Object lbi = ((sender as ListView).SelectedItem);
              if (lbi is Directory)
              {
                  fopvm.LoadChildren(lbi as Directory);
                  ChildList.ItemsSource = (lbi as Directory).Children;
                  ChildList.Items.Refresh();
              }
              else
              {
                  this.FileName = (lbi as File).FullPath;
              }
            }
        }

        private void BaseList_SelectionChanged(object sender, RoutedEventArgs e)
        {
            Drive lbi = ((sender as ListView).SelectedItem as Drive);
            fopvm.LoadChildren(lbi);
            ChildList.ItemsSource = lbi.Children;
            ChildList.Items.Refresh();
        }

        private void Open_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Cancel_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Close();
        }


    }
}
