﻿using CommonModule.BaseTypes;
using ContactModule.ViewModels;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContactModule.Views
{
    /// <summary>
    /// Logique d'interaction pour AddContactView.xaml
    /// </summary>
    public partial class AddContactView : WorkspaceViewBase
    {
        public ContactViewModel ContactVM;
        private IRegionManager regionManager { get; set; }

        public AddContactView(IRegionManager _regionManager, ContactViewModel _contactVM)
        {
            InitializeComponent();

            this.regionManager = _regionManager;
            ContactVM = _contactVM;          
            this.DataContext = ContactVM;
        }
    }
}
