﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ContactModule.ViewModels;
using CommonModule.BaseTypes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using Microsoft.Practices.ServiceLocation;
using ContactModule.Controls;


namespace ContactModule.Views
{
    /// <summary>
    /// Logique d'interaction pour ContactControl.xaml
    /// </summary>
    public partial class ContactViewBase : WorkspaceViewBase
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        protected ContactViewModel contactVM;

        /// <summary>
        /// initialiazes the class
        /// </summary>
        /// <param name="_regionManager"></param>
        public ContactViewBase(IRegionManager _regionManager)
        {
            InitializeComponent();
            this.regionManager = _regionManager;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();

            container.RegisterType<Object, WrapperView>("WrapperView");
            container.RegisterType<Object, AddContactView>("AddContactView");
            container.RegisterType<Object, ModifyContactView>("ModifyContactView");

            contactVM = new ContactViewModel(_regionManager);
            this.container.RegisterInstance<ContactViewModel>(contactVM);

            this.DataContext = contactVM;

        }

        /// <summary>
        /// Open the setting view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenAddContactView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("AddContactView", UriKind.Relative);
            regionManager.RequestNavigate("ContactViewBase", moduleAView);
        }

        private void RefreshContactList(object sender, MouseButtonEventArgs e)
        {
            contactVM.LoadContact();
        }
    }
}
