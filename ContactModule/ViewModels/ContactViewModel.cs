﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using S22.Imap;
using System.Net.Mail;
using System.Windows;
using Microsoft.Practices.Prism.Commands;
using System.Threading;
using System.Windows.Data;
using Microsoft.Practices.Prism.Regions;
using System.Windows.Input;
using NhibernateORM;
using ContactModule.Controls;


namespace ContactModule.ViewModels
{
    public class ContactViewModel : INotifyPropertyChanged
    {
        public ContactViewModel(IRegionManager _regionManager)
        {
            this.regionManager = _regionManager;

            ContactDAO = NHinerbanteDatabaseHandler.getInstance();

            alpha = new List<String> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            sendMailCommand = new DelegateCommand(DoSendMailCommand, IsSelected);
            modifyCommand = new DelegateCommand(DoModifyContactCommand, IsSelected);
            deleteCommand = new DelegateCommand(DoDeleteContactCommand, IsSelected);
            cancelCommand = new DelegateCommand(DoCancelCommand, IsSelected);
            validateCommand = new DelegateCommand(DoValidateCommand, IsFormChecked);
            validateModCommand = new DelegateCommand(DoValidateModCommand, IsFormChecked);
            selectAlphaCommand = new DelegateCommand(DoSelectAlphaCommand, IsSelected);
            imageCommand = new DelegateCommand(DoImageCommand, IsSelected);

            Vignettes = new ObservableCollection<VignetteControl>();

            TxtBirthday = new DateTime(1950, 01, 01);

            Init();
        }

        public void Init()
        {
            LoadContact();
        }

        private IRegionManager regionManager { get; set; }

        public NHinerbanteDatabaseHandler ContactDAO;

        private Contact _currentContact;

        public Contact CurrentContact
        {
            get { return _currentContact; }
            set
            {
                _currentContact = value;
                OnPropertyChanged("CurrentContact");
            }
        }


        private VignetteControl _currentVignette;

        public VignetteControl CurrentVignette
        {
            get { return _currentVignette; }
            set
            {
                _currentVignette = value;
                OnPropertyChanged("CurrentVignette");
            }
        }


        private List<String> alpha;

        public List<String> Alpha
        {
            get { return alpha; }
            set
            {
                alpha = value;
                OnPropertyChanged("Alpha");
            }
        }

        private String currentLetter;

        public String CurrentLetter
        {
            get { return currentLetter; }
            set
            {
                currentLetter = value;
                DoSelectAlphaCommand();
                OnPropertyChanged("CurrentLetter");
            }
        }

        #region Commands

        private DelegateCommand sendMailCommand;
        private DelegateCommand modifyCommand;
        private DelegateCommand deleteCommand;
        private DelegateCommand cancelCommand;
        private DelegateCommand validateCommand;
        private DelegateCommand validateModCommand;
        private DelegateCommand selectAlphaCommand;
        private DelegateCommand imageCommand;

        public DelegateCommand ImageCommand
        {
            get
            {
                return imageCommand;
            }
        }

        public DelegateCommand SelectAlphaCommand
        {
            get
            {
                return selectAlphaCommand;
            }
        }

        public DelegateCommand ValidateModCommand
        {
            get
            {
                return validateModCommand;
            }
        }

        public DelegateCommand ValidateCommand
        {
            get
            {
                return validateCommand;
            }
        }

        public DelegateCommand CancelCommand
        {
            get
            {
                return cancelCommand;
            }
        }

        public Boolean _Selectable = true;

        public Boolean IsSelected()
        {
            return _Selectable;
        }

        private bool IsFormChecked()
        {
            return true;
        }

        public DelegateCommand DeleteCommand
        {
            get
            {
                return deleteCommand;
            }
        }

        public DelegateCommand ModifyCommand
        {
            get
            {
                return modifyCommand;
            }
        }

        public DelegateCommand SendMailCommand
        {
            get
            {
                return sendMailCommand;
            }
        }



        #endregion

        #region getters setters Contact

        private string txtFirstName;
        private string txtLastName;
        private string txtEmail;
        private string txtPostal;
        private string txtPhone;
        private DateTime txtBirthday;
        private byte[] image;

        private byte[] Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }
        }

        public String TxtFirstName
        {
            get
            {
                return txtFirstName;
            }
            set
            {
                txtFirstName = value;
                OnPropertyChanged("TxTFirstName");
            }
        }

        public String TxtLastName
        {
            get
            {
                return txtLastName;
            }
            set
            {
                txtLastName = value;
                OnPropertyChanged("TxTLastName");
            }
        }

        public String TxtMail
        {
            get
            {
                return txtEmail;
            }
            set
            {
                txtEmail = value;
                OnPropertyChanged("TxtMail");
            }
        }

        public String TxtPostal
        {
            get
            {
                return txtPostal;
            }
            set
            {
                txtPostal = value;
                OnPropertyChanged("TxtPostal");
            }
        }

        public String TxtPhone
        {
            get
            {
                return txtPhone;
            }
            set
            {
                txtPhone = value;
                OnPropertyChanged("TxtPhone");
            }
        }

        public DateTime TxtBirthday
        {
            get
            {
                return txtBirthday;
            }
            set
            {
                txtBirthday = value;
                OnPropertyChanged("TxtBirthday");
            }
        }

        #endregion

        private ObservableCollection<VignetteControl> _vignettes;

        public ObservableCollection<VignetteControl> Vignettes
        {
            get
            {
                return _vignettes;
            }
            set
            {
                _vignettes = value;
                OnPropertyChanged("Vignettes");
            }
        }

        private String parameter;

        public String Parameter
        {
            get { return parameter; }
            set { parameter = value; }
        }

        /// <summary>
        /// Signals when a property is modified
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        //////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Delete a contact from the local DB
        /// </summary>
        private void DoDeleteContactCommand()
        {
            if (CurrentVignette != null)
            {
                var contact = CurrentVignette.Contact;
                ContactDAO.DeleteContact(contact);
                Vignettes.Remove(CurrentVignette);
            }
        }

        /// <summary>
        /// Load the Modify View to edit a contact
        /// </summary>
        private void DoModifyContactCommand()
        {
            if (CurrentVignette != null)
            {
                var contact = CurrentVignette.Contact;

                TxtFirstName = contact.FirstName;
                TxtLastName = contact.LastName;
                TxtMail = contact.Email;
                TxtPostal = contact.Postal;
                TxtPhone = contact.Phone;
                TxtBirthday = contact.Birthday;

                var moduleAView = new Uri("ModifyContactView", UriKind.Relative);
                regionManager.RequestNavigate("ContactViewBase", moduleAView);
            }
        }

        /// <summary>
        /// Apply modification in the local DB
        /// </summary>
        private void DoValidateModCommand()
        {
            var contact = CurrentVignette.Contact;

            contact.FirstName = TxtFirstName;
            contact.LastName = TxtLastName;
            contact.Email = TxtMail;
            contact.Postal = TxtPostal;
            contact.Phone = TxtPhone;
            contact.Birthday = TxtBirthday;

            ContactDAO.SaveContactChanges(contact);
            CurrentVignette.Contact = contact;
            LoadContact();

            ClearFormContact();

            var moduleAView = new Uri("WrapperView", UriKind.Relative);
            regionManager.RequestNavigate("ContactViewBase", moduleAView);
        }

        /// <summary>
        /// Open the Mailer module with the current contact's address to send them a mail
        /// </summary>
        private void DoSendMailCommand()
        {
            var parameter = new NavigationParameters();
            if (CurrentVignette != null)
            {
                parameter.Add("EmailContact", CurrentVignette.Contact.Email);

                var moduleAView = new Uri("MailerViewBase", UriKind.Relative);
                regionManager.RequestNavigate("b", moduleAView, parameter);

                var moduleBView = new Uri("SendView", UriKind.Relative);
                regionManager.RequestNavigate("MailerViewBase", moduleBView, parameter);
            }
        }

        /// <summary>
        /// Filter by Alpha to sort the contacts
        /// </summary>
        private void DoSelectAlphaCommand()
        {
            if (CurrentLetter != null)
            {
                var contacts = ContactDAO.GetContactsByLetter(CurrentLetter);

                Vignettes.Clear();
                foreach (var item in contacts)
                {
                    VignetteControl tmp = new VignetteControl(this);
                    tmp.Init(item);
                    Vignettes.Add(tmp);
                }
            }
        }

        /// <summary>
        /// Load a contact with the form informations then add it to the DB
        /// </summary>
        private void DoValidateCommand()
        {
            Contact contact = new Contact();

            if (txtFirstName != null && txtLastName != null && TxtMail != null && TxtPostal != null && TxtPhone != null && TxtBirthday != null)
            {
                contact.FirstName = TxtFirstName;
                contact.LastName = TxtLastName;
                contact.Email = TxtMail;
                contact.Postal = TxtPostal;
                contact.Phone = TxtPhone;
                contact.Birthday = TxtBirthday;

                ContactDAO.AddContact(contact);

                VignetteControl tmp = new VignetteControl(this);
                tmp.Init(contact);

                Vignettes.Add(tmp);

                ClearFormContact();

                var moduleAView = new Uri("WrapperView", UriKind.Relative);
                regionManager.RequestNavigate("ContactViewBase", moduleAView);
            }
            else
            {
                MessageBox.Show("Remplissez les champs");
            }
        }

        private void DoImageCommand()
        {
            ///here the fct to put the image in the db
        }

        /// <summary>
        /// Clear the form
        /// </summary>
        private void ClearFormContact()
        {
            TxtFirstName = null;
            TxtLastName = null;
            TxtMail = null;
            TxtPostal = null;
            TxtPhone = null;
            TxtBirthday = new DateTime(1950, 01, 01);
        }

        /// <summary>
        /// Return on the main View and clear the form
        /// </summary>
        private void DoCancelCommand()
        {
            var moduleAView = new Uri("WrapperView", UriKind.Relative);
            regionManager.RequestNavigate("ContactViewBase", moduleAView);

            ClearFormContact();
        }

        /// <summary>
        /// Load contacts from the DB in the main View
        /// </summary>
        public void LoadContact()
        {
            Vignettes.Clear();
            var contacts = ContactDAO.GetContactsTable();

            foreach (var item in contacts)
            {
                VignetteControl tmp = new VignetteControl(this);
                tmp.Init(item);
                Vignettes.Add(tmp);
            }
        }



        ///// <summary>
        ///// Cette fct permet de mettre une image en bitmap (utile lorsque l'on veut la mettre en db) !
        ///// </summary>
        ///// <param name="path"></param>
        ///// <returns></returns>
        //private byte[] ImageDB(string path)
        //{
        //    byte[] bmpBytes = null;

        //    using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(path))
        //    using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
        //    {
        //        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        bmpBytes = ms.GetBuffer();
        //    }
        //    return bmpBytes;
        //}
    }
}
