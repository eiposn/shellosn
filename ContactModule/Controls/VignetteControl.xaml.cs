﻿using ContactModule.ViewModels;
using NhibernateORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContactModule.Controls
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class VignetteControl : UserControl
    {
        ContactControl Information { get; set; }

        public ContactViewModel ContactVM;

        public Boolean isOpened;

        private Contact contact;

        public Contact Contact
        {
            get { return contact; }
            set { contact = value; }
        }

        /// <summary>
        /// initializes the class
        /// </summary>
        public VignetteControl(ContactViewModel _contactVM)
        {
            InitializeComponent();
            Information = new ContactControl(_contactVM);
            Info.Children.Add(Information);
            Info.Visibility = Visibility.Collapsed;

            ContactVM = _contactVM;
            this.DataContext = ContactVM;
        }

        public void Init(Contact obj)
        {
            this.Prenom.Content = "Prénom : " + obj.FirstName;
            this.Nom.Content = "Nom : " + obj.LastName;

            contact = obj;

            Information.Init(obj);
        }

        /// <summary>
        /// Switch to Mailer module to send a mail with the contact information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendMail_Button(object sender, RoutedEventArgs e)
        {
        }

        private void Mouse_Enter(object sender, MouseEventArgs e)
        {
            if (isOpened == false)
            {
                Info.Visibility = Visibility.Visible;
                isOpened = true;
            }
            else
            {
                Info.Visibility = Visibility.Collapsed;
                isOpened = false;
            }
        }

    }
}
