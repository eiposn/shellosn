﻿using ContactModule.ViewModels;
using NhibernateORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContactModule.Controls
{
    /// <summary>
    /// Logique d'interaction pour ContactCardControl.xaml
    /// </summary>
    public partial class ContactControl : UserControl
    {
        public ContactViewModel ContactVM;
        /// <summary>
        /// initializes the class
        /// </summary>
        /// 
        public ContactControl(ContactViewModel _contactVM)
        {
            InitializeComponent();
            ContactVM = _contactVM;
            this.DataContext = ContactVM;
        }

        public void Init(Contact obj)
        {
            this.LastName.Content = "Nom : " + obj.LastName;
            this.FirstName.Content = "Prénom : " + obj.FirstName;
            this.Phone.Content = "Téléphone : " + obj.Phone;
            this.Mail.Content = "Email : " + obj.Email;
            this.Birthday.Content = "Anniversaire : " + obj.Birthday;
            this.Postal.Content = "Adresse : " + obj.Postal;
        }
    }
}
