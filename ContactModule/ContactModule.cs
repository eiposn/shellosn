﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using System.Windows;
using Microsoft.Practices.Unity;
using OSN;
using ContactModule.Views;
using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using ContactModule.Controls;
using MailerModule.Views;

namespace ContactModule
{
    [Module(ModuleName = "Contacts", OnDemand = true)]
    public class ContactModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public ContactModule(IRegionManager _regionManager, IUnityContainer _container, TaskBarRegistry _taskbar, IModuleCatalog _catalog)
        {
            IEnumerable<ModuleInfo> module = _catalog.Modules;
            this.regionManager = _regionManager;
            this.container = _container;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            container.RegisterType<Object, ContactViewBase>("ContactViewBase");
            container.RegisterType<Object, MailerViewBase>("MailerViewBase");
            string Imgpath = "";
            foreach(ModuleInfo cur in module)
            {
                if (cur.ModuleName == "Contacts")
                    Imgpath = cur.Ref;

            }

            Imgpath = System.IO.Path.ChangeExtension(Imgpath, "png");
            _taskbar.RegisterMenuItem("ContactViewBase", "Contacts", Imgpath);
        }

        /// <summary>
        /// You must load the views in order for the navigation to resolve
        /// </summary>
        void LoadViewInRegion<TViewType>(string regionName)
        {
            IRegion region = regionManager.Regions[regionName];
            string viewName = typeof(TViewType).Name;

            object view = region.GetView(viewName);
            if (view == null)
            {
                view = container.Resolve<TViewType>();

                region.Add(view, viewName);
            }
        }

        public void Initialize()
        {
            string ribbonRegionName = ConfigurationManager.AppSettings["RibbonRegionName"];
            string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];


            var moduleAView = new Uri("ContactViewBase", UriKind.Relative);
            regionManager.RequestNavigate("b", moduleAView);

            var moduleBView = new Uri("WrapperView", UriKind.Relative);
            regionManager.RequestNavigate("ContactViewBase", moduleBView);
        }
    }
}
