﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.VoIP.VoIPInvitationClasses
{
    [Serializable]
    public class PendingInvitations
    {
        public PendingInvitations(string userName, String FirstName, String LastName, string message, DateTime DateRequest)
        {
            this.UserName = userName;
            this.FirstName = FirstName;
            this.lastName = LastName;
            this.Message = message;
            this.DateOfRequest = DateRequest;
        }

        public String UserName;
        public String FirstName;
        public String lastName;

        public String Message;
        public DateTime DateOfRequest;

        public override string ToString()
        {
            return UserName + " s'appellant " + FirstName + " " + lastName + " " + "souhaite vous inviter en ami : \"" + Message + "\"" + Environment.NewLine + "le : " + DateOfRequest.ToString();
        }
    }
}
