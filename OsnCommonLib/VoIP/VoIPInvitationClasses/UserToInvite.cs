﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.VoIP.VoIPInvitationClasses
{
    [Serializable]
    public class UserToInvite
    {
        public UserToInvite(String firstName, String lastName)
        {
            FirstName = firstName;
            LastName = lastName;

        }

        public String FirstName {get; set;}
        public String LastName { get; set; }
        public String City { get; set; }
        public String Username { get; set; }
        public String FullName
        {
            get
            {
                return (FirstName + LastName);
            }
        }
    }
}
