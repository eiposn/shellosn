﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.VoIP.VoIPInvitationClasses
{
    [Serializable]
    public class InviteUserInfos
    {
       public InviteUserInfos(UserToInvite userInformations, String message)
        {
            UserInformations = userInformations;
            Message = message;
        }
         public UserToInvite UserInformations;
         public String Message;

         public override string ToString()
         {
             return UserInformations.Username + " " + UserInformations.FullName + " " + UserInformations.City + " vous envoie : " + Message;
         }
    }
}
