﻿using CommonLib.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CommonLib.VoIP
{
    [Serializable]
    public class UserIdentifier
    {
        public UserIdentifier(String sessionToken, String username)
        {
            SessionToken = sessionToken;
            Username = username;
            Result = EServiceResult.Success;
        }

        public UserIdentifier(EServiceResult result)
        {
            SessionToken = "";
            Username = "";
            Result = result;
        }

        public String SessionToken { get; set; }
        public String Username { get; set; }
        public EServiceResult Result { get; set; }
    }
}
