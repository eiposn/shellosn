﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib.GlobalClasses;

namespace CommonLib.VoIP
{
    [Serializable]
    public class ContactInfo : UserInfo
    {
        public ContactInfo(String Username, EUserType UserType, String ipAddress, String LocalIpAddress)
            : base(Username, UserType, ipAddress, LocalIpAddress)
        {
            this.ClientStatus = EClientStatus.Offline;
        }

        public ContactInfo(String Username, EUserType UserType, String FirstName, String LastName, String ipAddress, String LocalIpAddress)
            : base(Username, UserType, ipAddress, LocalIpAddress)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.ClientStatus = EClientStatus.Offline;
        }

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String UserIdentifier { get; set; }
        public String FullName 
        {
            get 
            {
                return (FirstName + " " + LastName);
            }
        }

        public override string ToString()
        {
            return Username + " " + UserType.ToString() + " " + ClientStatus.ToString() + " " + IpAddress + " " + FullName;
        }
    }
}
