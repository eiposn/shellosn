﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib.GlobalClasses;
using CommonLib.VoIP.VoIPInvitationClasses;
using CommonLib.VoIP;
using Hik.Communication.ScsServices.Service;

namespace CommonLib.Contract
{
    /// <summary>
    /// This interface defines methods of VoIP Client
    /// that can be called remotely by the Server.
    /// </summary>
    public interface IVoIPClient
    {

        /// <summary>
        /// This method is used to get Contact list from server.
        /// It is called by server once after user logged in to server.
        /// </summary>
        /// <param name="users">All user's contacts informations</param>
        void OnGetContactList(ContactInfo[] users);

        void OnUserInvitation(InviteUserInfos user);

        void OnGetPendingInvitations(PendingInvitations[] invitations);

        /// <summary>
        /// This method is called from chat server to inform that a new user
        /// joined to chat room.
        /// </summary>
        /// <param name="userInfo">Informations of new user</param>
        void OnAskForBridge(UserInfo userInfo);


        /// <summary>
        /// This method is called from chat server to inform that a new user
        /// joined to chat room.
        /// </summary>
        /// <param name="userInfo">Informations of new user</param>
        void OnUserLogin(ContactInfo userInfo);

        /// <summary>
        /// This method is called from chat server to inform that an existing user
        /// has left the chat room.
        /// </summary>
        /// <param name="nick">Informations of user</param>
        void OnUserLogout(String username);

        /// <summary>
        /// This method is called from chat server to inform that a user
        /// changed his/her status.
        /// </summary>
        /// <param name="username">Nick of the user</param>
        /// <param name="newStatus">New status of the user</param>
        void OnUserStatusChange(UserInfo username, string sessionID, EClientStatus newStatus);

        /// <summary>
        /// This method is called by the server in part of the login process
        /// </summary>
        /// <param name="AccountSalt">Represent the public account salt of a client </param>
        /// <param name="SessionSalt">Represent the ephemeral Session Salt of a client</param>
        /// <returns>return the salted password of the user</returns>
        String OnLoginProcess(String AccountSalt, String SessionSalt);

    }
}
