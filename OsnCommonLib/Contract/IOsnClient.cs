﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib.GlobalClasses;
using CommonLib.Osn.OsnCalendarClasses;


namespace CommonLib.Contract
{
    /// <summary>
    /// This interface defines methods of Osn Client
    /// that can be called remotely by the Server.
    /// </summary>
    public interface IOsnClient
    {
        void OnGetPendingCalendarEvents(PendingCalendarEvents[] events);

        /// <summary>
        /// This method is called by the server in part of the login process
        /// </summary>
        /// <param name="AccountSalt">Represent the public account salt of a client </param>
        /// <param name="SessionSalt">Represent the ephemeral Session Salt of a client</param>
        /// <returns>return the salted password of the user</returns>
        String OnLoginProcess(String AcountSalt, String SessionSalt);
    }
}
