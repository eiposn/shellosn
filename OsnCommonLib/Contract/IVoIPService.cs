﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hik.Communication.ScsServices.Service;
using CommonLib.GlobalClasses;
using CommonLib.VoIP.VoIPInvitationClasses;
using CommonLib.VoIP;

namespace CommonLib.Contract
{
    /// <summary>
    /// This interface defines methods of VoIP Service
    /// that can be called remotely by client applications.
    /// </summary>
    [ScsService(Version = "1.0.0.0")]
    public interface IVoIPService
    {
        UserIdentifier Login(UserInfo userInfo);
        void Logout();

        UserToInvite[] SearchUserToInvite(UserToInvite PotentialUser);

        void invitationAccepted(string AcceptedUser);
        EServiceResult InviteUser(InviteUserInfos user);

       EServiceResult InvitationState(String Username, Boolean Result);

        void AskForBridge(UserInfo userinfo, String ContactUserName);
        void ChangeStatus(EClientStatus newStatus);
    }

    [Serializable]
    public enum EServiceResult
    {
        WrongPassword = 0,
        UnknowUser,
        PermissionError,
        Success
    }
}
