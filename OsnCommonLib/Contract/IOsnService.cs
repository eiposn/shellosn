﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib.GlobalClasses;
using Hik.Communication.ScsServices.Service;

namespace CommonLib.Contract
{

    /// <summary>
    /// This interface defines methods of Osn Service
    /// that can be called remotely by client applications.
    /// </summary>
    [ScsService(Version = "1.0.0.0")]
    public interface IOsnService
    {
        bool Login(UserInfo userInfo);
        void Logout();
    }
}
