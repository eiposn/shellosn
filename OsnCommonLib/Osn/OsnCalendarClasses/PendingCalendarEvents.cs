﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Osn.OsnCalendarClasses
{
    [Serializable]
    public class PendingCalendarEvents
    {
        public String EventName;
        public String Place;
        public String Comment;
        public DateTime Date;

        PendingCalendarEvents()
        {

        }

        PendingCalendarEvents(String eventName, String place, String comment, DateTime date)
        {
            EventName = eventName;
            Place = place;
            Comment = comment;
            Date = date;
        }

    }
}
