﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace CommonLib.GlobalClasses.Exceptions
{
    [Serializable]
    public class AuthentificationException : ApplicationException
    {
        /// <summary>
        /// Contstructor.
        /// </summary>
        public AuthentificationException()
        {

        }

        /// <summary>
        /// Contstructor.
        /// </summary>
        public AuthentificationException(SerializationInfo serializationInfo, StreamingContext context)
            : base(serializationInfo, context)
        {

        }

        /// <summary>
        /// Contstructor.
        /// </summary>
        /// <param name="message">Exception message</param>
        public AuthentificationException(string message)
            : base(message)
        {

        }
    }
}
