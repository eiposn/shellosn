﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.GlobalClasses
{

    [Serializable]
    public class UserInfo
    {
        public UserInfo(String Username, EUserType UserType, String IpAddress, String LocalIpAddress)
        {
            this.Username = Username;
            this.UserType = UserType;
            this.IpAddress = IpAddress;
            this.LocalIpAddress = LocalIpAddress;
        }

        public String Username { get; set; }
        public EUserType UserType { get; private set; }
        public EClientStatus ClientStatus { get; set; }
        public String IpAddress { get; set; }
        public String LocalIpAddress { get; set; }

        public override string ToString()
        {
            return Username + " " + UserType.ToString() + " " + ClientStatus.ToString() + " " + IpAddress;
        }
    }

    public enum EClientStatus
    {
        Online,
        Busy,
        Away,
        Offline
    };

    public enum EUserType
    {
        VoIP = 0,
        Administrator = 1
    };

}
