﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System.Configuration;
using VoipModule.Views;
using OSN;
using Microsoft.Practices.ServiceLocation;
using VoipModule.ViewModels;

namespace VoipModule
{
    [Module(ModuleName = "Vidéo Tchat", OnDemand = true)]
    public class VoipModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;
        public VoipModule(IRegionManager _regionManager, IUnityContainer _container, TaskBarRegistry _taskbar, IModuleCatalog _catalog)
        {
            this.regionManager = _regionManager;
            this.container = _container;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            container.RegisterType<Object, VoipView>("VoipView");
            IEnumerable<ModuleInfo> module = _catalog.Modules;
            string Imgpath = "";
            foreach (ModuleInfo cur in module)
            {
                if (cur.ModuleName == "Vidéo Tchat")
                    Imgpath = cur.Ref;

            }

            Imgpath = System.IO.Path.ChangeExtension(Imgpath, "png");
            _taskbar.RegisterMenuItem("VoipView", "Vidéo Tchat", Imgpath);
        }

        public void Initialize()
        {
            string ribbonRegionName = ConfigurationManager.AppSettings["RibbonRegionName"];
            string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];

            LoadViewInRegion<VoipView>("b");
            var moduleAView = new Uri("VoipView", UriKind.Relative);
            regionManager.RequestNavigate("b", moduleAView);

            var moduleBView = new Uri("MessagingAreaControl", UriKind.Relative);
            regionManager.RequestNavigate("ChatView", moduleBView);
        }

        void LoadViewInRegion<TViewType>(string regionName)
        {
            IRegion region = regionManager.Regions[regionName];
            string viewName = typeof(TViewType).Name;

            object view = region.GetView(viewName);
            if (view == null)
            {
                view = container.Resolve<TViewType>();

                region.Add(view, viewName);
            }
        }

    }
}

namespace VoipModule.Controls
{
}
