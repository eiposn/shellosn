﻿using CommonLib.GlobalClasses;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using VoipModule.EventArg;

namespace VoipModule.ViewModels
{
    public class MessagingAreaControlVM : OnRaisePropertyChanged
    {
        private Queue<String> currentContactHistory;
        private VoIPViewModel VoIPVM;
        public event EventHandler<UpdateHistoryEventArg> UpdateHistory;
        public event EventHandler<AddMessageToHistoryEventArg> UpdateMessage;
        public event EventHandler<SwitchStateOfCallButtonEventArg> UpdateCallButton;


        private string _messageToSend;
        public string MessageToSend
        {
            get { return _messageToSend; }
            set
            {
                _messageToSend = value;
                RaisePropertyChanged("MessageToSend");
            }
        }

        private string _messageReceived;
        public string MessageReceived
        {
            get
            {
                return _messageReceived;
            }
            set
            {
                _messageReceived = value;
                AddMessageReceivedToHistory();
            }
        }

        #region Command buttons
        private DelegateCommand _sendCommand;

        public DelegateCommand SendCommand
        {
            get
            {
                return _sendCommand;
            }
        }

        private DelegateCommand _callCommand;

        public DelegateCommand CallCommand
        {
            get
            {
                return _callCommand;
            }
        }

        /// <summary>
        /// Returns true if a contact is currently selected
        /// </summary>
        public Boolean IsSelected()
        {
            return VoIPVM.IsContactSelected;
        }

        public Boolean Selected()
        {
            return true;
        }
        #endregion

        public void OnUpdateContactList()
        {
            var handler = UpdateHistory;

            UpdateHistoryEventArg myevent = new UpdateHistoryEventArg();
            myevent.ContactHistory = currentContactHistory;
            if (handler != null)
            {
                handler(this, myevent);
            }
        }

        public MessagingAreaControlVM(VoIPViewModel parent)
        {
            this.VoIPVM = parent;
            this.VoIPVM.InitMessagingArea(this);
            _sendCommand = new DelegateCommand(SendMessage, Selected);
            _callCommand = new DelegateCommand(Call, Selected);
        }

        private void Call()
        {
            if (VoIPVM.IsContactConnected() == false)
            {
                //TODO mettre un message disant que le contact n'est pas connecté et qu'il est impossible de lui parler
            }
            VoIPVM.CallCurrentContact();
        }

        public ICommand CallButtonAction
        {
            get
            {
                return new DelegateCommand<object>((args) =>
                {
                    if (VoIPVM.IsContactConnected() == false)
                    {
                        ((ToggleButton)args).IsChecked = false;
                        //TODO mettre un message disant que le contact n'est pas connecté et qu'il est impossible de lui parler
                    }
                    // Get Password as Binding not supported for control-type PasswordBox
                    else if ((bool)((ToggleButton)args).IsChecked)
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    ((ToggleButton)args).Content = "Raccrocher";
                                }));
                        VoIPVM.CallCurrentContact();
                        //VoIPVM.SwitchStateOfTheCallHangupButton();
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            ((ToggleButton)args).Content = "Appeler";
                        }));
                        VoIPVM.HangupCurrentContact();
                        //VoIPVM.SwitchStateOfTheCallHangupButton();
                    }
                });
            }
        }

        public void ChangeHistory(Queue<String> contactHistory)
        {

            currentContactHistory = new Queue<String>(contactHistory);
            OnUpdateContactList();
        }

        private void AddMessageToSendToHistory()
        {
            var handler = UpdateMessage;

            AddMessageToHistoryEventArg myevent = new AddMessageToHistoryEventArg();
            myevent.MessageToAdd = MessageToSend;
            myevent.UserName = VoIPVM.UserNamevm;
            if (handler != null)
            {
                handler(this, myevent);
            }
            MessageToSend = "";
        }

        private void AddMessageReceivedToHistory()
        {
            var handler = UpdateMessage;

            AddMessageToHistoryEventArg myevent = new AddMessageToHistoryEventArg();
            myevent.MessageToAdd = MessageReceived;
            myevent.UserName = VoIPVM.UserNamevm;
            if (handler != null)
            {
                handler(this, myevent);
            }
            MessageToSend = "";
        }

        public void SendMessage()
        {
            if (VoIPVM.IsContactConnected() == false)
            {
                //TODO mettre un message disant que le contact n'est pas connecté et qu'il est impossible de lui parler
            }
            if (!string.IsNullOrEmpty(_messageToSend) && (IsSelected() == true))
            {
                VoIPVM.AddMessageToCurrentContactHistory(VoIPVM.UserNamevm, MessageToSend);
                VoIPVM.SendMessageToCurrentContact(_messageToSend);
                AddMessageToSendToHistory();
            }
        }

        public void SwitchStateOfTheCallHangupButton()
        {
            var handler = UpdateCallButton;

            SwitchStateOfCallButtonEventArg myevent = new SwitchStateOfCallButtonEventArg();
            if (handler != null)
            {
                handler(this, myevent);
            }
        }
    }
}