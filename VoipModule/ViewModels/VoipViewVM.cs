﻿using CommonLib.GlobalClasses;
using CommonLib.VoIP;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using VoipModule.Models;
using VoipModule.Views.UserControls;
//using Microsoft.TeamFoundation.MVVM;
using VoipModule.Services.ClientToServer;
using VoipModule.Services;
using VoipModule.EventArg;
using System.Windows.Documents;
using System.Windows.Media;
using VoipModule.Services.ClientToClient;
using Hik.Communication.ScsServices.Service;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using P2PCommonLib.Contracts;
using Microsoft.Practices.Prism.Commands;
using System.Reflection;
using System.Resources;
using VoipModule.Audio_Communication;
using System.Net;
using System.Text.RegularExpressions;
using System.Configuration;
using CommonLib.VoIP.VoIPInvitationClasses;

namespace VoipModule.ViewModels
{
    public class VoIPViewModel : OnRaisePropertyChanged
    {
        #region private propreties

        private MessagingAreaControlVM MessageAreaVM;
        private Thread VoIPThread;
        private Thread HostThread;
        private List<Thread> VoIPChatListThread;
        private VoIPClient _VoipClient;
        private ContactInformations[] contactList;

        private VoIPHost _VoIPHost;
        private VoIPPeer _VoIPPeer;
        private VoIPChatServiceController _VoIPChatController;

        private List<Thread> ChatServices;

        private UserIdentifier personalUserIdentifier;

        private string publicIp;

        public string PublicIp
        {
            get
            {
                if (String.IsNullOrEmpty(publicIp))
                {
                    VoIPServiceController VoipService = (VoIPServiceController)Service;
                    publicIp = VoipService.PublicIp;
                }
                return publicIp;
            }
        }

        /// <summary>
        /// This object is used to host Chat Service on a SCS server.
        /// </summary>
        private IScsServiceApplication _serviceApplication;
        private IVoIPServiceController Service { get; set; }

        #endregion

        #region public propreties

        public ConnectionCommand Delete { get; set; }
        public event EventHandler<UpdateContactListEventArg> UpdateContactList;
        public event EventHandler<UpdateContactStatusEventArgs> UpdateContactStatus;
        public event EventHandler<MessageReceivedFromContactEventArg> NewMessageFromCOntact;

        public UserIdentifier personnalUserIdentifier
        {
            get
            {
                VoIPServiceController VoipService = (VoIPServiceController)Service;
                return VoipService.My_userIdentifier;
            }
        }

        public bool CanDelete
        {
            get { return _currentContact != null; }
        }

        #endregion

        #region private propreties with public accessor

        private Boolean IsSelected;

        public Boolean IsContactSelected
        {
            get
            {
                return IsSelected;
            }
        }

        private bool firstTime = false;


        private string _UserName = ConfigurationManager.AppSettings["UserName"].ToString();
        public string UserNamevm
        {
            get
            {
                return (_UserName);
            }
            set
            {
                _UserName = value;
                RaisePropertyChanged("UserNamevm");
            }
        }

        private EClientStatus _CurrentStatus = EClientStatus.Online;
        public int CurrentStatus
        {
            get
            {
                return ((int)_CurrentStatus);
            }
            set
            {
                _CurrentStatus = (EClientStatus)value;
                Service.ChangeStatus((EClientStatus)value); //TODO check if the call the server to change the status works
                RaisePropertyChanged("CurrentStatus");
            }
        }

        private ContactInformations _currentContact;
        public ContactInformations CurrentContact
        {
            get { return _currentContact; }
            set
            {
                _currentContact = value;
                RaisePropertyChanged("CurrentContact");
            }
        }
        #endregion

        #region constructeur destructeur Init

        public VoIPViewModel()
        {
            MessageAreaVM = null;
            IsSelected = false;
            _currentContact = null;
        }

        public void Init()
        {
            string usr = ConfigurationManager.AppSettings["UserName"].ToString();
            string testpsw = ConfigurationManager.AppSettings["PassWord"].ToString();
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["UserName"].ToString()) && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["PassWord"].ToString()))
            {
                OnLoginInProcess();
                //we need to try to login;
                VoIPThread = new Thread(HandleVoipServices);
                VoIPThread.Start();
            }
        }

        public void InitServices()
        {
            ChatServices = new List<Thread>();

            HostThread = new Thread(HandleHostP2PServices);
            //VoIPChatThread = new Thread(HandleVoipChatServices);

            _VoIPHost = null;
            _VoIPPeer = null;
            _VoIPChatController = null;

            // Start the threads.

            //VoIPChatThread.Start();
        }

        public void InitMessagingArea(MessagingAreaControlVM msgAreaVM)
        {
            this.MessageAreaVM = msgAreaVM;
        }

        public void Destrutor()
        {
            Service.Disconnect();
        }

        ~VoIPViewModel()
        {
            try
            {
                Service.Disconnect();
                VoIPThread.Abort();
                for (int i = 0; i < ChatServices.Count(); i++)
                {
                    ChatServices[i].Abort();
                }
                HostThread.Abort();
            }
            catch
            {
            }
            //VoIPChatThread.Abort();
        }

        /*protected override void Finalize()
        {
            try
            {
                Service.Disconnect();
            }
            finally
            {
            }
        }*/
        #endregion

        #region private methods
        private void VoIPService_UpdateContactList(object sender, UpdateContactListEventArg e)
        {
            List<ContactInformations> tmplist = new List<ContactInformations>();
            List<UserIdentifier> tmpUserIdentifier = new List<UserIdentifier>();
            foreach (ContactInfo tmpct in e.ContactList)
            {
                ContactInformations tmpCtToAdd = new ContactInformations(tmpct);
                Application.Current.Dispatcher.Invoke(new Action(() => tmpCtToAdd.Init(this)));
                tmplist.Add(tmpCtToAdd);
                UserIdentifier tmpIdentifier = new UserIdentifier(tmpCtToAdd.UserIdentifier, tmpCtToAdd.Username);
                tmpUserIdentifier.Add(tmpIdentifier);
            }
            contactList = tmplist.ToArray<ContactInformations>();

            /*if (_VoIPHost == null)
                _VoIPHost = new VoIPHost(tmpUserIdentifier.ToArray<UserIdentifier>());
            else
            {
                // TODO appeller un fonction pour update le tableau de useridentifier
            }*/

            OnUpdateContactList(); //reçois la liste de contact


            //init le service d'Hosting pour la communication par chat
            while (HostThread == null) ;
            if (firstTime != true)
            {
                HostThread.Start(tmpUserIdentifier.ToArray<UserIdentifier>());
                firstTime = true;
            }
            else
            {
                _VoIPHost.AddNewContact(tmpUserIdentifier.ToArray<UserIdentifier>());
            }
            //InitVoipChatServices(tmpUserIdentifier.ToArray<UserIdentifier>());
        }

        private void UserCardClick(UserCardControl usr)
        {
            foreach (ContactInformations ct in contactList)
            {
                if (ct.Username.CompareTo(usr.UserNick) == 0)
                {
                    _currentContact = ct;
                    IsSelected = true;
                    if (MessageAreaVM != null)
                    {
                        MessageAreaVM.ChangeHistory(_currentContact.History);
                    }
                }
            }
        }

        #region Thread Service serveur communication

        private void HandleHostP2PServices(object contacts)
        {
            InitVoipChatServices((UserIdentifier[])contacts);
        }



        private void HandleVoipServices()
        {
            Service = new VoIPServiceController();
            _VoipClient = new VoIPClient();
            _VoipClient.UpdateContactList += VoIPService_UpdateContactList;
            _VoipClient.UpdateContactStatus += VoIPService_UpdateContactStatus;
            _VoipClient.GetOneInvitation += VoIPService_OnGetOneInvitation;
            _VoipClient.GetPendingInvitation += VoIPService_OnGetPendingInvitation;

            Boolean result = Service.Connect(_VoipClient);
            if (result == false)
            {
                OnEndLogin(result);
                VoIPThread.Abort();
            }
            else
            {
                InitServices();
            }
            OnEndLogin(result);
        }

        private void HandleCLientP2PServices(object messageToSend)
        {
            ConnectToContact();
            VoIPServiceController VoipService = (VoIPServiceController)Service;
            UserIdentifier usr = new UserIdentifier(VoipService.My_userIdentifier.SessionToken, VoipService.My_userIdentifier.Username);
            if (messageToSend != null)
                _currentContact._voipChatService.SendTextMessage(usr, (string)messageToSend);
            else
            {
                _currentContact._voipChatService.StartCallProtocol(VoipService.My_userIdentifier.Username);
                audio = new AudioConnection();
                if (CurrentContact.IpAddress != PublicIp)
                {
                    UpnpForward upnp = new UpnpForward();
                    upnp.launch();
                    audio.Connect(CurrentContact.IpAddress);
                }
                else
                {
                    audio.Connect(CurrentContact.LocalIpAddress);
                }
            }
        }

        #endregion

        #region P2P

        private void InitVoipChatServices(UserIdentifier[] contacts)
        {
            try
            {
                _serviceApplication = ScsServiceBuilder.CreateService(new ScsTcpEndPoint(62345));
                _VoIPHost = new VoIPHost(contacts, this);
                _serviceApplication.AddService<IVoIPHost, VoIPHost>(_VoIPHost);
                _serviceApplication.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Service can not be started. Exception detail: " + ex.Message);
                //MessageBox.Show("Service can not be started. Exception detail: " + ex.Message);
                return;
            }
        }

        private void ConnectToContact()
        {
            VoIPServiceController VoipService = (VoIPServiceController)Service;
            //if (_currentContact.IpAddress.CompareTo(PublicIp) == 0)
            {
                _currentContact._voipChatService.Connect(_currentContact._peerChatService, VoipService.My_userIdentifier, _currentContact.LocalIpAddress);
            }
            /*else
            {
                _currentContact._voipChatService.Connect(_currentContact._peerChatService, VoipService.My_userIdentifier, _currentContact.IpAddress);
            }*/
            //TODO mettre un autre system de connection
            _currentContact.ConnectionState = EConnectionState.Connected;
        }

        public void ConnectionHosting(string userName)
        {
            foreach (ContactInformations ct in contactList)
            {
                if (ct.Username.CompareTo(userName) == 0)
                {
                    if (_currentContact == ct)
                        _currentContact.ConnectionState = EConnectionState.Hosting;
                    else
                        ct.ConnectionState = EConnectionState.Hosting;
                }
            }
        }
        #endregion


        #endregion

        #region public method

        /// <summary>
        /// Adds a new message to message history.
        /// </summary>
        /// <param name="nick">Nick of sender</param>
        /// <param name="message">Message</param>
        public void AddMessageToCurrentContactHistory(string nick, string message)
        {
            _currentContact.AddMessageToHistory(nick, message);
        }

        public ContactInformations GetContact(string userName)
        {
            foreach (ContactInformations ct in contactList)
            {
                if (ct.Username.CompareTo(userName) == 0)
                {
                    return ct;
                }
            }
            return null;
        }

        public void ReceiveMessage(string username, string message)
        {
            ContactInformations tmpct;
            foreach (ContactInformations ct in contactList)
            {
                if (ct.Username.CompareTo(username) == 0)
                {
                    tmpct = ct;
                    ct.ReceiveMessage(username + " : " + message);
                }
            }
        }

        public void OnUpdateContactList()
        {
            var handler = UpdateContactList;

            UpdateContactListEventArg UCLEA = new UpdateContactListEventArg();
            UCLEA.ContactList = (ContactInfo[])contactList;
            if (handler != null)
            {
                handler(this, UCLEA);
            }
        }

        public void OnUpdateContactStatus(UserInfo userInfo, string sessionID, EClientStatus ContactNewStatus)
        {


            var handler = UpdateContactStatus;

            UpdateContactStatusEventArgs UCS = new UpdateContactStatusEventArgs();
            UCS.UserInformations = userInfo;
            UCS.useridentifier = sessionID;
            UCS.ClientNewStatus = ContactNewStatus;
            if (handler != null)
            {
                handler(this, UCS);
            }
        }

        /// <summary>
        /// Handles MouseDoubleClick event of all User cards.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        public void UserCard_MouseSimpleClick(UserCardControl usr)
        {
            if (_currentContact == null)
            {
                UserCardClick(usr);
            }
            else if (usr.UserNick.CompareTo(_currentContact.Username) != 0)
            {
                UserCardClick(usr);
            }
        }

        public void cmbCurrentUserStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //TODO call the server to change the status
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not changes status. Error Detail: " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public Boolean IsContactConnected()
        {
            if (_currentContact == null)
                return false;
            if (_currentContact.ClientStatus == EClientStatus.Offline)
            {
                return false;
            }
            return true;
        }

        #region SendMessage
        public void SendMessageToCurrentContact(string messageToSend)
        {
            //If the connection has not been made
            if (_currentContact.ConnectionState == EConnectionState.NoConnection && _currentContact.ClientStatus != EClientStatus.Offline)
            {
                Thread tmp = new Thread(HandleCLientP2PServices);
                tmp.Start(messageToSend);
                ChatServices.Add(tmp);
            }

            else if (_currentContact.ConnectionState == EConnectionState.Connected)
            {
                VoIPServiceController VoipService = (VoIPServiceController)Service;
                UserIdentifier usr = new UserIdentifier(VoipService.My_userIdentifier.SessionToken, VoipService.My_userIdentifier.Username);
                _currentContact._voipChatService.SendTextMessage(usr, messageToSend);
            }
            else if (_currentContact.ConnectionState == EConnectionState.Hosting)
            {
                _VoIPHost.SendMessageToContact(_currentContact.UserIdentifier, messageToSend);
            }
        }

        #endregion

        #endregion

        #region MessagingArea VM
        public void PutReceivedMessageToView(string userName, string message)
        {
            if (_currentContact != null)
            {
                if (_currentContact.Username == userName)
                {
                    Application.Current.Dispatcher.Invoke(new Action(() => MessageAreaVM.ChangeHistory(_currentContact.History)));
                }
                else
                {
                    var handler = NewMessageFromCOntact;

                    MessageReceivedFromContactEventArg e = new MessageReceivedFromContactEventArg();
                    e.UserName = userName;
                    if (handler != null)
                    {
                        handler(this, e);
                    }
                }
            }
            else
            {
                var handler = NewMessageFromCOntact;

                MessageReceivedFromContactEventArg e = new MessageReceivedFromContactEventArg();
                e.UserName = userName;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
        }

        private void VoIPService_UpdateContactStatus(object sender, UpdateContactStatusEventArgs e)
        {
            for (int i = 0; i <= contactList.Length - 1; ++i)
            {
                if (contactList[i].Username.CompareTo(e.UserInformations.Username) == 0)
                {
                    contactList[i].UserIdentifier = e.useridentifier;
                    contactList[i].LocalIpAddress = e.UserInformations.LocalIpAddress;
                    contactList[i].IpAddress = e.UserInformations.IpAddress;
                    contactList[i].ClientStatus = e.UserInformations.ClientStatus;
                }
            }
            _VoIPHost.UpdateContactInfo(e.UserInformations, e.useridentifier);
            this.OnUpdateContactStatus(e.UserInformations, e.useridentifier, e.ClientNewStatus);
        }
        #endregion

        #region login

        public event EventHandler<EventArgs> LoginInProcess;
        public event EventHandler<EndLoginEventArg> EndLogin;

        private string _pseudo = ConfigurationManager.AppSettings["UserName"].ToString();

        public string Pseudo
        {
            get
            {
                return _pseudo;
            }
            set
            {
                if (_pseudo != value)
                {
                    _pseudo = value;
                    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["UserName"].Value = value;
                    config.Save(ConfigurationSaveMode.Modified);
                }
            }
        }

        public void OnLoginInProcess()
        {
            var handler = LoginInProcess;

            EventArgs e = new EventArgs();
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void OnEndLogin(bool status)
        {
            var handler = EndLogin;

            EndLoginEventArg e = new EndLoginEventArg();
            e.status = status;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                return new DelegateCommand<object>((args) =>
                {
                    OnLoginInProcess();
                    // Get Password as Binding not supported for control-type PasswordBox
                    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["PassWord"].Value = ((PasswordBox)args).Password;
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");

                    //string LoginPassword = ((PasswordBox)args).Password;
                    //Thread.Sleep(5000);
                    // Rest of code here
                    VoIPThread = new Thread(HandleVoipServices);
                    VoIPThread.Start();
                });
            }
        }
        #endregion

        #region audioCall

        public AudioConnection audio;

        public void CallCurrentContact()
        {
            //If the connection has not been made
            if (_currentContact.ConnectionState == EConnectionState.NoConnection && _currentContact.ClientStatus != EClientStatus.Offline)
            {
                Thread tmp = new Thread(HandleCLientP2PServices);
                tmp.Start(null);
                ChatServices.Add(tmp);
            }
            else if (_currentContact.ConnectionState == EConnectionState.Connected)
            {
                VoIPServiceController VoipService = (VoIPServiceController)Service;
                _currentContact._voipChatService.StartCallProtocol(VoipService.My_userIdentifier.Username);
                audio = new AudioConnection();
                //if (CurrentContact.IpAddress != PublicIp)
                {
                    UpnpForward upnp = new UpnpForward();
                    upnp.launch();
                    audio.Connect(CurrentContact.IpAddress);
                }
                /*else
                {
                    audio.Connect(CurrentContact.LocalIpAddress);
                }*/
            }
            else if (_currentContact.ConnectionState == EConnectionState.Hosting)
            {
                VoIPServiceController VoipService = (VoIPServiceController)Service;
                _VoIPHost.StartCallProtocol(_currentContact.Username);
                audio = new AudioConnection();
                //if (CurrentContact.IpAddress != PublicIp)
                {
                    UpnpForward upnp = new UpnpForward();
                    upnp.launch();
                    audio.Connect(CurrentContact.IpAddress);
                }
                /*else
                {
                    audio.Connect(CurrentContact.LocalIpAddress);
                }*/
            }
        }

        public void HangupCurrentContact()
        {
            //If the connection has not been made
            if (_currentContact.ConnectionState == EConnectionState.NoConnection)
            {
                //Problem
            }

            else if (_currentContact.ConnectionState == EConnectionState.Connected)
            {
                VoIPServiceController VoipService = (VoIPServiceController)Service;
                _currentContact._voipChatService.StopCallProtocol(VoipService.My_userIdentifier.Username);
                audio.Disconnect();
            }
            else if (_currentContact.ConnectionState == EConnectionState.Hosting)
            {
                _VoIPHost.HangupCall(_currentContact.UserIdentifier);
                audio.Disconnect();
                //TODO call on send
            }
        }

        public void SwitchStateOfTheCallHangupButton()
        {
            MessageAreaVM.SwitchStateOfTheCallHangupButton();
        }

        #endregion

        #region SendInvitation

        private string userFirstNameToSearch;

        public string UserFirstNameToSearch
        {
            get { return userFirstNameToSearch; }
            set
            {
                if (userFirstNameToSearch != value)
                    userFirstNameToSearch = value;
            }
        }

        private string userLastNameToSearch;

        public string UserLastNameToSearch
        {
            get { return userLastNameToSearch; }
            set
            {
                if (userLastNameToSearch != value)
                    userLastNameToSearch = value;
            }
        }

        private string userNameIsInvitingYou;

        public string UserNameIsInvitingYou
        {
            get { return userNameIsInvitingYou; }
            set
            {
                if (userNameIsInvitingYou != value)
                {
                    userNameIsInvitingYou = value;
                    RaisePropertyChanged("UserNameIsInvitingYou");
                }
            }
        }

        private string invitationMessage;

        public string InvitationMessage
        {
            get { return invitationMessage; }
            set
            {
                if (invitationMessage != value)
                {
                    invitationMessage = value;
                    RaisePropertyChanged("InvitationMessage");
                }
            }
        }

        private String userNameInviting;

        public String UserNameInviting
        {
            get
            {
                return userNameInviting;
            }
            set
            {
                if (userNameInviting != value)
                    userNameInviting = value;
            }
        }


        public event EventHandler<UpdateUsersFoundEventArg> ListFound;
        public event EventHandler<InvitationSentEventArgs> InvitationSent;
        public event EventHandler<GetOneInvitationEventArg> GetInvitation;
        public event EventHandler<InvitationAcceptedOrDecliendEventArg> InvitationTerminted;

        public ICommand SearchUsers
        {
            get
            {
                return new DelegateCommand<object>((arg) =>
                {
                    if (String.IsNullOrEmpty(UserFirstNameToSearch) || String.IsNullOrEmpty(UserLastNameToSearch))
                        return;

                    VoIPServiceController VoipService = (VoIPServiceController)Service;
                    UserToInvite[] found = VoipService.SearchUsersoInvite(UserFirstNameToSearch, UserLastNameToSearch);

                    var handler = ListFound;

                    UpdateUsersFoundEventArg e = new UpdateUsersFoundEventArg();
                    e.users = found;
                    if (handler != null)
                    {
                        handler(this, e);
                    }

                    // demander au serveur si des personnes correspondent aux noms 
                    ((Border)arg).Visibility = Visibility.Visible;
                });
            }
        }

        public ICommand InviteUser
        {
            get
            {
                return new DelegateCommand<object>((arg) =>
                {
                    VoIPServiceController VoipService = (VoIPServiceController)Service;
                    UserToInvite tmpusr = new UserToInvite(CurrentContactToInvite.LastName, CurrentContactToInvite.FirstName);
                    tmpusr.Username = CurrentContactToInvite.UserNick;
                    VoipService.SendInvitationToUser(tmpusr);

                    var handler = InvitationSent;

                    InvitationSentEventArgs e = new InvitationSentEventArgs();
                    if (handler != null)
                    {
                        handler(this, e);
                    }

                    // demander au serveur si des personnes correspondent aux noms 
                    ((StackPanel)arg).Visibility = Visibility.Hidden;
                    ((StackPanel)arg).Children[1].Visibility = Visibility.Hidden;
                });
            }
        }

        public ICommand AcceptClick
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    VoIPServiceController VoipService = (VoIPServiceController)Service;
                    VoipService.AccepteOrDeclineInvitation(userNameInviting, true);

                    var handler = InvitationTerminted;
                    InvitationAcceptedOrDecliendEventArg e = new InvitationAcceptedOrDecliendEventArg();
                    e.isAccepted = true;
                    e.userName = UserNameInviting;
                    if (handler != null)
                    {
                        handler(this, e);
                    }
                });
            }
        }

        public ICommand DeclineClick
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    VoIPServiceController VoipService = (VoIPServiceController)Service;
                    VoipService.AccepteOrDeclineInvitation(userNameInviting, false);

                    var handler = InvitationTerminted;
                    InvitationAcceptedOrDecliendEventArg e = new InvitationAcceptedOrDecliendEventArg();
                    e.isAccepted = false;
                    e.userName = UserNameInviting;
                    if (handler != null)
                    {
                        handler(this, e);
                    }
                });
            }
        }

        private PendingInvitations[] pendingInvitations;

        public PendingInvitations[] MyPendingInvitations
        {
            get { return pendingInvitations; }
            set 
            {
                if (pendingInvitations != value)
                    pendingInvitations = value;
            }
        }

        private UserCardControl _currentContactToInvite;
        public UserCardControl CurrentContactToInvite
        {
            get { return _currentContactToInvite; }
            set
            {
                _currentContactToInvite = value;
                RaisePropertyChanged("CurrentContact");
            }
        }

        /// <summary>
        /// Handles MouseDoubleClick event of all User cards.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        public void UserCardFound_MouseSimpleClick(UserCardControl usr)
        {
            if (_currentContactToInvite == null)
            {
                _currentContactToInvite = usr;
            }
            else if (usr.UserNick.CompareTo(_currentContactToInvite.UserNick) != 0)
            {
                _currentContactToInvite = usr;
            }
        }

        private void VoIPService_OnGetOneInvitation(object sender, GetOneInvitationEventArg e)
        {
            userNameInviting = e.usr.UserInformations.Username;

            UserNameIsInvitingYou = e.usr.UserInformations.FullName + " alias " + userNameInviting + " shouaite vous ajouter en ami :";
            InvitationMessage = e.usr.Message;

            var handler = GetInvitation;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void VoIPService_OnGetPendingInvitation(object sender, GetPendingInvitationEventArg e)
        {
            userNameInviting = e.pendingInvitations[0].UserName;
            MyPendingInvitations = e.pendingInvitations;

            UserNameIsInvitingYou = e.pendingInvitations[0].FirstName + " " + e.pendingInvitations[0].lastName + " alias " + userNameInviting + " shouaite vous ajouter en ami :";
            InvitationMessage = e.pendingInvitations[0].Message;

            var handler = GetInvitation;

            GetOneInvitationEventArg tmp = new GetOneInvitationEventArg();
            tmp.usr = null;
            if (handler != null)
            {
                handler(this, tmp);
            }
        }

        #endregion
    }
}
