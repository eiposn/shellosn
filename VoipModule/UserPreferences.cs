﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule
{
    /// <summary>
    /// This class is used to save and load preferences of the user.
    /// </summary>
    [Serializable]
    internal class UserPreferences
    {
        /// <summary>
        /// Used to synronize threads while creating singleton object.
        /// </summary>
        private static readonly object SyncObj = new object();

        /// <summary>
        /// Gets the singleton instance of this class.
        /// </summary>
        public static UserPreferences Current
        {
            get
            {
                if (_current == null)
                {
                    lock (SyncObj)
                    {
                        if (_current == null)
                        {
                            _current = LoadPreferences();
                        }
                    }
                }

                return _current;
            }
        }

        /// <summary>
        /// Nick of the user.
        /// </summary>
        public string Nick { get; set; }

        /// <summary>
        /// Path of user's avatar file.
        /// </summary>
        public string AvatarFile { get; set; }

        /// <summary>
        /// Ip address of the chat server.
        /// </summary>
        public string ServerIpAddress { get; set; }

        /// <summary>
        /// TCP port of the chat server.
        /// </summary>
        public int ServerTcpPort { get; set; }

        /// <summary>
        /// The singleton instance of this class.
        /// </summary>
        private static UserPreferences _current;

        /// <summary>
        /// Constructor.
        /// </summary>
        private UserPreferences()
        {
        }

        /// <summary>
        /// Saves preferences to the disc.
        /// </summary>
        public void Save()
        {
            try
            {
                ClientHelper.SerializeObjectToFile(
                    this,
                    Path.Combine(ClientHelper.GetCurrentDirectory(), "Preferences.bin")
                    );
            }
            catch
            {

            }
        }

        /// <summary>
        /// Load last preferences from the disc.
        /// </summary>
        /// <returns>Last user preferences (or default values if not found)</returns>
        private static UserPreferences LoadPreferences()
        {
            try
            {
                var preferenceFile = Path.Combine(ClientHelper.GetCurrentDirectory(), "Preferences.bin");
                if (File.Exists(preferenceFile))
                {
                    return (UserPreferences)ClientHelper.DeserializeObjectFromFile(preferenceFile);
                }
            }
            catch
            {

            }

            return null;
        }
    }
}
