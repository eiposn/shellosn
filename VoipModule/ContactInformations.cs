﻿using CommonLib.VoIP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using VoipModule.Services.ClientToClient;
using VoipModule.ViewModels;

namespace VoipModule
{
    public class ContactInformations : ContactInfo
    {
        public Queue<String> History;
        public EConnectionState ConnectionState;
        public VoIPChatServiceController _voipChatService;
        public VoIPPeer _peerChatService;

        public ContactInformations(ContactInfo ct)
            : base(ct.Username, ct.UserType, ct.IpAddress, ct.LocalIpAddress)
        {
            /*ContactInfo propreties*/
            this.FirstName = ct.FirstName;
            this.LastName = ct.LastName;
            this.UserIdentifier = ct.UserIdentifier;

            /*UserInfo propreties*/
            this.ClientStatus = ct.ClientStatus;

            /*ContactInformations propreties*/
            History = null;
            ConnectionState = EConnectionState.NoConnection;
            _voipChatService = null;
            _peerChatService = null;
        }

        public void Init(VoIPViewModel voipVM)
        {
            History = new Queue<String>();
            _voipChatService = new VoIPChatServiceController();
            _peerChatService = new VoIPPeer(voipVM);
        }

        /// <summary>
        /// Adds a new message to message history.
        /// </summary>
        /// <param name="nick">Nick of sender</param>
        /// <param name="message">Message</param>
        public void AddMessageToHistory(string nick, string message)
        {
            String messageToAdd = nick + " : " + message;
            History.Enqueue(messageToAdd);
            if (History.Count() > 150)
            {
                History.Dequeue();
            }
        }

        public void ReceiveMessage(string message)
        {
            String messageToAdd = message;
            History.Enqueue(messageToAdd);
            if (History.Count() > 150)
            {
                History.Dequeue();
            }
        }
    }

    public enum EConnectionState
    {
        NoConnection,
        Hosting,
        Connected
    };
}
