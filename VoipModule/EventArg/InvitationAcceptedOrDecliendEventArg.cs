﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.EventArg
{
    public class InvitationAcceptedOrDecliendEventArg : EventArgs
    {
        public bool isAccepted;
        public string userName;
    }
}
