﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.EventArg
{
    public class AddMessageToHistoryEventArg : EventArgs
    {
        public string UserName { get; set; }
        public string MessageToAdd { get; set; }
    }
}
