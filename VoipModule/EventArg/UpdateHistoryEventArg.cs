﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace VoipModule.EventArg
{
    public class UpdateHistoryEventArg : EventArgs
    {
        public Queue<String> ContactHistory { get; set; }
    }
}
