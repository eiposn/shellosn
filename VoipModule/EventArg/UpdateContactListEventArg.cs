﻿using CommonLib.VoIP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.EventArg
{
    public class UpdateContactListEventArg : EventArgs
    {
        public ContactInfo[] ContactList { get; set; }
    }
}
