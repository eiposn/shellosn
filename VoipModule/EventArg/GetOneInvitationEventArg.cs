﻿using CommonLib.VoIP.VoIPInvitationClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.EventArg
{
    public class GetOneInvitationEventArg : EventArgs
    {
        public InviteUserInfos usr;
    }
}
