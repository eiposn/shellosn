﻿using CommonLib.GlobalClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.EventArg
{
    public class UpdateContactStatusEventArgs : EventArgs
    {
        public UserInfo UserInformations { get; set; }
        public String useridentifier { get; set; }
        public EClientStatus ClientNewStatus { get; set; }
    }
}
