﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using VoipModule;
using VoipModule.Services.ClientToServer;

namespace VoipModule.Models
{
    public class ConnectionCommand : ICommand
    {
        private readonly IVoIPServiceController _serviceVoip;
        /*private readonly Func<bool> _canExecute;
        private readonly Action<VoipContactModel> _deleted;*/

        public event EventHandler CanExecuteChanged;

        public ConnectionCommand(IVoIPServiceController service/*, Func<bool> canExecute*/)
        {
            _serviceVoip = service;
            //_canExecute = canExecute;
            //_deleted = deleted;
        }

        public bool CanExecute(object parameter)
        {
            //return _canExecute();
            return true;
        }

        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                var contact = parameter as VoipContactModel;
                if (contact != null)
                {
                    var result = MessageBox.Show("Are you sure you wish to delete the contact?",
                                                              "Confirm Delete", MessageBoxButton.OKCancel);

                    if (result.Equals(MessageBoxResult.OK))
                    {
                        //_serviceVoip.DeleteContact(contact);
                    }
                }
            }
        }

        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}
