﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using VoipModule.Services;
using System.IO;
using CommonModule.BaseTypes;
using VoipModule.ViewModels;
using VoipModule.EventArg;
using System.Windows.Threading;


namespace VoipModule.Views.UserControls
{
    /// <summary>
    /// This control is used to show incoming messages and write new messages.
    /// </summary>
    public partial class MessagingAreaControl : UserControl
    {
        #region Public Properties

        #endregion

        private System.Threading.SynchronizationContext context;

        #region Constructor and initializing methods

        /// <summary>
        /// Constructor.
        /// </summary>
        public MessagingAreaControl(VoIPViewModel myvm)
        {
            _myvm = myvm;
            _userPreferences = UserPreferences.Current;

            Binding = new MessagingAreaControlVM(myvm);
            Binding.UpdateHistory += this.VoIPService_UpdateHistory;
            Binding.UpdateMessage += this.VoIPService_AddMessage;
            Binding.UpdateCallButton += this.VoIPService_SwitchStateOfCallButton;
            this.DataContext = Binding;

            InitializeComponent();
            InitializeControls();
            context = System.Threading.SynchronizationContext.Current;
        }

        /// <summary>
        /// Initializes some controls.
        /// </summary>
        private void InitializeControls()
        {
            txtMessageHistory.IsReadOnly = true;
        }

        #endregion

        #region Private fields

        private MessagingAreaControlVM Binding;

        /// <summary>
        /// reference to the main view
        /// </summary>
        private VoIPViewModel _myvm;

        /// <summary>
        /// Reference to the user's text style.
        /// </summary>
        private readonly UserPreferences _userPreferences;

        #endregion

        #region Public methods

        /// <summary>
        /// Adds a new message to message history.
        /// </summary>
        /// <param name="nick">Nick of sender</param>
        /// <param name="message">Message</param>
        public void MessageReceived(string nick, string message)
        {
            //Create a new paragraph to write new message
            var messageParagraph = new Paragraph();
            messageParagraph.FontFamily = new FontFamily("Verdana");
            messageParagraph.FontSize = 12;

            //Add message to paragraph
            messageParagraph.Inlines.Add(new Run(nick + ": " + message));
            messageParagraph.Foreground = Brushes.White;

            //Add new parapraph to message history
            txtMessageHistory.Document.Blocks.Add(messageParagraph);

            if (txtMessageHistory.Document.Blocks.Count > 200)
            {
                txtMessageHistory.Document.Blocks.Remove(txtMessageHistory.Document.Blocks.FirstBlock);
            }

            txtMessageHistory.ScrollToEnd();
        }

        #endregion

        #region Private methods

        private void VoIPService_SwitchStateOfCallButton(object sender, SwitchStateOfCallButtonEventArg e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if (btnCall.IsChecked == false)
                        {
                            /*Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                                {*/
                            btnCall.IsChecked = true;
                            btnCall.Content = "Racrocher";
                            //}));
                        }
                        else
                        {
                            /*Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                            {*/
                            btnCall.IsChecked = false;
                            btnCall.Content = "Appeler";
                            //}));
                        }
                    }));
        }

        private void VoIPService_UpdateHistory(object sender, UpdateHistoryEventArg e)
        {
            this.txtMessageHistory.Document.Blocks.Clear();
            foreach (string tmpstr in e.ContactHistory)
            {
                Paragraph para = new Paragraph();
                para.FontFamily = new FontFamily("Verdana");
                para.FontSize = 13;
                para.Foreground = new SolidColorBrush(new Color
                {
                    A = 255,
                    R = 255,
                    G = 255,
                    B = 255
                });
                para.Inlines.Add(new Run(tmpstr));
                this.txtMessageHistory.Document.Blocks.Add(para);
            }
            txtMessageHistory.ScrollToEnd();
        }

        public delegate void AddTextCallBack(Paragraph message);

        private void VoIPService_AddMessage(object sender, AddMessageToHistoryEventArg e)
        {
            //create new paragraph
            Paragraph tmp = new Paragraph();
            tmp.FontFamily = new FontFamily("Verdana");
            tmp.FontSize = 13;
            tmp.Foreground = new SolidColorBrush(new Color
            {
                A = 255,
                R = 255,
                G = 255,
                B = 255
            });
            //add message to the paragraph
            tmp.Inlines.Add(new Run(e.UserName + ": " + e.MessageToAdd));
            //add the paragraph to the RichTextBox

            if (txtMessageHistory.Dispatcher.CheckAccess())
            {
                txtMessageHistory.Document.Blocks.Add(tmp);
                txtMessageHistory.ScrollToEnd();
            }
            else
            {
                context.Send(AddTextToUI, tmp);
            }
        }

        private void AddTextToUI(object message)
        {
            txtMessageHistory.Dispatcher.Invoke(new Action(() => txtMessageHistory.Document.Blocks.Add((Paragraph)message)));
        }

        #region Sending message

        /// <summary>
        /// Handles KeyDown event of txtWriteMessage textbox.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void AddMessageKeyDown(object sender, KeyEventArgs e)
        {
            //If user pressed to enter in message sending textbox, send message..
            if (e.Key == Key.Enter)
            {
                Binding.SendMessage();
            }
        }

        #endregion

        #endregion
    }
}
