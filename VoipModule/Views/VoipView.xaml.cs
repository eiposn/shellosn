﻿using CommonLib.VoIP;
using CommonModule.BaseTypes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VoipModule.Services;
using VoipModule.Views.UserControls;
using CommonLib.Contract;
using System.Security.Cryptography;
using CommonLib.GlobalClasses;
using CommonLib.VoIP.VoIPInvitationClasses;
using VoipModule.ViewModels;
using VoipModule.EventArg;
using Microsoft.Practices.Prism.Commands;


namespace VoipModule.Views
{
    /// <summary>
    /// Logique d'interaction pour VoipView.xaml
    /// </summary>
    public partial class VoipView : WorkspaceViewBase
    {
        private VoIPViewModel bindingclass;
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public VoipView(IRegionManager _regionManager)
        {
            bindingclass = new VoIPViewModel();
            bindingclass.UpdateContactList += this.VoIPService_UpdateContactList;
            bindingclass.UpdateContactStatus += this.VoIPService_UpdateContactStatus;
            bindingclass.LoginInProcess += this.VoIPService_LoginInProcess;
            bindingclass.EndLogin += this.VoIPService_EndLogin;
            bindingclass.NewMessageFromCOntact += this.VoIPService_NewMessage;
            bindingclass.ListFound += this.VoIPService_UpdateContactsFound;
            bindingclass.InvitationSent += this.VoIPService_InvitationSent;
            bindingclass.GetInvitation += this.VoIPClient_GetInvitation;
            bindingclass.InvitationTerminted += this.VoIPClient_InvitationTerminated;
            bindingclass.Init();
            this.DataContext = bindingclass;
            InitializeComponent();
            this.regionManager = _regionManager;

            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();

            container.RegisterInstance<VoIPViewModel>(bindingclass);
            container.RegisterType<Object, MessagingAreaControl>("MessagingAreaControl");
        }

        ~VoipView()
        {
            bindingclass.Destrutor();
        }



        private void VoIPService_UpdateContactStatus(object sender, UpdateContactStatusEventArgs e)
        {
            foreach (UserCardControl userCardControl in spUsers.Children)
            {
                if (e.UserInformations.Username.CompareTo(userCardControl.UserNick) == 0)
                {
                    userCardControl.UserStatus = e.ClientNewStatus;
                }
            }

        }

        private void VoIPService_UpdateContactList(object sender, UpdateContactListEventArg e)
        {
            foreach (ContactInfo ct in e.ContactList)
            {
                Application.Current.Dispatcher.Invoke(new Action(() => AddUserToListInternal(ct)));
            }
        }


        /// <summary>
        /// Changes status of a user in user list.
        /// </summary>
        /// <param name="nick">Nick of the user</param>
        /// <param name="newStatus">New status of the user</param>
        public void OnUserStatusChangeInternal(string nick, EClientStatus newStatus)
        {
            //Find user in list
            var userCard = FindUserInList(nick);

            //Change status of user if found
            if (userCard != null)
            {
                userCard.UserStatus = newStatus;
            }
        }


        /// <summary>
        /// Searches a user (by nick) in user list and gets user card control of user.
        /// </summary>
        /// <param name="nick">Nick to search</param>
        /// <returns>Found user card of user</returns>
        private UserCardControl FindUserInList(string nick)
        {
            return spUsers.Children.Cast<UserCardControl>().FirstOrDefault(userCardControl => userCardControl.UserNick == nick);
        }

        /// <summary>
        /// Adds user to user list in right area of the window.
        /// </summary>
        /// <param name="contactInfo">New user informations</param>
        private void AddUserToListInternal(ContactInfo contactInfo)
        {
            //Do not add user to list if it is already exists.
            if (FindUserInList(contactInfo.Username) != null)
            {
                return;
            }

            //Find correct order (by name) to insert the user
            var orderedIndex = 0;
            foreach (UserCardControl userCardControl in spUsers.Children)
            {
                if (contactInfo.Username.CompareTo(userCardControl.UserNick) < 0)
                {
                    break;
                }
                orderedIndex++;
            }

            //Create user control
            var userCard = new UserCardControl
            {
                UserNick = contactInfo.Username,
                UserStatus = contactInfo.ClientStatus,
                AvatarBytes = null, //TODO add the image to the contactInfo
                Height = 60
            };
            userCard.MouseLeftButtonUp += UserCard_MouseSimpleClick;

            //Insert user to user list
            spUsers.Children.Insert(
                orderedIndex,
                userCard
                );
        }

        /// <summary>
        /// Removes an existing user from user list.
        /// </summary>
        /// <param name="nick"></param>
        private void RemoveUserFromListInternal(string nick)
        {
            //Find user in list
            var userCard = FindUserInList(nick);

            //Remove if found
            if (userCard != null)
            {
                spUsers.Children.Remove(userCard);
                userCard.MouseLeftButtonUp -= UserCard_MouseSimpleClick;
            }
        }

        /// <summary>
        /// Handles MouseDoubleClick event of all User cards.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void UserCard_MouseSimpleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            var userCard = e.Source as UserCardControl;
            if (userCard == null)
            {
                return;
            }
            //si un contact est déjà selectionner il faut le remettre en blanc
            if (bindingclass.CurrentContact != null)
            {
                UserCardControl tmpusr = FindUserInList(bindingclass.CurrentContact.Username);
                if (tmpusr.grdCard.Background == Brushes.LightBlue)
                    tmpusr.grdCard.Background = Brushes.White;
            }
            // met le contact selectionné en bleu
            userCard.grdCard.Background = Brushes.LightBlue;
            userCard.imgNewMessage.Visibility = System.Windows.Visibility.Collapsed;
            bindingclass.UserCard_MouseSimpleClick(userCard);
        }

        private void Research_Click2(object sender, RoutedEventArgs e)
        {
            //Test 1
            /*ContactInfo tmpct = new ContactInfo("Michou", CommonLib.GlobalClasses.EUserType.VoIP, "0.0.0.0");
            tmpct.ClientStatus = CommonLib.GlobalClasses.EClientStatus.Busy;
            AddUserToListInternal(tmpct);

            ContactInfo tmpct2 = new ContactInfo("Anabelle", CommonLib.GlobalClasses.EUserType.VoIP, "0.0.0.0");
            tmpct2.ClientStatus = CommonLib.GlobalClasses.EClientStatus.Away;
            AddUserToListInternal(tmpct2);*/
            if (bindingclass.CurrentContact.Username == "Flash")
            {
                bindingclass.CurrentContact.ClientStatus = EClientStatus.Online;
                bindingclass.CurrentContact.IpAddress = "82.235.161.168";
                //bindingclass.OnUpdateContactStatus(bindingclass.CurrentContact.Username, EClientStatus.Online);
            }
        }



        #region login

        private void VoIPService_LoginInProcess(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    grdConnectInProcess.Visibility = System.Windows.Visibility.Visible;
                    brdConnectInProcess.Visibility = System.Windows.Visibility.Visible;
                }));
        }

        private void VoIPService_EndLogin(object sender, EndLoginEventArg e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    grdConnectInProcess.Visibility = System.Windows.Visibility.Collapsed;
                    brdConnectInProcess.Visibility = System.Windows.Visibility.Collapsed;

                    if (e.status == true)
                    {
                        grdConnect.Visibility = System.Windows.Visibility.Collapsed;
                        brdConnect.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }));
        }

        #endregion

        #region Nouveau Message

        private void VoIPService_NewMessage(object sender, MessageReceivedFromContactEventArg e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                UserCardControl usr = FindUserInList(e.UserName);
                usr.grdCard.Background = Brushes.Orange;
                usr.imgNewMessage.Visibility = System.Windows.Visibility.Visible;
            }));
        }

        #endregion

        #region invitation

        private void Research_Click(object sender, RoutedEventArgs e)
        {
            if (btnInvitation.IsChecked == true)
            {
                btnInvitation.Content = "Quitter L'invitation";
                StackIvitation.Visibility = System.Windows.Visibility.Visible;
                grdInvitation.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                btnInvitation.Content = "Inviter une personne";
                StackIvitation.Visibility = System.Windows.Visibility.Collapsed;
                brdResultInvitation.Visibility = System.Windows.Visibility.Collapsed;
                grdInvitation.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void VoIPService_UpdateContactsFound(object sender, UpdateUsersFoundEventArg e)
        {
            if (e.users != null)
            {
                //clean la liste 
                Application.Current.Dispatcher.Invoke(new Action(() => spUsers2.Children.Clear()));
                if (e.users.Count() != 0)
                {
                    foreach (UserToInvite ct in e.users)
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() => AddUserFoundToListInternal(ct)));
                    }
                }
            }
        }

        /// <summary>
        /// Adds user to user list in right area of the window.
        /// </summary>
        /// <param name="contactInfo">New user informations</param>
        private void AddUserFoundToListInternal(UserToInvite contactInfo)
        {
            //Do not add user to list if it is already exists.
            if (FindFoundUserInList(contactInfo.Username) != null)
            {
                return;
            }

            //Find correct order (by name) to insert the user
            var orderedIndex = 0;
            foreach (UserCardControl userCardControl in spUsers2.Children)
            {
                if (contactInfo.Username.CompareTo(userCardControl.UserNick) < 0)
                {
                    break;
                }
                orderedIndex++;
            }

            //Create user control
            var userCard = new UserCardControl
            {
                UserNick = contactInfo.Username,
                FirstName = contactInfo.FirstName,
                LastName = contactInfo.LastName,
                AvatarBytes = null, //TODO add the image to the contactInfo
                Height = 60
            };
            userCard.MouseLeftButtonUp += UserCard_FoundMouseSimpleClick;

            //Insert user to user list
            spUsers2.Children.Insert(
                orderedIndex,
                userCard
                );
        }

        /// <summary>
        /// Handles MouseDoubleClick event of all User cards.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void UserCard_FoundMouseSimpleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            var userCard = e.Source as UserCardControl;
            if (userCard == null)
            {
                return;
            }

            //si un contact est déjà selectionner il faut le remettre en blanc
            if (bindingclass.CurrentContactToInvite != null)
            {
                UserCardControl tmpusr = FindFoundUserInList(bindingclass.CurrentContactToInvite.UserNick);
                if (tmpusr.grdCard.Background == Brushes.LightBlue)
                    tmpusr.grdCard.Background = Brushes.White;
            }
            // met le contact selectionné en bleu
            userCard.grdCard.Background = Brushes.LightBlue;
            userCard.imgNewMessage.Visibility = System.Windows.Visibility.Collapsed;
            bindingclass.UserCardFound_MouseSimpleClick(userCard);
        }

        /// <summary>
        /// Searches a user (by nick) in user list and gets user card control of user.
        /// </summary>
        /// <param name="nick">Nick to search</param>
        /// <returns>Found user card of user</returns>
        private UserCardControl FindFoundUserInList(string nick)
        {
            return spUsers2.Children.Cast<UserCardControl>().FirstOrDefault(userCardControl => userCardControl.UserNick == nick);
        }

        private void VoIPService_InvitationSent(object sender, InvitationSentEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    btnInvitation.Content = "Quitter L'invitation";
                    brdInvistationSent.Visibility = System.Windows.Visibility.Visible;
                }));

        }

        private void ButtonOK_Clicked(object sender, RoutedEventArgs e)
        {
            brdInvistationSent.Visibility = System.Windows.Visibility.Collapsed;
            grdInvitation.Visibility = System.Windows.Visibility.Collapsed;
            btnInvitation.Content = "Inviter une personne";
            btnInvitation.IsChecked = false;
        }

        private void VoIPClient_GetInvitation(object sender, GetOneInvitationEventArg e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                StackPendingIvitation.Visibility = System.Windows.Visibility.Visible;
                grdPendingInvitation.Visibility = System.Windows.Visibility.Visible;
            }));
        }

        private void VoIPClient_InvitationTerminated(object sender, InvitationAcceptedOrDecliendEventArg e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                StackPendingIvitation.Visibility = System.Windows.Visibility.Collapsed;
                grdPendingInvitation.Visibility = System.Windows.Visibility.Collapsed;
            }));
        }

        #endregion
    }
}

