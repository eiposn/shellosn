﻿using CommonLib.VoIP;
using Hik.Communication.Scs.Communication;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.ScsServices.Client;
using Hik.Communication.ScsServices.Communication.Messages;
using P2PCommonLib.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.Services.ClientToClient
{
    public class VoIPChatServiceController : IVoIPChatServiceController
    {
        #region Private fields

        /// <summary>
        /// The object which handles remote method calls from server.
        /// It implements IChatClient contract.
        /// </summary>
        private VoIPPeer _VoipClient;

        private UserIdentifier _currentUserId;

        /// <summary>
        /// This object is used to connect to SCS Chat Service as a client. 
        /// </summary>
        private IScsServiceClient<IVoIPHost> _scsClient;

        #endregion

        #region IVoIPChatServiceController implementation

        /// <summary>
        /// Connects to the server.
        /// It automatically Logins to server if connection success.
        /// </summary>
        public void Connect(VoIPPeer voipclient, UserIdentifier usrId, string ipAddress)
        {
            _currentUserId = usrId;
            //Disconnect if currently connected
            Disconnect();

            //Create a ChatClient to handle remote method invocations by server
            _VoipClient = voipclient;

            //Create a SCS client to connect to SCS server
            _scsClient = ScsServiceClientBuilder.CreateClient<IVoIPHost>(new ScsTcpEndPoint(ipAddress, 62345), _VoipClient);

            //Register events of SCS client
            _scsClient.Connected += ScsClient_Connected;
            _scsClient.Disconnected += ScsClient_Disconnected;

            //Connect to the server
            _scsClient.Connect();
        }

        /// <summary>
        /// Disconnects from server if it is connected.
        /// </summary>
        public void Disconnect()
        {
            if (_scsClient != null && _scsClient.CommunicationState == CommunicationStates.Connected)
            {
                try
                {
                    _scsClient.ServiceProxy.Close();
                    _scsClient.Disconnect();
                }
                catch
                {

                }
                _scsClient = null;
            }
        }

        public void SendTextMessage(UserIdentifier userId, string messageToSend)
        {
            try
            {
                _scsClient.ServiceProxy.SendTextMessage(userId, messageToSend);
            }
            catch
            {

            }
        }

        public bool StartCallProtocol(String Username)
        {
            try
            {
                return _scsClient.ServiceProxy.StartCallProtocol(Username);
            }
            catch
            {
                return false;
            }
        }

        public void StopCallProtocol(String username)
        {
            try
            {
                _scsClient.ServiceProxy.StopCallProtocol(username);
            }
            catch
            {
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// This method handles Connected event of _scsClient.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void ScsClient_Connected(object sender, EventArgs e)
        {
            try
            {
                _scsClient.ServiceProxy.Identification(_currentUserId);
            }
            catch (ScsRemoteException ex)
            {
                Disconnect();
                Console.WriteLine(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            catch
            {
                Disconnect();
                Console.WriteLine("Can not login to contact. Please try again later.");
            }
        }

        /// <summary>
        /// This method handles Disconnected event of _scsClient.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void ScsClient_Disconnected(object sender, EventArgs e)
        {
            _scsClient.ServiceProxy.Close();
        }

        #endregion
    }

}
