﻿using CommonLib.VoIP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.Services.ClientToClient
{
    /// <summary>
    /// This interface defines method of chat controller that can be used by views.
    /// </summary>
    public interface IVoIPChatServiceController
    {
        /// <summary>
        /// Connects to the server.
        /// It automatically Logins to server if connection success.
        /// </summary>
        void Connect(VoIPPeer voipclient, UserIdentifier usrId, string ipAddress);

        /// <summary>
        /// Disconnects from server if it is connected.
        /// </summary>
        void Disconnect();
    }
}
