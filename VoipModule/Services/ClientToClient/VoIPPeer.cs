﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2PCommonLib.Contracts;
using CommonLib.VoIP;
using VoipModule.ViewModels;
using VoipModule.Audio_Communication;
using System.Net;
using System.Text.RegularExpressions;

namespace VoipModule.Services.ClientToClient
{
    public class VoIPPeer : IVoIPPeer
    {
        private VoIPViewModel _VoIPVM;

        public VoIPPeer(VoIPViewModel voipVM)
        {
            _VoIPVM = voipVM;
        }

        public void OnSendTextMessage(UserIdentifier userId, String Message)
        {
            _VoIPVM.ReceiveMessage(userId.Username, Message);
            _VoIPVM.PutReceivedMessageToView(userId.Username, Message);
        }

        public bool StartCallProtocol(String username)
        {
            ContactInformations info = _VoIPVM.GetContact(username);
            //if (info.IpAddress != _VoIPVM.PublicIp)
            {
                UpnpForward upnp = new UpnpForward();
                upnp.launch();
                _VoIPVM.audio = new AudioConnection();
                _VoIPVM.audio.Connect(info.IpAddress);
            }
            /*else
            {
                _VoIPVM.audio = new AudioConnection();
                _VoIPVM.audio.Connect(info.LocalIpAddress);
            }*/
            _VoIPVM.SwitchStateOfTheCallHangupButton();
            return true;
        }

        public void StopCallProtocol(String username)
        {
            _VoIPVM.SwitchStateOfTheCallHangupButton();
            _VoIPVM.audio.Disconnect();
            return;
        }
    }
}
