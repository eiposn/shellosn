﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2PCommonLib.Contracts;
using CommonLib.VoIP;
using Hik.Misc;
using Hik.Communication.ScsServices.Service;
using Hik.Collections;
using CommonLib.GlobalClasses.Exceptions;
using VoipModule.ViewModels;
using CommonLib.GlobalClasses;
using VoipModule.Audio_Communication;
using System.Net;
using System.Text.RegularExpressions;

namespace VoipModule.Services.ClientToClient
{
    class VoIPHost : ScsService, IVoIPHost
    {
        private VoIPViewModel _voipVM;
        private ThreadSafeSortedList<long, PrivateVoIPPeer> _clients;
        private UserIdentifier[] _UserIds;

        public VoIPHost(UserIdentifier[] contactIdList, VoIPViewModel voipvm)
        {
            _clients = new ThreadSafeSortedList<long, PrivateVoIPPeer>();
            _UserIds = contactIdList;
            _voipVM = voipvm;
        }

        public void Identification(UserIdentifier userId)
        {
            foreach (UserIdentifier User in _UserIds)
            {
                if ((User.SessionToken == userId.SessionToken) && (userId.SessionToken != ""))
                {
                    var client = CurrentClient;
                    var client_proxy = client.GetClientProxy<IVoIPPeer>();
                    PrivateVoIPPeer serverClient = new PrivateVoIPPeer(userId, client, client_proxy);
                    Task.Factory.StartNew(
                    () =>
                    { 
                        _clients[client.ClientId] = serverClient;
                        client.Disconnected += Client_Disconnected;
                    });
                }
                /*else
                {
                    throw new PermissionException("This is not one of your contacts");
                }*/
            }
        }

        public void AddNewContact(UserIdentifier[] userId)
        {
            _UserIds = userId;
        }

        public void UpdateContactInfo(UserInfo usrinfo, string sessionID)
        {
            for (int i = 0; i <= _UserIds.Length - 1; ++i)
            {
                if (_UserIds[i].Username.CompareTo(usrinfo.Username) == 0)
                {
                    _UserIds[i].SessionToken = sessionID;
                }
            }
        }

        /// <summary>
        /// This Fuction receive the message from the client
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="Message"></param>
        public void SendTextMessage(UserIdentifier userId, String Message)
        {
            _voipVM.ConnectionHosting(userId.Username);
            _voipVM.ReceiveMessage(userId.Username, Message);
            _voipVM.PutReceivedMessageToView(userId.Username, Message);
        }

        public void Close()
        {
            var senderClient = _clients[CurrentClient.ClientId];
            if (senderClient == null)
            {
                throw new PermissionException("Can not change status before login.");
            }
            ClientLogout(CurrentClient.ClientId);
        }

        /// <summary>
        /// Send a message to a contact
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="message"></param>
        public void SendMessageToContact(string contactId, string message)
        {
            PrivateVoIPPeer currentContact = FindClientByDbId(contactId);
            currentContact.ClientProxy.OnSendTextMessage(_voipVM.personnalUserIdentifier, message);
        }

        public void HangupCall(string contactId)
        {
            PrivateVoIPPeer currentContact = FindClientByDbId(contactId);
            currentContact.ClientProxy.StopCallProtocol(_voipVM.personnalUserIdentifier.Username);
        }

        #region Private Method
        private PrivateVoIPPeer FindClientByDbId(string contactId)
        {
            return (from client in _clients.GetAllItems()
                    where client.UserID.SessionToken == contactId
                    select client).FirstOrDefault();
        }

        /// <summary>
        /// Handles Disconnected event of all clients.
        /// </summary>
        /// <param name="sender">Client object that is disconnected</param>
        /// <param name="e">Event arguments (not used in this event)</param>
        private void Client_Disconnected(object sender, EventArgs e)
        {
            //Get client object
            var client = (IScsServiceClient)sender;

            //Perform logout (so, if client did not call Logout method before close,
            //we do logout automatically).
            ClientLogout(client.ClientId);
        }

        /// <summary>
        /// This method is called when a client Calls Logout method of service or a client
        /// connection fails.
        /// </summary>
        /// <param name="clientId">Unique Id of client that is logged out</param>
        private void ClientLogout(long clientId)
        {
            //Get client from client list, if not in list do not continue
            PrivateVoIPPeer client = _clients[clientId];
            if (client == null)
            {
                return;
            }
            //Start a new task to inform all other users
            Task.Factory.StartNew(
                () =>
                {
                    //Remove client from online clients list
                    _clients.Remove(client.Client.ClientId);
                    //Unregister to Disconnected event (not needed really)
                    client.Client.Disconnected -= Client_Disconnected;
                });
        }

        public void userConnected(String Username, String SessionToken)
        {
            int i;

            for (i = 0; i <= _clients.Count; ++i)
            {
                if (_UserIds[i].Username == Username)
                {
                    _UserIds[i].SessionToken = SessionToken;
                }
            }
        }

        public void UserDisconnected(String Username, String SessionToken)
        {
            int i;

            for (i = 0; i <= _clients.Count; ++i)
            {
                if (_UserIds[i].Username == Username)
                {
                    _UserIds[i].SessionToken = "";
                }
            }
        }


        public bool StartCallProtocol(String username)
        {
            ContactInformations info = _voipVM.GetContact(username);
            //if (info.IpAddress != _voipVM.PublicIp)
            {
                UpnpForward upnp = new UpnpForward();
                upnp.launch();
                _voipVM.audio = new AudioConnection();
                _voipVM.audio.Connect(info.IpAddress);
            }
            /*else
            {
                _voipVM.audio = new AudioConnection();
                _voipVM.audio.Connect(info.LocalIpAddress);
            }*/
            _voipVM.SwitchStateOfTheCallHangupButton();
            return true;
        }

        public void StopCallProtocol(String username)
        {
            _voipVM.SwitchStateOfTheCallHangupButton();
            _voipVM.audio.Disconnect();
            return;
        }

        #endregion

        #region VoIPPeer
        private sealed class PrivateVoIPPeer
        {
            public PrivateVoIPPeer(UserIdentifier userId, IScsServiceClient client, IVoIPPeer clientProxy)
            {
                this.UserID = userId;
                this.Client = client;
                this.ClientProxy = clientProxy;
            }

            /// <summary>
            /// Scs client reference.
            /// </summary>
            public IScsServiceClient Client { get; private set; }

            /// <summary>
            /// Proxy object to call remote methods of chat client.
            /// </summary>
            public IVoIPPeer ClientProxy { get; private set; }

            /// <summary>
            /// User informations of client.
            /// </summary>
            public UserIdentifier UserID;
        }
        #endregion
    }
}
