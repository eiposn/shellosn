﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib.GlobalClasses;
using CommonLib.Contract;
using CommonLib.VoIP.VoIPInvitationClasses;

namespace VoipModule.Services.ClientToServer
{
    /// <summary>
    /// This interface defines method of chat controller that can be used by views.
    /// </summary>
    public interface IVoIPServiceController
    {
        /// <summary>
        /// Connects to the server.
        /// It automatically Logins to server if connection success.
        /// </summary>
        Boolean Connect(VoIPClient voipclient);

        /// <summary>
        /// Disconnects from server if it is connected.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Change status of user.
        /// </summary>
        /// <param name="newStatus">New status</param>
        void ChangeStatus(EClientStatus newStatus);

        void SendInvitationToUser(UserToInvite usr);

        UserToInvite[] SearchUsersoInvite(String FirstName, String LastName);
    }
}