﻿using CommonLib.Contract;
using CommonLib.GlobalClasses;
using CommonLib.VoIP;
using CommonLib.VoIP.VoIPInvitationClasses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using VoipModule.EventArg;


namespace VoipModule.Services.ClientToServer
{
    public class VoIPClient : IVoIPClient
    {
        ContactInfo[] mycontacts;
        PendingInvitations[] pendingInvitations;

        public event EventHandler<UpdateContactListEventArg> UpdateContactList;
        public event EventHandler<UpdateContactStatusEventArgs> UpdateContactStatus;
        public event EventHandler<GetOneInvitationEventArg> GetOneInvitation;
        public event EventHandler<GetPendingInvitationEventArg> GetPendingInvitation;


        public void OnGetPendingInvitation(PendingInvitations[] infos)
        {
            var handler = GetPendingInvitation;

            GetPendingInvitationEventArg e = new GetPendingInvitationEventArg();
            e.pendingInvitations = infos;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public void OnGetOneInvitation(InviteUserInfos user)
        {
            var handler = GetOneInvitation;

            GetOneInvitationEventArg e = new GetOneInvitationEventArg();
            e.usr = user;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public  void OnUpdateContactList()
        {
            var handler = UpdateContactList;

            UpdateContactListEventArg UCLEA = new UpdateContactListEventArg();
            UCLEA.ContactList = mycontacts;
            if (handler != null)
            {
                handler(this, UCLEA);
            }
        }

        public void OnUpdateContactStatus(UserInfo userInfo, string sessionID, EClientStatus ContactNewStatus)
        {
            var handler = UpdateContactStatus;

            UpdateContactStatusEventArgs UCS = new UpdateContactStatusEventArgs();
            UCS.UserInformations = userInfo;
            UCS.useridentifier = sessionID;
            UCS.ClientNewStatus = ContactNewStatus;
            if (handler != null)
            {
                handler(this, UCS);
            }
        }

        /// <summary>
        /// This method is used to get user list from chat server.
        /// It is called by server once after user logged in to server.
        /// </summary>
        /// <param name="users">All online user informations</param>
        public void OnGetContactList(ContactInfo[] users)
        {
            mycontacts = users;
            OnUpdateContactList();
#if DEBUG
            Console.WriteLine("La liste de contacts envoyé par le serveur :");
            foreach (ContactInfo tmpusr in users)
            {
                Console.WriteLine(tmpusr.ToString());
            }
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public void OnUserInvitation(InviteUserInfos user)
        {
            OnGetOneInvitation(user);
#if DEBUG
            Console.WriteLine("Une invitation est arrivée :");
            Console.WriteLine(user.ToString());
#endif
        }

        /// <summary>
        /// This method is called from chat server to inform that a new user
        /// joined to chat room.
        /// </summary>
        /// <param name="userInfo">Informations of new user</param>
        public void OnAskForBridge(UserInfo userInfo)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invitations"></param>
        public void OnGetPendingInvitations(PendingInvitations[] invitations)
        {
            pendingInvitations = invitations;
            OnGetPendingInvitation(invitations);
#if DEBUG
            Console.WriteLine("La liste de d'invitations en attente envoyé par le serveur :");
            foreach (PendingInvitations tmpinv in invitations)
            {
                Console.WriteLine(tmpinv.ToString());
            }
#endif
        }

        /// <summary>
        /// This method is called from chat server to inform that a new user
        /// joined to chat room.
        /// </summary>
        /// <param name="contactInfo">Informations of new user</param>
        public void OnUserLogin(ContactInfo contactInfo)
        {
            if (mycontacts != null)
            {
                for (int i = 0; i <= mycontacts.Length - 1; ++i)
                {
                    if (mycontacts[i].Username.CompareTo(contactInfo.Username) == 0)
                    {
                        mycontacts[i] = contactInfo;
                        OnUserStatusChange((UserInfo)contactInfo, contactInfo.UserIdentifier, EClientStatus.Online);
                    }
                }
            }
#if DEBUG
            Console.WriteLine(contactInfo.ToString() + "vient de se connecté");
#endif
        }

        /// <summary>
        /// This method is called from chat server to inform that an existing user
        /// has left the chat room.
        /// </summary>
        /// <param name="nick">Informations of user</param>
        public void OnUserLogout(String username)
        {
            for (int i = 0; i <= mycontacts.Length - 1; ++i)
            {
                if (mycontacts[i].Username.CompareTo(username) == 0)
                {
                    mycontacts[i].ClientStatus = EClientStatus.Offline;
                }
            }
#if DEBUG
            Console.WriteLine(username.ToString() + "vient de se déconnecté");
#endif
        }

        /// <summary>
        /// This method is called from chat server to inform that a user
        /// changed his/her status.
        /// </summary>
        /// <param name="userinfo">Nick of the user</param>
        /// <param name="newStatus">New status of the user</param>
        public void OnUserStatusChange(UserInfo userinfo, string sessionID, EClientStatus newStatus)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => OnUpdateContactStatus(userinfo, sessionID, newStatus)));
            //OnUpdateContactStatus(username, newStatus);
            for (int i = 0; i > mycontacts.Length - 1; ++i)
            {
                if (mycontacts[i].Username.CompareTo(userinfo.Username) == 0)
                {
                    mycontacts[i].UserIdentifier = sessionID;
                    mycontacts[i].LocalIpAddress = userinfo.LocalIpAddress;
                    mycontacts[i].IpAddress = userinfo.IpAddress;
                    mycontacts[i].ClientStatus = newStatus;
                }
            }
#if DEBUG
            Console.WriteLine(userinfo.ToString() + "vient de se passer " + newStatus.ToString());
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AcountSalt"></param>
        /// <param name="SessionSalt"></param>
        /// <returns></returns>
        public String OnLoginProcess(String AcountSalt, String SessionSalt)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();

            String tmpHashedPassword = ConfigurationManager.AppSettings["PassWord"].ToString(); //Properties.Resources.Password; // "ThisIsThePassword";
            byte[] byteArrayPassword = System.Text.Encoding.Default.GetBytes(tmpHashedPassword);
            byte[] byteArrayHashedPassword = sha.ComputeHash(byteArrayPassword);
            tmpHashedPassword = BitConverter.ToString(byteArrayHashedPassword).Replace("-", "");

            tmpHashedPassword += AcountSalt;
            byte[] accountSaltedPassword = System.Text.Encoding.Default.GetBytes(tmpHashedPassword);
            byte[] accountHashedSaltedPassword = sha.ComputeHash(accountSaltedPassword);

            tmpHashedPassword = BitConverter.ToString(accountHashedSaltedPassword).Replace("-", "");

            tmpHashedPassword += SessionSalt;
            byte[] bASessionSaltPassword = System.Text.Encoding.Default.GetBytes(tmpHashedPassword);
            byte[] hashData3 = sha.ComputeHash(bASessionSaltPassword);
            String result = BitConverter.ToString(hashData3).Replace("-", "");

            return result;
        }
    }
}
