﻿using CommonLib.Contract;
using CommonLib.GlobalClasses;
using CommonLib.VoIP;
using CommonLib.VoIP.VoIPInvitationClasses;
using Hik.Communication.Scs.Communication;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.ScsServices.Client;
using Hik.Communication.ScsServices.Communication.Messages;
using Hik.Misc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VoipModule.Services.ClientToServer
{
    /// <summary>
    /// This class is a mediator with view and SCS system.
    /// </summary>
    internal class VoIPServiceController : IVoIPServiceController
    {
        #region Private fields

        /// <summary>
        /// The object which handles remote method calls from server.
        /// It implements IChatClient contract.
        /// </summary>
        private VoIPClient _VoipClient;

        /// <summary>
        /// This object is used to connect to SCS Chat Service as a client. 
        /// </summary>
        private IScsServiceClient<IVoIPService> _scsClient;

        private UserIdentifier my_userIdentifier;

        public UserIdentifier My_userIdentifier
        {
            get { return my_userIdentifier; }
        }

        private string publicIp;

        public string PublicIp
        {
            get { return publicIp; }
        }

        #endregion

        #region IVoIPServiceController implementation

        /// <summary>
        /// Connects to the server.
        /// It automatically Logins to server if connection success.
        /// </summary>
        public Boolean Connect(VoIPClient voipclient)
        {
            //Disconnect if currently connected
            Disconnect();

            //Create a ChatClient to handle remote method invocations by server
            _VoipClient = voipclient;

            //Create a SCS client to connect to SCS server
            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerIP"].ToString()))
                _scsClient = ScsServiceClientBuilder.CreateClient<IVoIPService>(new ScsTcpEndPoint("127.0.0.1", 10048), _VoipClient);
            else
                _scsClient = ScsServiceClientBuilder.CreateClient<IVoIPService>(new ScsTcpEndPoint(ConfigurationManager.AppSettings["ServerIP"].ToString(), 10048), _VoipClient);

            //Register events of SCS client
            _scsClient.Connected += ScsClient_Connected;
            _scsClient.Disconnected += ScsClient_Disconnected;

            //Connect to the server
            try
            {
                _scsClient.Connect();
            }
            catch
            {
                Console.WriteLine("Impossible de se connecter au serveur");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Disconnects from server if it is connected.
        /// </summary>
        public void Disconnect()
        {
            if (_scsClient != null && _scsClient.CommunicationState == CommunicationStates.Connected)
            {
                try
                {
                    _scsClient.ServiceProxy.Logout();
                    _scsClient.Disconnect();
                }
                catch
                {

                }
                _scsClient = null;
            }
        }

        /// <summary>
        /// Search users
        /// </summary>
        public UserToInvite[] SearchUsersoInvite(String FirstName, String LastName)
        {
            UserToInvite usr = new UserToInvite(FirstName, LastName);
            return _scsClient.ServiceProxy.SearchUserToInvite(usr);
        }

        /// <summary>
        /// invite user
        /// </summary>
        public void SendInvitationToUser(UserToInvite usr)
        {
            InviteUserInfos usrtoinvite = new InviteUserInfos(usr, "Ajoute moi en Ami !");
            _scsClient.ServiceProxy.InviteUser(usrtoinvite);
        }

        /// <summary>
        /// Accepte/decline user
        /// </summary>
        public void AccepteOrDeclineInvitation(string usr, bool isAccepted)
        {
            _scsClient.ServiceProxy.InvitationState(usr, isAccepted);
        }

        /// <summary>
        /// Change status of user.
        /// </summary>
        /// <param name="newStatus">New status</param>
        public void ChangeStatus(EClientStatus newStatus)
        {
            _scsClient.ServiceProxy.ChangeStatus(newStatus);
        }

        #endregion

        #region Private methods

        public static string GetIP()
        {
            WebClient webclient = new WebClient();
            string externalIP = "";
            externalIP = webclient.DownloadString("https://api.ipify.org/");
            /*externalIP = webclient.DownloadString("http://checkip.dyndns.org/");
            externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                                           .Matches(externalIP)[0].ToString();*/
            return externalIP;
        }

        /// <summary>
        /// This method handles Connected event of _scsClient.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void ScsClient_Connected(object sender, EventArgs e)
        {
            try
            {
                publicIp = GetIP();
                UserInfo usr = new UserInfo(ConfigurationManager.AppSettings["UserName"].ToString(), EUserType.VoIP, publicIp, Misc.GetLocalIPAddress());
                my_userIdentifier = _scsClient.ServiceProxy.Login(usr);
                System.Threading.Thread.Sleep(500);
                _scsClient.ServiceProxy.ChangeStatus(EClientStatus.Online);
            }
            catch (ScsRemoteException ex)
            {
                Disconnect();
                Console.WriteLine(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            catch
            {
                Disconnect();
                Console.WriteLine("Can not login to server. Please try again later.");
            }
        }

        /// <summary>
        /// This method handles Disconnected event of _scsClient.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void ScsClient_Disconnected(object sender, EventArgs e)
        {
            _scsClient.ServiceProxy.Logout();
        }

        #endregion
    }
}
