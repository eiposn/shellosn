﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VoipModule.Audio_Communication
{
    public class AudioConnection
    {
        private UdpClient udpSender;
        private UdpClient udpListener;
        private volatile bool connected;
        private WaveInEvent waveIn;
        private IWavePlayer waveOut;
        private BufferedWaveProvider waveProvider;
        private IPEndPoint remote;
        private Thread myThread;
        public void Connect(string ip)
        {
            waveIn = new WaveInEvent();
            waveOut = new WaveOut();
            waveIn.BufferMilliseconds = 50;
            waveIn.WaveFormat = new WaveFormat(48000, 16, 1);
            waveIn.DataAvailable += waveIn_DataAvailable;

            waveProvider = new BufferedWaveProvider(waveIn.WaveFormat);
            waveProvider.DiscardOnBufferOverflow = true;

            waveIn.StartRecording();

            waveOut.Init(waveProvider);
            waveOut.Play();
            udpSender = new UdpClient();
            remote = new IPEndPoint(IPAddress.Parse(ip), 12349);
            connected = true;
            myThread = new Thread(ReceptionLoop);
            myThread.IsBackground = true;
            myThread.Start();
        }

        private void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            udpSender.Send(e.Buffer, e.BytesRecorded, remote);
        }

        public void Disconnect()
        {
            waveIn.DataAvailable -= waveIn_DataAvailable;
            myThread.Abort();
            connected = false;
            udpListener.Close();
            udpSender.Close();
            
            waveIn.StopRecording();
            waveOut.Stop();
            
        }

        private void ReceptionLoop(object obj)
        {
            udpListener = new UdpClient(12349);
            IPEndPoint Local = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 12349);
            while (connected == true)
            {
                if (connected == true )
                {
                    Byte[] receiveBytes = udpListener.Receive(ref Local);
                    waveProvider.AddSamples(receiveBytes, 0, receiveBytes.Length);
                } 
            }
        }
    }
}
