﻿using Mono.Nat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoipModule.Audio_Communication
{
    class UpnpForward
    {
        public void launch()
        {
            NatUtility.DeviceFound += DeviceFound;
            NatUtility.DeviceLost += DeviceLost;
            NatUtility.StartDiscovery();
        }
        private void DeviceFound(object sender, DeviceEventArgs args)
        {
            INatDevice device = args.Device;
            foreach (Mapping portMap in device.GetAllMappings())
            {
                Console.WriteLine(portMap.ToString());
            }
            if (device.GetSpecificMapping(Protocol.Udp, 12345).PublicPort == -1)
                device.CreatePortMap(new Mapping(Protocol.Udp, 12345, 12345));
        }

        private void DeviceLost(object sender, DeviceEventArgs args)
        {
            INatDevice device = args.Device;
        }
    }
}
