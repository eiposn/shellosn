﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using S22.Imap;
using System.Text;
using System.Threading.Tasks;

namespace MailerModule.Models
{
    class Message
    {
        public string Date { get; set; }

        private Boolean fetched;

        public Boolean Fetched
        {
            get { return fetched; }
            set { fetched = value; }
        }

        private uint uid;

        public uint Uid
        {
            get { return uid; }
            set { uid = value; }
        }

        private MailMessage mailMessage;

        public MailMessage MailMessage
        {
            get { return mailMessage; }
            set { mailMessage = value; }
        }

        /// <summary>
        /// Initializes the class
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="message"></param>
        public Message(uint uid, MailMessage message)
        {
            this.Uid = uid;
            this.Fetched = false;
            this.MailMessage = message;
            this.Date = message.Date().Value.ToString("F");
        }
    }
}
