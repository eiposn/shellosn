﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailerModule.Models
{
    public class CookieManager
    {
        public String Path { get; set; }

        Dictionary<string, string> cookies = new Dictionary<string, string>();

        public Dictionary<string, string> Cookies
        {
            get { return cookies; }
            set { cookies = value; }
        }

        private List<String> indesirables = new List<string>();

        public List<String> Indesirables
        {
            get { return indesirables; }
            set { indesirables = value; }
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        public CookieManager()
        {
            Path = System.Reflection.Assembly.GetEntryAssembly().Location + ".cookie";
        }

        /// <summary>
        /// Load informations from the cookie file
        /// </summary>
        public void Load()
        {
            if (File.Exists(Path))
            {
                String[] entries = File.ReadAllText(Path).Split(';');

                foreach (var entry in entries)
                {
                    var str = entry.Split(':');
                    if (str[0].CompareTo("Indesirable") == 0 && !Indesirables.Contains(str[1]))
                        Indesirables.Add(str[1]);
                    else
                        Cookies.Add(str[0], str[1]);
                }
            }
        }

        /// <summary>
        /// Add value to Cookies
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddEntry(string key, string value)
        {
            if (Cookies.ContainsKey(key)) Cookies[key] = value;
            else Cookies.Add(key, value);

            MakeFile();
        }

        /// <summary>
        /// Delete value to Cookies
        /// </summary>
        /// <param name="key"></param>
        public void RemoveEntry(string key)
        {
            if (Cookies != null && Cookies.ContainsKey(key))
            {
                Cookies.Remove(key);
                MakeFile();
            }
        }

        /// <summary>
        /// Add Undesirable mail to Cookies
        /// </summary>
        /// <param name="address"></param>
        public void AddIndesirable(string address)
        {
            if (!Indesirables.Contains(address))
                Indesirables.Add(address);

            MakeFile();
        }

        /// <summary>
        /// Remove Undesirable mail from Cookies
        /// </summary>
        /// <param name="address"></param>
        public void RemoveIndesirable(string address)
        {
            if (Indesirables != null && Indesirables.Contains(address))
            {
                Indesirables.Remove(address);
                MakeFile();
            }
        }

        /// <summary>
        /// Generate the file for cookies
        /// </summary>
        public void MakeFile()
        { 
            string str = "";
            bool first = true;
            foreach (var entry in Cookies)
            {
                if (!first) str += ";";
                else first = false;

                str += entry.Key + ":" + entry.Value;
            }
            foreach (var inde in Indesirables)
            {
                if (str.Length != 0) str += ";";
                str += "Indesirable:" + inde;
            }
            File.WriteAllText(Path, str);
        }
    }
}
