﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MailerModule.Models
{
    public class LogInformation
    {
        public Boolean Authed { get; set; }

        public Boolean Autoload { get; set; }

        public String Hostname { get; set; }

        public NetworkCredential Credentials { get; set; }

        public CookieManager C_Manager { get; set; }

        private Dictionary<string, string> parameters = new Dictionary<string, string>();

        public Dictionary<string, string> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <param name="_cred"></param>
        /// <param name="_host"></param>
        public LogInformation(NetworkCredential _cred, String _host)
        {
            C_Manager = new CookieManager();
            Credentials = _cred;
            Hostname = _host;
            Autoload = false;
            Authed = false;
        }
    }
}
