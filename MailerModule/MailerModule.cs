﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using OSN;
using MailerModule.Views;
using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.PubSubEvents;

namespace MailerModule
{
    [Module(ModuleName = "Courrier", OnDemand = true)]
    public class MailerModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public MailerModule(IRegionManager _regionManager, IUnityContainer _container, TaskBarRegistry _taskbar, IModuleCatalog _catalog)
        {
            this.regionManager = _regionManager;
            this.container = _container;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            container.RegisterType<Object, MailerViewBase>("MailerViewBase");
            IEnumerable<ModuleInfo> module = _catalog.Modules;
            string Imgpath = "";
            foreach (ModuleInfo cur in module)
            {
                if (cur.ModuleName == "Courrier")
                    Imgpath = cur.Ref;

            }
            Imgpath = System.IO.Path.ChangeExtension(Imgpath, "png");
            _taskbar.RegisterMenuItem("MailerViewBase", "Courrier", Imgpath);
        }

        /// <summary>
        /// You must load the views in order for the navigation to resolve
        /// </summary>
        void LoadViewInRegion<TViewType>(string regionName)
        {
            IRegion region = regionManager.Regions[regionName];
            string viewName = typeof(TViewType).Name;

            object view = region.GetView(viewName);
            if (view == null)
            {
                view = container.Resolve<TViewType>();

                region.Add(view, viewName);
            }
        }

        public void Initialize()
        {
            string ribbonRegionName = ConfigurationManager.AppSettings["RibbonRegionName"];
            string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];

            var moduleAView = new Uri("MailerViewBase", UriKind.Relative);
            regionManager.RequestNavigate("b", moduleAView);

            var moduleBView = new Uri("SettingView", UriKind.Relative);
            regionManager.RequestNavigate("MailerViewBase", moduleBView);
        }
    }
}
