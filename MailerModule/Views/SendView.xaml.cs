﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CommonModule.BaseTypes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Win32;
using MailerModule.Models;
using System.Net;
using AppMailer;
using System.Configuration;
using System.ComponentModel;
using System.Net.Mail;
using MailerModule.ViewModels;
using System.Net.Mime;

namespace MailerModule.Views
{
    /// <summary>
    /// Logique d'interaction pour SendView.xaml
    /// </summary>
    public partial class SendView : WorkspaceViewBase
    {
        public LogInformation Info { get; set; }
        private readonly IRegionManager regionManager;
        public MailViewModel MailVM;
        public List<string> attachmentFiles = new List<string>();

        public SendView(IRegionManager _regionManager, LogInformation _info)
        {
            InitializeComponent();
            this.regionManager = _regionManager;
            this.Info = _info;

            MailVM = new MailViewModel(Info, _regionManager);

            this.DataContext = MailVM;
        }

        void PopUpUntilSendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            popUpBlock.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Send a mail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Send_Mail(object sender, RoutedEventArgs e)
        {
            try
            {
                Mailer mailer = new Mailer();

                mailer.FromToAddress(Info.Credentials.UserName, txtTo.Text);
                mailer.Subject = txtSubject.Text;
                mailer.Message = txtBody.Text;
                mailer.PrepareMail();
                mailer.Cred = Info.Credentials;

                if (attachmentFiles.Count > 0)
                {
                    mailer.AddAttachments(attachmentFiles);
                }

                mailer.Send(Parser_mail("smtp"), PopUpUntilSendCompleted);
                popUpBlock.Visibility = Visibility.Visible;

                txtTo.Text = "";
                txtSubject.Text = "";
                txtBody.Text = "";
                attachmentFiles.Clear();
                filenames.Text = "";
                //txtBody.Document.Blocks.Clear();
            }
            catch (Exception)
            {
                MessageBox.Show("Mail not sent");
            }
        }

        /// <summary>
        /// Obtains the hostname for specific email extension
        /// </summary>
        /// <param name="ext"></param>
        /// <returns></returns>
        private string Parser_mail(String ext)
        {
            var str = Info.Credentials.UserName.Substring(Info.Credentials.UserName.LastIndexOf('@') + 1);
            return ConfigurationManager.AppSettings[ext + "." + str];
        }

        private void Add_Attachment(object sender, RoutedEventArgs e)
        {
            OpenFileDialog FileDialog = new OpenFileDialog();
            if (FileDialog.ShowDialog() == true)
            {
                string filePath = FileDialog.FileName;
                attachmentFiles.Add(filePath);
                filenames.Text += filePath + "  ";
            }
        }

        private void Clean_Attachment(object sender, RoutedEventArgs e)
        {
            attachmentFiles.Clear();
            filenames.Text = "";
        }
    }
}
