﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CommonModule.BaseTypes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Win32;
using MailerModule.Models;
using System.Net;
using S22.Imap;
using System.Net.Mail;
using System.IO;
using NhibernateORM;
using MailerModule.ViewModels;

namespace MailerModule.Views
{
    /// <summary>
    /// Logique d'interaction pour SpamView.xaml
    /// </summary>
    public partial class SpamView : WorkspaceViewBase
    {
        internal LogInformation Info { get; set; }

        private readonly IRegionManager regionManager;

        public SpamViewModel MailVM;

        /// <summary>
        /// Constructor, initialize the view
        /// </summary>
        /// <param name="_regionManager"></param>
        /// <param name="_credentials"></param>
        /// <param name="_hostname"></param>
        public SpamView(IRegionManager _regionManager, LogInformation _info, Dictionary<string, WorkspaceViewBase> _listViews)
        {
            InitializeComponent();
            this.regionManager = _regionManager;
            Info = _info;
            _listViews.Add("Spam", this);

            MailVM = new SpamViewModel(Info, _regionManager);
            this.DataContext = MailVM;
        }

        /// <summary>
        /// Switch to the setting view
        /// </summary>
        private void SwitchToSettingView()
        {
            var moduleAView = new Uri("MailView", UriKind.Relative);
            regionManager.RequestNavigate("MailerViewBase", moduleAView);
        }
    }
}
