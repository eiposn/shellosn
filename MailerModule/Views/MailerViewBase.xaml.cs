﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System.Net;
using MailerModule.Models;
using CommonModule.BaseTypes;
using MailerModule.ViewModels;

namespace MailerModule.Views
{
    /// <summary>
    /// Logique d'interaction pour MailerViewBase.xaml
    /// </summary>
    public partial class MailerViewBase : UserControl
    {
        public LogInformation info;
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;
        private List<string> firstLaunch = new List<string>();
        protected Dictionary<string, WorkspaceViewBase> listViews = new Dictionary<string, WorkspaceViewBase>();
        /// <summary>
        /// Manages the different views for the Mailer module
        /// </summary>
        /// <param name="_regionManager"></param>
        public MailerViewBase(IRegionManager _regionManager)
        {
            InitializeComponent();
            this.info = new LogInformation(new NetworkCredential(), "");
            this.regionManager = _regionManager;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            this.container.RegisterInstance<LogInformation>(info);
            this.container.RegisterInstance<Dictionary<string, WorkspaceViewBase>>(listViews);


            container.RegisterType<Object, SettingView>("SettingView");
            container.RegisterType<Object, MailView>("MailView");
            container.RegisterType<Object, SendView>("SendView");
            container.RegisterType<Object, SpamView>("SpamView");
            container.RegisterType<Object, TrashView>("TrashView");

            info.C_Manager.Load();
            if (info.C_Manager.Cookies.ContainsKey("login") && info.C_Manager.Cookies.ContainsKey("password"))
            {
                info.Credentials.UserName = info.C_Manager.Cookies["login"];
                info.Credentials.Password = info.C_Manager.Cookies["password"];
                info.Autoload = true;
            }
        }

        /// <summary>
        /// Open the setting view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenSettingView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("SettingView", UriKind.Relative);
            regionManager.RequestNavigate("MailerViewBase", moduleAView);
        }

        /// <summary>
        /// Open the mail view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenMailView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("MailView", UriKind.Relative);
            regionManager.RequestNavigate("MailerViewBase", moduleAView);

            if (!firstLaunch.Contains("Mail"))
            {
                firstLaunch.Add("Mail");
                var mailView = listViews["Mail"] as MailView;
                if (mailView != null)
                {
                    mailView.MailVM.Init();
                }
            }
            else
            {
                var mailView = listViews["Mail"] as MailView;
                if (info.Authed)
                {
                    if (mailView != null && mailView.MailVM.Client == null)
                    {
                        mailView.MailVM.Init();
                    }
                }
                else
                {
                    if (mailView != null)
                    {
                        mailView.MailVM.Client = null;

                    }
                }
            }
        }

        /// <summary>
        /// Open the send view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenSendView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (info.Authed)
            {
                var moduleAView = new Uri("SendView", UriKind.Relative);
                regionManager.RequestNavigate("MailerViewBase", moduleAView);
            }
        }

        /// <summary>
        /// Open the trash view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenTrashView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("TrashView", UriKind.Relative);
            regionManager.RequestNavigate("MailerViewBase", moduleAView);

            if (!firstLaunch.Contains("Trash"))
            {
                firstLaunch.Add("Trash");
                var mailView = listViews["Trash"] as TrashView;
                if (mailView != null)
                {
                    mailView.MailVM.Init();
                }
            }
        }

        /// <summary>
        /// Open the spam view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenSpamView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("SpamView", UriKind.Relative);
            regionManager.RequestNavigate("MailerViewBase", moduleAView);

            if (!firstLaunch.Contains("Spam"))
            {
                firstLaunch.Add("Spam");
                var mailView = listViews["Spam"] as SpamView;
                if (mailView != null)
                {
                    mailView.MailVM.Init();
                }
            }
        }
    }
}
