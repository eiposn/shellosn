﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CommonModule.BaseTypes;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Win32;
using MailerModule.Models;
using System.Net;
using System.Configuration;
using S22.Imap;

namespace MailerModule.Views
{
    /// <summary>
    /// Logique d'interaction pour SettingView.xaml
    /// </summary>
    public partial class SettingView : WorkspaceViewBase
    {
        public ImapClient Client { get; set; }

        public LogInformation Info { get; set; }

        private readonly IRegionManager regionManager;

        /// <summary>
        /// Constructor, initialize the view
        /// </summary>
        /// <param name="_regionManager"></param>
        /// <param name="_info"></param>
        public SettingView(IRegionManager _regionManager, LogInformation _info)
        {
            InitializeComponent();
            this.regionManager = _regionManager;
            Info = _info;

            if (Info.Autoload)
            {
                Login();
            }
        }

        /// <summary>
        /// Obtains the hostname for specific email extension
        /// </summary>
        /// <param name="ext"></param>
        private void Parser_mail(String ext)
        {
            var str = Info.Credentials.UserName.Substring(Info.Credentials.UserName.LastIndexOf('@') + 1);
            Info.Hostname = ConfigurationManager.AppSettings[ext + "." + str];            
        }

        /// <summary>
        /// Connect the client to the IMAP server
        /// </summary>
        private void Login()
        {
            try
            {
                Parser_mail("imap");
                Client = new ImapClient(Info.Hostname, 993, Info.Credentials.UserName, Info.Credentials.Password, AuthMethod.Login, true);
                Info.Authed = true;

                LogBlock.Text = "Vous êtes actuellement connecté : " + Info.Credentials.UserName;
                Logout_Button.Visibility = Visibility.Visible;
            }
            catch (Exception)
            {
                MessageBox.Show("Identifiants Incorrect");
            }
        }

        /// <summary>
        /// Sets credential and registers them in the cookie file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_button(object sender, RoutedEventArgs e)
        {
            Info.Credentials.UserName = txtLogin.Text;
            Info.Credentials.Password = txtPassword.Password;

            Info.C_Manager.AddEntry("login", Info.Credentials.UserName);
            Info.C_Manager.AddEntry("password", Info.Credentials.Password);

            Login();
        }

        /// <summary>
        /// Logout the client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Logout_button(object sender, RoutedEventArgs e)
        {
            LogBlock.Text = "Vous n'êtes actuellement pas connecté";
            Info.Authed = false;
            Client.Logout();
            Client.Dispose();
            Logout_Button.Visibility = Visibility.Hidden;
        }
    }
}
