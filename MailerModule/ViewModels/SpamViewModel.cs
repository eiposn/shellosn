﻿using MailerModule.Models;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using NhibernateORM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace MailerModule.ViewModels
{
    public class SpamViewModel : INotifyPropertyChanged
    {
        public SpamViewModel(LogInformation _info, IRegionManager _regionManager)
        {
            Info = _info;
            regionManager = _regionManager;
            MailDAO = NHinerbanteDatabaseHandler.getInstance();

            replyCommand = new DelegateCommand(Reply, IsSelected);
            fetchCommand = new DelegateCommand(FetchMailsFromDB, IsFetchable);
            selectCommand = new DelegateCommand(ShowMailDetails, IsSelected);
            restoreCommand = new DelegateCommand(RestoreMails, IsFetchable);
            deleteMailCommand = new DelegateCommand(DeleteMail, IsSelected);

            SpamMails = new ObservableCollection<Mail>();
        }

        public void Init()
        {
            FetchMailsFromDB();
        }

        private LogInformation Info { get; set; }

        private IRegionManager regionManager { get; set; }

        #region Commands

        public Boolean _Fetchable = true;
        public Boolean _Selectable = true;

        /// <summary>
        /// Returns true if there isn't a fetchCommand currently running
        /// </summary>
        /// <returns></returns>
        public Boolean IsFetchable()
        {
            return _Fetchable;
        }

        /// <summary>
        /// Returns true if a mail is currently selected
        /// </summary>
        /// <returns></returns>
        public Boolean IsSelected()
        {
            return _Selectable;
        }

        private DelegateCommand replyCommand;
        private DelegateCommand fetchCommand;
        private DelegateCommand selectCommand;
        private DelegateCommand restoreCommand;
        private DelegateCommand deleteMailCommand;

        public DelegateCommand ReplyCommand
        {
            get
            {
                return replyCommand;
            }
        }

        public DelegateCommand FetchCommand
        {
            get
            {
                return fetchCommand;
            }
        }

        public DelegateCommand SelectCommand
        {
            get
            {
                return selectCommand;
            }
        }

        public DelegateCommand RestoreCommand
        {
            get
            {
                return restoreCommand;
            }
        }

        public DelegateCommand DeleteMailCommand
        {
            get
            {
                return deleteMailCommand;
            }
        }

        #endregion

        #region Getters Setters Mail

        public String TxtName
        {
            get
            {
                return _currentMail.Name;
            }
            set
            {
                _currentMail.Name = value;
                OnPropertyChanged("TxTName");
            }
        }

        public String TxtAddress
        {
            get
            {
                return _currentMail.Address;
            }
            set
            {
                _currentMail.Address = value;
                OnPropertyChanged("TxTAddress");
            }
        }

        public String TxtSubject
        {
            get
            {
                return _currentMail.Subject;
            }
            set
            {
                _currentMail.Subject = value;
                OnPropertyChanged("TxtSubject");
            }

        }

        public String TxtContent
        {
            get
            {
                return _currentMail.MailContent;
            }
            set
            {
                _currentMail.MailContent = value;
                OnPropertyChanged("TxtContent");
            }
        }

        public DateTime Date
        {
            get
            {
                return _currentMail.Date;
            }
            set
            {
                _currentMail.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public uint UID
        {
            get
            {
                return _currentMail.UID;
            }
            set
            {
                _currentMail.UID = value;
                OnPropertyChanged("UID");
            }
        }

        public bool IsSpam
        {
            get
            {
                if (_currentMail.IsSpam == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsTrash
        {
            get
            {
                if (_currentMail.IsTrash == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsAttachment
        {
            get
            {
                if (_currentMail.IsAttachment == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        private Mail _currentMail;

        public Mail CurrentMail
        {
            get { return _currentMail; }
            set
            {
                _currentMail = value;
                OnPropertyChanged("CurrentMail");
            }
        }

        private ObservableCollection<Mail> _mails;

        public ObservableCollection<Mail> SpamMails
        {
            get
            {
                return _mails;
            }
            set
            {
                _mails = value;
                OnPropertyChanged("SpamMails");
            }
        }

        public NHinerbanteDatabaseHandler MailDAO;

        /// <summary>
        /// Signals when a property is modified
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        //////////////////////////////////////////////////////

        /// <summary>
        /// Load mails from the DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FetchMailsFromDB()
        {
            _Fetchable = false;

            SpamMails.Clear();
            var list = MailDAO.GetSpamMails();
            foreach (var mail in list)
            {
                SpamMails.Add(mail);
            }

            _Fetchable = true;
        }

        /// <summary>
        /// Load details for the currently selected mail in the datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ShowMailDetails()
        {
        }

        /// <summary>
        /// Reply to a mail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Reply()
        {
            if (CurrentMail != null)
            {
                var receiver = CurrentMail.Address;
                Info.Parameters.Add("receiver", receiver);

                var moduleAView = new Uri("SendView", UriKind.Relative);
                regionManager.RequestNavigate("MailerViewBase", moduleAView);
            }
        }

        /// <summary>
        /// Delete the current mail selected from the DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteMail()
        {
            if (CurrentMail != null)
            {
                CurrentMail.IsTrash = 1;
                MailDAO.SaveMailChanges(CurrentMail);
                SpamMails.Remove(CurrentMail);
            }
        }

        /// <summary>
        /// Remove address from Spam section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RestoreMails()
        {
            if (CurrentMail != null)
            {
                Info.C_Manager.RemoveIndesirable(CurrentMail.Address);

                MailDAO.SetMailIndesirable(CurrentMail.Address, 0);
                //Mails.RemoveAll(item => item.Address == message.Address);

                var address = CurrentMail.Address;
                for (int i = SpamMails.Count; i > 0; i--)
                {
                    if (SpamMails.ElementAt(i - 1).Address == address)
                    {
                        SpamMails.Remove(SpamMails.ElementAt(i - 1));
                    }
                }
            }
        }

    }
}
