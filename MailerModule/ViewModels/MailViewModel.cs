﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using NhibernateORM;
using System.Collections.ObjectModel;
using S22.Imap;
using System.Net.Mail;
using System.Windows;
using Microsoft.Practices.Prism.Commands;
using System.Threading;
using MailerModule.Services;
using System.Windows.Data;
using MailerModule.Models;
using Microsoft.Practices.Prism.Regions;
using System.Windows.Input;
using System.IO;

namespace MailerModule.ViewModels
{
    public class MailViewModel : INotifyPropertyChanged, INavigationAware
    {      
        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            Destinataire = navigationContext.Parameters["EmailContact"] as String;
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            // This view will handle all navigation view requests for MailView, so we always return true.
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
            // Intentionally not implemented
        }

        public MailViewModel(LogInformation _info, IRegionManager _regionManager)
        {
            Info = _info;
            regionManager = _regionManager;
            MailDAO = NHinerbanteDatabaseHandler.getInstance();

            replyCommand = new DelegateCommand(Reply, IsSelected);
            researchCommand = new DelegateCommand(ResearchMails, IsSearchable);
            deleteMailCommand = new DelegateCommand(DeleteMail, IsSelected);
            getAttachmentCommand = new DelegateCommand(Load_Attachment, IsAttach);
            addIndesirableCommand = new DelegateCommand(AddIndesirable, IsSelected);

            Mails = new ObservableCollection<Mail>();
            BindingOperations.EnableCollectionSynchronization(Mails, _lock);
        }

        /// <summary>
        /// Start long operations in a Task
        /// </summary>
        public void Init()
        {
            Task initAsync = InitAsync();
        }

        /// <summary>
        /// Fetch mails and populate the DB
        /// </summary>
        /// <returns></returns>
        public Task InitAsync()
        {
            Task task = new Task(() =>
            {
                FetchMailsFromDB();

                GifVisibility = Visibility.Hidden;

                if (Info.Authed)
                {
                    Login(Info.Hostname, Info.Credentials.UserName, Info.Credentials.Password);
                    try
                    {
                        PopulateDB();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Récupération des mails interrompu");
                    }
                }
            });
            task.Start();
            return task;
        }

        private LogInformation Info { get; set; }

        private IRegionManager regionManager { get; set; }

        #region Commands

        /// <summary>
        /// Returns true if a mail is currently selected
        /// </summary>
        /// <returns></returns>
        public Boolean IsSelected()
        {
            if (CurrentMail != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if there is an attachment
        /// </summary>
        /// <returns></returns>
        public Boolean IsAttach()
        {
            if (CurrentMail != null && IsAttachment)
            {
                AttachmentVisibility = Visibility.Visible;
                return true;
            }
            AttachmentVisibility = Visibility.Hidden;
            return false;
        }

        /// <summary>
        /// Returns true if the research field is not empty
        /// </summary>
        /// <returns></returns>
        public Boolean IsSearchable()
        {
            if (ResearchField != null && ResearchField.Length > 0)
            {
                return true;
            }
            return false;
        }

        private DelegateCommand replyCommand;
        private DelegateCommand researchCommand;
        private DelegateCommand deleteMailCommand;
        private DelegateCommand getAttachmentCommand;
        private DelegateCommand addIndesirableCommand;


        public DelegateCommand ReplyCommand
        {
            get
            {
                return replyCommand;
            }
        }

        public DelegateCommand ResearchCommand
        {
            get
            {
                return researchCommand;
            }
        }

        public DelegateCommand DeleteMailCommand
        {
            get
            {
                return deleteMailCommand;
            }
        }

        public DelegateCommand GetAttachmentCommand
        {
            get
            {
                return getAttachmentCommand;
            }
        }

        public DelegateCommand AddIndesirableCommand
        {
            get
            {
                return addIndesirableCommand;
            }
        }

        #endregion

        #region Getters Setters Mail

        public String TxtName
        {
            get
            {
                return _currentMail.Name;
            }
            set
            {
                _currentMail.Name = value;
                OnPropertyChanged("TxTName");
            }
        }

        public String TxtAddress
        {
            get
            {
                return _currentMail.Address;
            }
            set
            {
                _currentMail.Address = value;
                OnPropertyChanged("TxTAddress");
            }
        }

        public String TxtSubject
        {
            get
            {
                return _currentMail.Subject;
            }
            set
            {
                _currentMail.Subject = value;
                OnPropertyChanged("TxtSubject");
            }

        }

        public String TxtContent
        {
            get
            {
                return _currentMail.MailContent;
            }
            set
            {
                _currentMail.MailContent = value;
                OnPropertyChanged("TxtContent");
            }
        }

        public DateTime Date
        {
            get
            {
                return _currentMail.Date;
            }
            set
            {
                _currentMail.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public uint UID
        {
            get
            {
                return _currentMail.UID;
            }
            set
            {
                _currentMail.UID = value;
                OnPropertyChanged("UID");
            }
        }

        public bool IsSpam
        {
            get
            {
                if (_currentMail.IsSpam == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsTrash
        {
            get
            {
                if (_currentMail.IsTrash == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsAttachment
        {
            get
            {
                if (_currentMail.IsAttachment == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        private Object _lock = new Object();

        private Mail _currentMail;

        public Mail CurrentMail
        {
            get { return _currentMail; }
            set
            {
                _currentMail = value;
                OnPropertyChanged("CurrentMail");
                ReplyCommand.RaiseCanExecuteChanged();
                DeleteMailCommand.RaiseCanExecuteChanged();
                GetAttachmentCommand.RaiseCanExecuteChanged();
                AddIndesirableCommand.RaiseCanExecuteChanged();
            }
        }

        private Visibility _gifVisibility;

        public Visibility GifVisibility
        {
            get { return _gifVisibility; }
            set
            {
                _gifVisibility = value;
                OnPropertyChanged("GifVisibility");
            }
        }


        private Visibility _attachmentVisibility;

        public Visibility AttachmentVisibility
        {
            get { return _attachmentVisibility; }
            set
            {
                _attachmentVisibility = value;
                OnPropertyChanged("AttachmentVisibility");
            }
        }

        private String _destinataire;

        public String Destinataire
        {
            get { return _destinataire; }
            set
            {
                _destinataire = value;
                OnPropertyChanged("Destinataire");
            }
        }

        public ImapClient Client { get; set; }

        private ObservableCollection<Mail> _mails;

        public ObservableCollection<Mail> Mails
        {
            get
            {
                return _mails;
            }
            set
            {
                _mails = value;
                OnPropertyChanged("Mails");
            }
        }

        private String _researchField;

        public String ResearchField
        {
            get
            {
                return _researchField;
            }
            set
            {
                _researchField = value;
                OnPropertyChanged("ResearchField");
                ResearchCommand.RaiseCanExecuteChanged();
            }
        }

        public NHinerbanteDatabaseHandler MailDAO;

        /// <summary>
        /// Signals when a property is modified
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        ////////////////////////////////////////////////////////////

        /// <summary>
        /// Connect the client to the IMAP server
        /// </summary>
        public void Login(String Hostname, String UserName, String Password)
        {
            try
            {
                Client = new ImapClient(Hostname, 993, UserName, Password, AuthMethod.Login, true);
                Client.NewMessage += new EventHandler<IdleMessageEventArgs>(OnNewMessage);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event when a new message appears on the IMAP servers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnNewMessage(object sender, IdleMessageEventArgs e)
        {
            var res = Client.Search(SearchCondition.All());
            res = res.OrderByDescending(x => x).ToList();

            var message = Client.GetMessage(res.First(), FetchOptions.Normal);

            Mail mail = new Mail();
            mail.Name = message.From.DisplayName;
            mail.Address = message.From.Address;
            mail.Subject = message.Subject;
            mail.MailContent = message.Body.ToString();
            mail.UID = res.First();
            mail.Date = (DateTime)message.Date();

            if (message.Attachments.Count > 0)
            {
                mail.IsAttachment = 1;
            }

            MailDAO.AddMail(mail);
            Mails.Insert(0, mail);
        }

        /// <summary>
        /// Fetch mails from the mail server
        /// Parse them then put them in the local DB
        /// </summary>
        /// <returns></returns>
        public void PopulateDB()
        {
            if (Client == null)
                throw new Exception();

            var uids = Client.Search(SearchCondition.All()).ToList();
            uids = uids.OrderByDescending(x => x).ToList();

            int elemNmbr = uids.Count;
            List<uint> list = new List<uint>();
            List<uint> uidsDB = MailDAO.GetUids();

            for (int i = 0; i < elemNmbr; i++)
            {
                if (!uidsDB.Contains(uids.First()))
                {
                    list.Add(uids.First());
                }
                uids.Remove(uids.First());
            }

            if (list.Count > 0)
            {
                int i = 0;
                while (list.Count > 0)
                {
                    if (list.Count > 25)
                    {
                        i = 25;
                    }
                    else
                    {
                        i = list.Count;
                    }

                    if (Client == null)
                        throw new Exception();

                    IEnumerable<MailMessage> messages = Client.GetMessages(list.Take(i), FetchOptions.Normal);
                    var mails = assocMessages(messages, list.Take(i).ToList());
                    list.RemoveRange(0, i);

                    foreach (var mail in mails)
                    {
                        Mails.Add(mail);
                    }
                }
            }
        }

        /// <summary>
        /// Associate mails with uids and add them to the DB
        /// </summary>
        /// <param name="msgs"></param>
        /// <param name="listUids"></param>
        public List<Mail> assocMessages(IEnumerable<MailMessage> msgs, List<uint> listUids)
        {
            List<Mail> list = new List<Mail>();
            foreach (var message in msgs)
            {
                Mail mail = new Mail();
                mail.Name = message.From.DisplayName;
                mail.Address = message.From.Address;
                mail.Subject = message.Subject;
                mail.MailContent = message.Body.ToString();
                mail.UID = listUids.First();
                mail.Date = (DateTime)message.Date();

                listUids.Remove(listUids.First());
                MailDAO.AddMail(mail);
                list.Add(mail);
            }
            return list;
        }

        /// <summary>
        /// Load mails from the DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FetchMailsFromDB()
        {
            Mails.Clear();
            var count = MailDAO.GetMailsCount();
            var nbr = 0;
            var index = 0;
            while (count > 0)
            {
                nbr = count > 25 ? 25 : count;
                var list = MailDAO.GetMailsTable(nbr, index);
                foreach (var mail in list)
                {
                    Mails.Add(mail);
                }
                count -= nbr;
                index += nbr;
            }
        }

        /// <summary>
        /// Load details for the currently selected mail in the datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ShowMailDetails()
        {
        }

        /// <summary>
        /// Add an email to the Indesirable List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddIndesirable()
        {
            if (CurrentMail != null)
            {
                MailDAO.SetMailIndesirable(CurrentMail.Address, 1);
                //Mails.RemoveAll(item => item.Address == CurrentMail.Address); lorsque Mails == List<Mail>

                var address = CurrentMail.Address;
                for (int i = Mails.Count; i > 0; i--)
                {
                    if (Mails.ElementAt(i - 1).Address == address)
                    {
                        Mails.Remove(Mails.ElementAt(i - 1));
                    }
                }
            }
        }

        /// <summary>
        /// Switch to the setting view
        /// </summary>
        private void SwitchToSettingView()
        {
            var moduleAView = new Uri("SettingView", UriKind.Relative);
            regionManager.RequestNavigate("MailerViewBase", moduleAView);
        }

        /// <summary>
        /// Reply to a mail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Reply()
        {
            if (CurrentMail != null && Info.Authed)
            {
                 var parameter = new NavigationParameters();
                 parameter.Add("EmailContact", CurrentMail.Address);

                var moduleBView = new Uri("SendView", UriKind.Relative);
                regionManager.RequestNavigate("MailerViewBase", moduleBView, parameter);
            }
            if (!Info.Authed)
            {
                SwitchToSettingView();
            }
        }

        /// <summary>
        /// Delete the current mail selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteMail()
        {
            if (CurrentMail != null)
            {
                CurrentMail.IsTrash = 1;
                MailDAO.SaveMailChanges(CurrentMail);
                Mails.Remove(CurrentMail);
            }
        }

        /// <summary>
        /// Search mails for their criterias
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ResearchMails()
        {
            if (ResearchField != "")
            {
                Mails.Clear();
                List<Mail> listAddress = MailDAO.GetMailsByAddress(ResearchField);
                List<Mail> listSubject = MailDAO.GetMailsBySubject(ResearchField);

                foreach (Mail mail in listAddress)
                {
                    if (listSubject.Contains(mail))
                    {
                        listSubject.Remove(mail);
                    }
                }
                foreach (var item in listAddress)
                {
                    Mails.Add(item);
                }
                foreach (var item in listSubject)
                {
                    Mails.Add(item);
                }
            }
            else if (ResearchField == "")
            {
                FetchMailsFromDB();
            }
        }

        /// <summary>
        /// Load an attachment from a stream
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Load_Attachment()
        {
            String path = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            var confirmResult = MessageBox.Show("Do you want to download the file?",
                                     "Downloading",
                                     MessageBoxButton.YesNo);

            MailMessage message = Client.GetMessage(CurrentMail.UID, FetchOptions.Normal);
            if (message != null)
            {
                if (confirmResult == MessageBoxResult.Yes)
                {

                    foreach (var attachment in message.Attachments)
                    {
                        var key = CurrentMail.UID.ToString() + attachment.Name;
                        if (!Info.C_Manager.Cookies.ContainsKey(key))
                        {
                            Info.C_Manager.AddEntry(key, "true");
                            var fileStream = File.Create(path + "/" + attachment.Name);
                            CopyStream(attachment.ContentStream, fileStream);
                            Info.C_Manager.RemoveEntry(key);
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Copy a stream into a filestream
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
    }
}
