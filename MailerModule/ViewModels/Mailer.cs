﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.ComponentModel;
using System.Net.Mime;

namespace AppMailer
{
    class Mailer
    {
        public Mailer()
        {
        }

        public Mailer(string _to, string _from, string _subject, string _body)
        {
            try
            {
                toAddress = new MailAddress(_to);
                fromAdress = new MailAddress(_from);
                mail = new MailMessage(FromAdress, ToAddress);
                mail.Subject = _subject;
                mail.Body = _body;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }

        public void PrepareMail()
        {
            try
            {
                mail = new MailMessage(fromAdress, toAddress);
                mail.Subject = subject;
                mail.Body = message;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }

        public async void Send(String hostname, SendCompletedEventHandler PopUpSendCompleted)
        {
            try
            {
                using (SmtpClient smtpClient = new SmtpClient(hostname, 587))
                {
                    smtpClient.Credentials = cred;
                    smtpClient.EnableSsl = true;
                    smtpClient.SendCompleted += PopUpSendCompleted;
                    await smtpClient.SendMailAsync(mail);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Your credentials are incorrects or your mail account does not allow this operation");
                throw;
            }
        }

        public void AddAttachments(List<string>  filePaths)
        {
            foreach (var file in filePaths)
            {
                Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
                // Add time stamp information for the file.
                ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(file);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
                // Add the file attachment to this e-mail message.
                mail.Attachments.Add(data);
            }
        }

        public void FromToAddress(string _from, string _to)
        {
            try
            {
                toAddress = new MailAddress(_to);
                fromAdress = new MailAddress(_from);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }

        private int port;
        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        private MailMessage mail;
        public MailMessage Mail
        {
            get { return mail; }
            set { mail = value; }
        }

        private MailAddress toAddress;
        public MailAddress ToAddress
        {
            get { return toAddress; }
            set { toAddress = value; }
        }

        private MailAddress fromAdress;
        public MailAddress FromAdress
        {
            get { return fromAdress; }
            set { fromAdress = value; }
        }

        private string subject;
        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private NetworkCredential cred;
        public NetworkCredential Cred
        {
            get { return cred; }
            set { cred = value; }
        }
    }
}
