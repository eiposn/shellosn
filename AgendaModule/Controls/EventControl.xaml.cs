﻿using AgendaModule.ViewModels;
using NhibernateORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AgendaModule.Controls
{
    /// <summary>
    /// Logique d'interaction pour EventControl.xaml
    /// </summary>
    public partial class EventControl : UserControl
    {
        public Agenda Source { get; set; }
        public Boolean IsCollapsed { get; set; }
        
        public EventControl(AgendaViewModel agendaVM)
        {
            InitializeComponent();
            this.DataContext = agendaVM;
            IsCollapsed = true;
        }

        public void Init(Agenda obj)
        {
            Source = obj;
            Date.Text = obj.Date.ToString("dd MMMM yyyy");
            Subject.Text = obj.Subject;
            BlockContent.Text = obj.Content;
        }
    }
}
