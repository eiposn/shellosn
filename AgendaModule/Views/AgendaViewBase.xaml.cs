﻿using AgendaModule.ViewModels;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AgendaModule.Views
{
    /// <summary>
    /// Logique d'interaction pour AgendaViewBase.xaml
    /// </summary>
    public partial class AgendaViewBase : UserControl
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        protected AgendaViewModel agendaVM;

        /// <summary>
        /// Initializes the class
        /// </summary>
        public AgendaViewBase(IRegionManager _regionManager)
        {
            InitializeComponent();
            this.regionManager = _regionManager;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();

            container.RegisterType<Object, WrapperView>("WrapperView");
            container.RegisterType<Object, AddEventView>("AddEventView");
            container.RegisterType<Object, ModifyEventView>("ModifyEventView");

            agendaVM = new AgendaViewModel(_regionManager);
            this.container.RegisterInstance<AgendaViewModel>(agendaVM);

            this.DataContext = agendaVM;
        }

        /// <summary>
        /// Open the setting view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenAddEventView(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var moduleAView = new Uri("AddEventView", UriKind.Relative);
            regionManager.RequestNavigate("AgendaViewBase", moduleAView);
        }

        private void RefreshEventList(object sender, MouseButtonEventArgs e)
        {
            agendaVM.LoadEventList();
        }
    }
}
