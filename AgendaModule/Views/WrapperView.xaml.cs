﻿using AgendaModule.ViewModels;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AgendaModule.Views
{
    /// <summary>
    /// Logique d'interaction pour WrapperView.xaml
    /// </summary>
    public partial class WrapperView : UserControl
    {
        public AgendaViewModel AgendaVM;
        private IRegionManager regionManager { get; set; }

        public WrapperView(IRegionManager _regionManager, AgendaViewModel _agendaVM)
        {
            InitializeComponent();
            this.regionManager = _regionManager;

            AgendaVM = _agendaVM;
            this.DataContext = AgendaVM;
        }
    }
}
