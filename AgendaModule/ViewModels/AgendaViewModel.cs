﻿using AgendaModule.Controls;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using NhibernateORM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AgendaModule.ViewModels
{
    public class AgendaViewModel : INotifyPropertyChanged
    {
        public AgendaViewModel(IRegionManager _regionManager)
        {
            this.regionManager = _regionManager;
            AgendaDAO = NHinerbanteDatabaseHandler.getInstance();
            Events = new ObservableCollection<EventControl>();

            collapsingCommand = new DelegateCommand(DoCollapsingCommand);
            validateCommand = new DelegateCommand(DoValidateCommand);
            cancelCommand = new DelegateCommand(DoCancelCommand);
            modifyCommand = new DelegateCommand(DoModifyEventCommand, IsSelected);
            deleteCommand = new DelegateCommand(DoDeleteEventCommand, IsSelected);
            validateModCommand = new DelegateCommand(DoValidateModCommand, IsFormChecked);
            researchCommand = new DelegateCommand(ResearchEvents, IsSearchable);

            TxtDate = new DateTime(2016, 01, 01);
            Init();
        }

        public void Init()
        {
            LoadEventList();
        }

        public void LoadEventList()
        {
            Events.Clear();
            var list = AgendaDAO.GetAgendaTable();
            foreach (var item in list)
            {
                EventControl tmp = new EventControl(this);
                tmp.Init(item);
                Events.Add(tmp);
            }
        }

        #region Commands

        /// <summary>
        /// Returns true if the research field is not empty
        /// </summary>
        /// <returns></returns>
        public Boolean IsSearchable()
        {
            if (ResearchField != null && ResearchField.Length > 0)
            {
                return true;
            }
            return false;
        }

        private DelegateCommand collapsingCommand;
        private DelegateCommand validateCommand;
        private DelegateCommand cancelCommand;
        private DelegateCommand modifyCommand;
        private DelegateCommand deleteCommand;
        private DelegateCommand validateModCommand;
        private DelegateCommand researchCommand;

        public DelegateCommand ResearchCommand
        {
            get
            {
                return researchCommand;
            }
        }

        public DelegateCommand CollapsingCommand
        {
            get
            {
                return collapsingCommand;
            }
        }


        public DelegateCommand ValidateCommand
        {
            get
            {
                return validateCommand;
            }
        }

        public DelegateCommand CancelCommand
        {
            get
            {
                return cancelCommand;
            }
        }

        public DelegateCommand ModifyCommand
        {
            get
            {
                return modifyCommand;
            }
        }

        public DelegateCommand DeleteCommand
        {
            get
            {
                return deleteCommand;
            }
        }

        public DelegateCommand ValidateModCommand
        {
            get
            {
                return validateModCommand;
            }
        }

        public Boolean _Selectable = true;

        public Boolean IsSelected()
        {
            return _Selectable;
        }

        private bool IsFormChecked()
        {
            return true;
        }

        #endregion

        #region getters setters Agenda

        private string txtSubject;
        private string txtContent;
        private DateTime txtDate;


        public String TxtSubject
        {
            get
            {
                return txtSubject;
            }
            set
            {
                txtSubject = value;
                OnPropertyChanged("TxtSubject");
            }
        }

        public String TxtContent
        {
            get
            {
                return txtContent;
            }
            set
            {
                txtContent = value;
                OnPropertyChanged("TxtContent");
            }
        }


        public DateTime TxtDate
        {
            get
            {
                return txtDate;
            }
            set
            {
                txtDate = value;
                OnPropertyChanged("TxtDate");
            }
        }

        #endregion

        private ObservableCollection<EventControl> _events;

        public ObservableCollection<EventControl> Events
        {
            get { return _events; }
            set
            {
                _events = value;
                OnPropertyChanged("Events");
            }
        }


        private String _researchField;

        public String ResearchField
        {
            get
            {
                return _researchField;
            }
            set
            {
                _researchField = value;
                OnPropertyChanged("ResearchField");
                ResearchCommand.RaiseCanExecuteChanged();
            }
        }

        private EventControl _currentEvent;

        public EventControl CurrentEvent
        {
            get { return _currentEvent; }
            set
            {
                _currentEvent = value;
                OnPropertyChanged("CurrentEvent");
                DoCollapsingCommand();
            }
        }

        private EventControl _lastEvent;

        public EventControl LastEvent
        {
            get { return _lastEvent; }
            set
            {
                _lastEvent = value;
                OnPropertyChanged("LastEvent");
            }
        }

        public NHinerbanteDatabaseHandler AgendaDAO;

        private IRegionManager regionManager { get; set; }

        /// <summary>
        /// Signals when a property is modified
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        //////////////////////////////////////////////////////////////////

        /// <summary>
        /// Open or Close an eventControl when clicked
        /// </summary>
        private void DoCollapsingCommand()
        {
            if (CurrentEvent != null)
            {
                if (CurrentEvent.Contenu.Visibility == Visibility.Collapsed)
                {
                    CurrentEvent.Contenu.Visibility = Visibility.Visible;
                    if (LastEvent != null)
                    {
                        LastEvent.Contenu.Visibility = Visibility.Collapsed;
                    }
                    LastEvent = CurrentEvent;
                }
                else
                {
                    CurrentEvent.Contenu.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Load an eventControl whith the form's fields
        /// Add it to the DB
        /// </summary>
        private void DoValidateCommand()
        {
            if (TxtSubject != null && TxtDate != null && TxtContent != null)
            {
                Agenda source = new Agenda();
                source.Date = TxtDate;
                source.Subject = TxtSubject;
                source.Content = TxtContent;

                AgendaDAO.AddEvent(source);

                EventControl tmp = new EventControl(this);
                tmp.Init(source);
                Events.Add(tmp);

                ClearFormEvent();

                var moduleAView = new Uri("WrapperView", UriKind.Relative);
                regionManager.RequestNavigate("AgendaViewBase", moduleAView);
            }
            else
            {
                MessageBox.Show("Remplissez les champs");
            }
        }

        /// <summary>
        /// Delete an event from the DB
        /// </summary>
        private void DoDeleteEventCommand()
        {
            if (LastEvent != null)
            {
                var toDel = LastEvent.Source;
                AgendaDAO.DeleteEvent(toDel);
                Events.Remove(LastEvent);
            }
        }

        /// <summary>
        /// Modify an event from the DB
        /// </summary>
        private void DoModifyEventCommand()
        {
            if (LastEvent != null)
            {
                var source = LastEvent.Source;

                TxtSubject = source.Subject;
                TxtContent = source.Content;
                TxtDate = source.Date;

                var moduleAView = new Uri("ModifyEventView", UriKind.Relative);
                regionManager.RequestNavigate("AgendaViewBase", moduleAView);
            }
        }

        /// <summary>
        /// Return to the main View and Clear the form
        /// </summary>
        private void DoCancelCommand()
        {
            var moduleAView = new Uri("WrapperView", UriKind.Relative);
            regionManager.RequestNavigate("AgendaViewBase", moduleAView);

            ClearFormEvent();
        }

        /// <summary>
        /// Apply modification in the local DB
        /// </summary>
        private void DoValidateModCommand()
        {
            if (TxtSubject != null && TxtContent != null && TxtDate != null)
            {
                var _event = LastEvent.Source;

                _event.Subject = TxtSubject;
                _event.Content = TxtContent;
                _event.Date = TxtDate;


                AgendaDAO.SaveEventChanges(_event);
                LastEvent.Source = _event;

                LoadEventList();
                ClearFormEvent();

                var moduleAView = new Uri("WrapperView", UriKind.Relative);
                regionManager.RequestNavigate("AgendaViewBase", moduleAView);
            }
        }

        /// <summary>
        /// Returns the events by the subject selected
        /// </summary>
        private void ResearchEvents()
        {
            var list = AgendaDAO.GetEventBySubject(ResearchField);

            Events.Clear();
            foreach (var item in list)
            {
                EventControl tmp = new EventControl(this);
                tmp.Init(item);
                Events.Add(tmp);
            }
        }


        /// <summary>
        /// Clear the form
        /// </summary>
        private void ClearFormEvent()
        {
            TxtSubject = null;
            TxtContent = null;
            TxtDate = DateTime.Today;
        }
    }
}
