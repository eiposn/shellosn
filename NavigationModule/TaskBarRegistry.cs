﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSN
{
    public class TaskBarRegistry
    {
        static protected ObservableCollection<ModuleInform> moduleInfos;

        public ObservableCollection<ModuleInform> ModuleInfos
        {
            get { return moduleInfos; }
        }

         public TaskBarRegistry()
        {
            moduleInfos = new ObservableCollection<ModuleInform>();
        }

         /// <summary>
         /// Register a module for the taskbar
         /// </summary>
         /// <param name="PairedWorkspaceViewName">The name of the View.</param>
         /// <param name="ModuleName">The name of the module (Used to show the name of the module in taskbar) .</param>
        public void RegisterMenuItem(string PairedWorkspaceViewName, string ModuleName)
        {
            moduleInfos.Add(new ModuleInform(PairedWorkspaceViewName, ModuleName));
        }

        public void RegisterMenuItem(string PairedWorkspaceViewName, string ModuleName, string imgPath)
        {
            moduleInfos.Add(new ModuleInform(PairedWorkspaceViewName, ModuleName, imgPath));
        }

        public void RegisterMenuItem(string PairedWorkspaceViewName, string ModuleName, string imgPath, Uri _uri)
        {
            moduleInfos.Add(new ModuleInform(PairedWorkspaceViewName, ModuleName, imgPath, _uri));
        }
    }
}
