﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OSN
{
    public class ModuleInform
    {
        public String _PairedWorkspaceViewName;
        public String _ModuleName;
        public Icon _icon;
        public Image _iconSRC;
        public string _ImgPath;
        public Uri _Uri;

        public Uri Uri
        {
            get { return _Uri; }
        }
        public string ModuleName
        {
            get { return _ModuleName; }
        }
        public string ImgPath
        {
            get { return _ImgPath; }
        }
        public string PairedWorkspaceViewName
        {
            get { return _PairedWorkspaceViewName; }
        }
        public Icon Icon
        {
            get { return _icon; }
        }

        public Image IconSRC
        {
            get { return _iconSRC; }
        }

        public ModuleInform(String _PairedWorkspaceViewName, String _ModuleName)
        {
            this._PairedWorkspaceViewName = _PairedWorkspaceViewName;
            this._ModuleName = _ModuleName;
        }

        public ModuleInform(String _PairedWorkspaceViewName, String _ModuleName, String _ImgPath)
        {
            this._ImgPath = _ImgPath;
            this._PairedWorkspaceViewName = _PairedWorkspaceViewName;
            this._ModuleName = _ModuleName;
        }

        public ModuleInform(String _PairedWorkspaceViewName, String _ModuleName, String _ImgPath, Uri _Uri)
        {
            this._ImgPath = _ImgPath;
            this._PairedWorkspaceViewName = _PairedWorkspaceViewName;
            this._ModuleName = _ModuleName;
            this._Uri = _Uri;
        }
    }
}
