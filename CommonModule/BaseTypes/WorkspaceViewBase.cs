﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CommonModule.BaseTypes
{
    public class WorkspaceViewBase : UserControl, IRegionMemberLifetime
    {
        protected bool _KeepAlive = true;

        /// <summary>
        /// Set true if the module need to save the current session.
        /// </summary>
        public bool KeepAlive
        {
            get { return _KeepAlive; }
        }

        protected string _ViewName = "";
        public string ViewName
        {
            get { return _ViewName; }
        }
    }
}
