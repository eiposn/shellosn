﻿using Microsoft.Practices.Prism.Modularity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonModule
{
    [Module(ModuleName = "CommonModule", OnDemand = false)]
    public class CommonModule : IModule
    {
        public void Initialize()
        {
        }
    }
}
