﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhibernateORM
{
    public class Agenda
    {
        public virtual Guid Id { get; set; }
        public virtual string Subject { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Content { get; set; }
    }
}
