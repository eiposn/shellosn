﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Driver;
using NHibernate.Cfg;
using NHibernate;
using NHibernate.Criterion;
using System.Configuration;
using System.Collections;
using NHibernate.Transform;

namespace NhibernateORM
{
    public class NHinerbanteDatabaseHandler
    {
        #region Init Properties

        private static object syncRoot = new Object();

        private static volatile NHinerbanteDatabaseHandler _instance;

        public static ISession Session { get; private set; }

        private NHibernate.Cfg.Configuration _cfg;

        #endregion

        #region Init Functions

        private static NHinerbanteDatabaseHandler Instance
        {
            get
            {
                return Instance;
            }
        }

        public static NHinerbanteDatabaseHandler getInstance()
        {
            if (null == _instance)
            {
                lock (syncRoot)
                {
                    if (null == _instance)
                    {
                        _instance = new NHinerbanteDatabaseHandler();
                    }
                }
            }
            return _instance;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        private NHinerbanteDatabaseHandler()
        {
            _cfg = new NHibernate.Cfg.Configuration();
            Init();
        }

        /// <summary>
        /// This function init the class, it need to be called first thing
        /// </summary>
        public void Init()
        {
            _cfg.SetProperty(NHibernate.Cfg.Environment.ConnectionProvider, "NHibernate.Connection.DriverConnectionProvider");
            _cfg.SetProperty(NHibernate.Cfg.Environment.Dialect, "NHibernate.Dialect.SQLiteDialect");
            _cfg.SetProperty(NHibernate.Cfg.Environment.ConnectionString, "Data Source=OSNDatabase.db; Version=3;New=False");
            _cfg.SetProperty(NHibernate.Cfg.Environment.ConnectionDriver, "NHibernate.Driver.SQLite20Driver");
            _cfg.SetProperty(NHibernate.Cfg.Environment.QuerySubstitutions, "true=1;false=0");
            _cfg.SetProperty(NHibernate.Cfg.Environment.ShowSql, "true");
            _cfg.AddAssembly(typeof(NHinerbanteDatabaseHandler).Assembly);

            // Get an NHibernate Session
            ISessionFactory sessions = _cfg.BuildSessionFactory();
            Session = sessions.OpenSession();
        }

        #endregion

        #region Mails Functions

        //public List<Mail> GetMailsHeaders()
        //{
        //    var list = Session.CreateCriteria(typeof(Mail))
        //                                .SetProjection(Projections.ProjectionList()
        //                                .Add(Projections.Property("Name"))
        //                                .Add(Projections.Property("Address"))
        //                                .Add(Projections.Property("Subject"))
        //                                .Add(Projections.Property("Date"))
        //                                .Add(Projections.Id(), "Id")
        //                                .Add(Projections.Property("UID")))
        //                            //.SetResultTransformer(Transformers.AliasToBean(typeof(Mail)))
        //                            //.List<Mail>().ToList();
        //                            .List<IList>()
        //                            .Select(l => new Mail()
        //                            {
        //                                Name = (string)l[0],
        //                                Address = (string)l[1],
        //                                Subject = (string)l[2],
        //                                Date = (DateTime)l[3],
        //                                Id = (Guid)l[4],
        //                                UID = (uint)l[5],
        //                                MailContent = ""
        //                            });

        //    return list.ToList<Mail>();
        //}

        public int GetMailsCount()
        {
            var count = Session.CreateCriteria<Mail>()
                                            .SetProjection(
                                            Projections.Count(Projections.Id()))
                                            .UniqueResult<int>();
            return count;
        }

        public List<Mail> GetMailsTable()
        {
            var list = Session.CreateCriteria(typeof(Mail))
                                        .Add(Restrictions.Eq("IsSpam", 0))
                                        .Add(Restrictions.Eq("IsTrash", 0))
                                        .AddOrder(Order.Desc("Date"))
                                        .List<Mail>().ToList();
            return list;
        }

        public List<Mail> GetMailsTable(int maxRange, int index = 0)
        {
            var list = Session.CreateCriteria(typeof(Mail))
                                        .Add(Restrictions.Eq("IsSpam", 0))
                                        .Add(Restrictions.Eq("IsTrash", 0))
                                        .AddOrder(Order.Desc("Date"))
                                        .SetMaxResults(maxRange).SetFirstResult(index)
                                        .List<Mail>().ToList();
            return (list);
        }

        public List<Mail> GetMailsByAddress(string param)
        {
            var list = Session.CreateCriteria(typeof(Mail))
                                        .Add(Restrictions.InsensitiveLike("Address", param + "%"))
                                        .List<Mail>().ToList();
            return (list);
        }


        public List<Mail> GetSpamMails()
        {
            var list = Session.CreateCriteria(typeof(Mail))
                                        .Add(Restrictions.Eq("IsSpam", 1))
                                        .List<Mail>().ToList();
            return (list);
        }

        public List<Mail> GetTrashMails()
        {
            var list = Session.CreateCriteria(typeof(Mail))
                                        .Add(Restrictions.Eq("IsTrash", 1))
                                        .List<Mail>().ToList();
            return (list);
        }

        public List<Mail> GetMailsBySubject(string param)
        {
            var list = Session.CreateCriteria(typeof(Mail))
                                        .Add(Restrictions.InsensitiveLike("Subject", param + "%"))
                                        .List<Mail>().ToList();
            return (list);
        }

        public Mail GetMail(uint uid)
        {
            Mail mail = Session.CreateCriteria(typeof(Mail)).Add(Restrictions.Eq("UID", uid)).UniqueResult<Mail>();

            return mail;
        }

        public List<uint> GetUids()
        {
            var list = Session.CreateCriteria(typeof(Mail))
                            .SetProjection(Projections.ProjectionList().Add(Projections.Property("UID")))
                            .List<uint>().ToList();
            return (list);
        }

        public void SetMailIndesirable(string target, int value)
        {
            using (var transaction = Session.BeginTransaction())
            {
                String hqlUpdate = "update Mail m set m.IsSpam = :value where m.Address = :target";

                int updatedEntities = Session.CreateQuery(hqlUpdate)
                        .SetString("target", target)
                        .SetInt32("value", value)
                        .ExecuteUpdate();
                transaction.Commit();
            }
        }

        public void AddMail(Mail newMail)
        {
            SaveNewMail(newMail);
        }

        public void SaveNewMail(Mail mail)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Save(mail);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void SaveMailChanges(Mail mail)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.SaveOrUpdate(mail);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void DeleteMail(uint uid)
        {
            Mail mailToDelete = GetMail(uid);

            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(mailToDelete);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void DeleteMail(Mail mail)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(mail);
                transaction.Commit();
                Session.Clear();
            }
        }

        #endregion

        #region Contacts Functions

        public List<Contact> GetContactsTable()
        {
            var list = Session.CreateCriteria(typeof(Contact))
                                        .List<Contact>().ToList();
            return list;
        }

        public List<Contact> GetContactByFirstName(string param)
        {
            var list = Session.CreateCriteria(typeof(Contact))
                                        .Add(Restrictions.InsensitiveLike("FirstName", param + "%"))
                                        .List<Contact>().ToList();
            return (list);
        }

        public List<Contact> GetContactByLastName(string param)
        {
            var list = Session.CreateCriteria(typeof(Contact))
                                        .Add(Restrictions.InsensitiveLike("LastName", param + "%"))
                                        .List<Contact>().ToList();
            return (list);
        }

        public List<Contact> GetContactByPhone(string param)
        {
            var list = Session.CreateCriteria(typeof(Contact))
                                        .Add(Restrictions.InsensitiveLike("Phone", param + "%"))
                                        .List<Contact>().ToList();
            return (list);
        }


        public void AddContact(Contact newContact)
        {
            SaveNewContact(newContact);
        }

        public void SaveNewContact(Contact contact)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Save(contact);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void DeleteContact(Contact contact)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(contact);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void SaveContactChanges(Contact contact)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.SaveOrUpdate(contact);
                transaction.Commit();
                Session.Clear();
            }
        }

        public List<Contact> GetContactsByLetter(string param)
        {
            var list = Session.CreateCriteria(typeof(Contact))
                                        .Add(Restrictions.InsensitiveLike("LastName", param + "%"))
                                        .List<Contact>().ToList();
            return (list);
        }

        #endregion

        #region Agenda Functions

        public List<Agenda> GetAgendaTable()
        {
            var list = Session.CreateCriteria(typeof(Agenda))
                                        .List<Agenda>().ToList();
            return list;
        }

        public List<Agenda> GetEventBySubject(string param)
        {
            var list = Session.CreateCriteria(typeof(Agenda))
                                        .Add(Restrictions.InsensitiveLike("Subject", "%" + param + "%"))
                                        .List<Agenda>().ToList();
            return (list);
        }

        public List<Agenda> GetEventByDate(DateTime param)
        {
            var list = Session.CreateCriteria(typeof(Agenda))
                                        .Add(Restrictions.InsensitiveLike("Date", param))
                                        .List<Agenda>().ToList();
            return (list);
        }

        public void AddEvent(Agenda newEvent)
        {
            SaveNewEvent(newEvent);
        }

        public void SaveNewEvent(Agenda newEvent)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Save(newEvent);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void DeleteEvent(Agenda item)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(item);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void SaveEventChanges(Agenda item)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.SaveOrUpdate(item);
                transaction.Commit();
                Session.Clear();
            }
        }

        #endregion

        public void SaveNewFavorite(Favorite favorite)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Save(favorite);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void SaveNewHistory(History history)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Save(history);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void DeleteAllHistory()
        {
            using (var transaction = Session.BeginTransaction())
            {
                var query = Session.CreateQuery("delete from History");
                query.ExecuteUpdate();
                transaction.Commit();
            }
        }

        public void DeleteHistory(History history)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(history);
                transaction.Commit();
                Session.Clear();
            }
        }

        public void DeleteFavorite(Favorite favorite)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(favorite);
                transaction.Commit();
                Session.Clear();
            }
        }
        public List<Favorite> GetFavorite()
        {
            List<Favorite> list = Session.CreateCriteria(typeof(Favorite)).List<Favorite>().ToList();
            return (list);
        }

        public List<History> GetHistory()
        {
            List<History> list = Session.CreateCriteria(typeof(History)).AddOrder(Order.Desc("Date")).List<History>().ToList();
            return (list);
        }

        public BrowserSettings GetHome()
        {
            BrowserSettings home = Session.CreateCriteria(typeof(BrowserSettings)).Add(Restrictions.Eq("Name", "Accueil")).UniqueResult<BrowserSettings>();
            return (home);
        }

        public void SaveHome(BrowserSettings home)
        {
            using (var transaction = Session.BeginTransaction())
            {
                String hqlUpdate = "update BrowserSettings m set m.Value = :value where m.Name = :name";

                int updatedEntities = Session.CreateQuery(hqlUpdate)
                        .SetString("value", home.Value)
                        .SetString("name", "Accueil")
                        .ExecuteUpdate();
                transaction.Commit();
            }
        }
    }
}
