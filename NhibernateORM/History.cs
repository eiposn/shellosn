﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhibernateORM
{
    public class History
    {
        public virtual int Id { get; set; }
        public virtual string Url { get; set; }
        public virtual DateTime Date { get; set; }
    }
}
