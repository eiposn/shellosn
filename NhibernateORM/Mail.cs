﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhibernateORM
{
    public class Mail
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual string Subject { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string MailContent { get; set; }
        public virtual uint UID { get; set; }
        public virtual int IsTrash { get; set; }
        public virtual int IsSpam { get; set; }
        public virtual int IsAttachment { get; set; }
    }
}
