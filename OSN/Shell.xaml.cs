﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Prism.Regions;
using System.Windows.Controls.Ribbon;
using CommonModule.BaseTypes;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System.Configuration;
using System.ComponentModel;

namespace OSN
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class Shell : Window
    {
        private static Uri DesktopViewUri = new Uri("/DesktopView", UriKind.Relative);
        private IModuleManager moduleManager;
        private TaskBarRegistry taskBarRegistry;
        public IRegionManager RegionManager;
        public bool HelperExpanded = true;
        private GridLength full;
        private System.Windows.Forms.Timer timer1;

        public Shell(IModuleManager moduleManager, IRegionManager _regionmanager, TaskBarRegistry _taskBarRegistry)
        {
            this.moduleManager = moduleManager;
            this.RegionManager = _regionmanager;
            this.taskBarRegistry = _taskBarRegistry;
            InitializeComponent();
            Modules.ItemsSource = taskBarRegistry.ModuleInfos;
            taskBarRegistry.RegisterMenuItem("Close", "Close", "/OSN;component/Resources/iconmonstr-checkbox-16-icon-256.png");
            full = IsExpended.Width;
            Date.Text = DateTime.Now.ToShortDateString();
            Hours.Text = DateTime.Now.ToString("HH:mm");
            InitTimer();
        }

        public void InitTimer()
        {
            timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 60000; // in miliseconds
            timer1.Start();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Hours.Text = DateTime.Now.ToString("HH:mm");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.moduleManager.LoadModuleCompleted += this.ModuleManager_LoadModuleCompleted;
        }

        void ModuleManager_LoadModuleCompleted(object sender, LoadModuleCompletedEventArgs e)
        {
           /* if (e.ModuleInfo.ModuleName == "ModuleDesktop")
            this.RegionManager.RequestNavigate(
                            "MainRegion",
                            DesktopViewUri);*/
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void FirePropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (HelperExpanded == true)
             IsExpended.Width = new System.Windows.GridLength(0);
            else
                IsExpended.Width = full;
            HelperExpanded = !HelperExpanded;
            string HelperRegionName = ConfigurationManager.AppSettings["HelperRegionName"];
        }

        private void ShowModuleHelper(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void ShowModule(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (Modules.SelectedItem != null)
            {
                string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];
                ModuleInform select = (ModuleInform)Modules.SelectedItem;
                if (select.ModuleName == "Close")
                {
                   if ( MessageBox.Show("Are you sure you want to close OSN ?",
                      "Close OSN", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        Application.Current.Shutdown();
                }
                else
                RegionManager.RequestNavigate(workspaceRegionName, select.PairedWorkspaceViewName);
            }
        }
    }
}
