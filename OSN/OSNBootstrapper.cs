﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Prism.Modularity;
using System.Windows;
using Microsoft.Practices.ServiceLocation;
using System.Diagnostics;
using ModuleDesktop;
using Microsoft.Practices.Unity;
using CommonModule.BaseTypes;

namespace OSN
{
    public partial class OSNBootstrapper : UnityBootstrapper
    {
        protected override IModuleCatalog CreateModuleCatalog()
        {
            return new AggregateModuleCatalog();
        }

        protected override DependencyObject CreateShell()
        {
            return ServiceLocator.Current.GetInstance<Shell>();
        }

        /// <summary>
        /// Set the Mainview to the shell.
        /// </summary>
        protected override void InitializeShell()
        {
            base.InitializeShell();

            Application.Current.MainWindow = (Window)this.Shell;
            Application.Current.MainWindow.Show();
        }

        /// <summary>
        /// Configure the container all dependences need to be register.
        /// </summary>
        protected override void ConfigureContainer()   
        {
            var container = this.Container;
            base.ConfigureContainer();
            this.RegisterTypeIfMissing(typeof(IModuleManager), typeof(ModuleManager), true);
            container.RegisterType<DesktopView, DesktopView>("DesktopView");
            container.RegisterType<Object, WorkspaceViewBase>("WorkspaceViewBase");
            container.RegisterInstance<IUnityContainer>(container);
            container.RegisterInstance<TaskBarRegistry>(new TaskBarRegistry());
           //To do add specific instance of container like the network class.
        }


        /// <summary>
        /// Look for dll module in a file.
        /// </summary>
        protected override void ConfigureModuleCatalog()
        {
            Type ModuleDesktop = typeof(ModuleDesktopWorkspaceView);
            ModuleCatalog.AddModule(new ModuleInfo(ModuleDesktop.Name, ModuleDesktop.AssemblyQualifiedName));
            DirectoryModuleCatalog directoryCatalog = new DirectoryModuleCatalog() { ModulePath = @".\Modules" };
            ((AggregateModuleCatalog)ModuleCatalog).AddCatalog(directoryCatalog);
        }
    }
}

