﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Net.Sockets;
using System.Net;

namespace Hik.Misc
{
    /// <summary>
    /// miscellaneous static methods
    /// </summary>
    public static class Misc
    {
        /// <summary>
        /// This function generates a unique token.
        /// </summary>
        /// <returns>It returns the string who contains the unique Token</returns>
        static public String GenerateUniqueToken()
        {
            int size = 8;

            char[] alphaNumeric = new char[62];
            alphaNumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];

            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[size];
            crypto.GetNonZeroBytes(data);

            StringBuilder result = new StringBuilder(size);
            foreach (byte i in data)
            {
                result.Append(alphaNumeric[i % (alphaNumeric.Length - 1)]);
            }
            return result.ToString();
        }

        /// <summary>
        /// Allows you to have your ipAddress
        /// </summary>
        /// <returns>return Local Ip Address</returns>
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
