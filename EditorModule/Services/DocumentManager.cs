﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using System.IO;
using System.Windows.Media;

namespace EditorModule.Services
{
    class DocumentManager
    {

        private int _filterIndex = 1;
        private string _currentFile;
        private RichTextBox _textBox;

        /// <summary>
        /// Init the text zone
        /// </summary>
        /// <param name="textBox"></param>
        public DocumentManager(RichTextBox textBox)
        {
            _textBox = textBox;
        }

        /// <summary>
        /// Create a new document to start writing in.
        /// Show a dialog asking if you want to save the current doccument.
        /// </summary>
        public void NewDocument()
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Do you want to save your existing file first?", "Save Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                this.SaveDocument();
            }

            if (!String.IsNullOrEmpty(_currentFile))
            {
                this._currentFile = null;
            }
            this._textBox.Document.Blocks.Clear();
        }

        /// <summary>
        /// Show an open file dialog to let you select a file to open and start writing in.
        /// </summary>
        /// <returns></returns>
        public bool OpenDocument()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text Files (*.txt)|*.txt|Rich Text Format (*.rtf)|*.rtf";

            if (dlg.ShowDialog() == true)
            {
                _currentFile = dlg.FileName;

                using (Stream stream = dlg.OpenFile())
                {
                    TextRange range = new TextRange(_textBox.Document.ContentStart, _textBox.Document.ContentEnd);
                    if (dlg.FilterIndex == 1) range.Load(stream, DataFormats.Rtf);
                    if (dlg.FilterIndex == 2) range.Load(stream, DataFormats.Text);
                }

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Save the current document.
        /// </summary>
        /// <returns></returns>
        public bool SaveDocument()
        {
            if (string.IsNullOrEmpty(_currentFile)) return SaveDocumentAs();
            else
            {
                using (Stream stream = new FileStream(_currentFile, FileMode.Create))
                {
                    TextRange range = new TextRange(_textBox.Document.ContentStart, _textBox.Document.ContentEnd);
                    if (_filterIndex == 1) range.Save(stream, DataFormats.Rtf);
                    if (_filterIndex == 2) range.Save(stream, DataFormats.Text);

                }

                return true;
            }
        }

        /// <summary>
        /// Show a save file dialog to save the currend document.
        /// </summary>
        /// <returns></returns>
        public bool SaveDocumentAs()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Text Files (*.txt)|*.txt|Rich Text Format (*.rtf)|*.rtf";

            if (dlg.ShowDialog() == true)
            {
                _currentFile = dlg.FileName;
                _filterIndex = dlg.FilterIndex;
                return SaveDocument();
            }

            return false;
        }

        /// <summary>
        /// Save the current document and show a print dialog.
        /// </summary>
        /// <returns></returns>
        public bool PrintDocument()
        {
            //create a print dialog object and set options.
            this.SaveDocument();

            PrintDialog pDialog = new PrintDialog();

            if (pDialog.ShowDialog() == true)
            {
                pDialog.PrintVisual(this._textBox as Visual, "Rich text box");
                return true;
            }

            return false;
        }

        /// <summary>
        /// Show a dialog asking if you want to save the current doccument.
        /// Then Close the current document.
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        //public bool CloseDocument(MainWindow window)
        //{
        //    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Do you want to save your existing document first?", "Save Confirmation", MessageBoxButton.YesNo);
        //    if (messageBoxResult == MessageBoxResult.Yes)
        //    {
        //        if (this.SaveDocument() == false)
        //        {
        //            window.Close();
        //        }
        //    }
        //    window.Close();

        //    return true;
        //}

        /// <summary>
        /// Undo the last modification.
        /// </summary>
        public void Undo_Click()
        {
            _textBox.Undo();
        }

        /// <summary>
        /// Redo the last modification.
        /// </summary>
        public void Redo_Click()
        {
            _textBox.Redo();
        }

        /// <summary>
        /// Cut the selected text.
        /// </summary>
        public void Cut_Click()
        {
            _textBox.Cut();
        }

        /// <summary>
        /// Copy the selected text.
        /// </summary>
        public void Copy_Click()
        {
            _textBox.Copy();
        }

        /// <summary>
        /// Paste the previously cut/copied text.
        /// </summary>
        public void Paste_Click()
        {
            _textBox.Paste();
        }

        /// <summary>
        /// Delete the contents of the document.
        /// </summary>
        public void Delete_Click()
        {
            _textBox.Document.Blocks.Clear();
        }

        /// <summary>
        /// Change de font weight of the selected text.
        /// </summary>
        public void Bold_Click()
        {
            _textBox.FontWeight = FontWeights.Bold;
        }

        /// <summary>
        /// Change the font style of the selected text to italic.
        /// </summary>
        public void Italic_Click()
        {
            _textBox.FontStyle = FontStyles.Italic;
        }

        /// <summary>
        /// Change a property of the selected text.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        public void ApplyToSelection(DependencyProperty property, object value)
        {
            if (value != null)
            {
                _textBox.Selection.ApplyPropertyValue(property, value);
            }
        }

    }
}
