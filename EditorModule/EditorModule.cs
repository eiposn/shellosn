﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using OSN;
using System.Configuration;
using EditorModule.Views;
using Microsoft.Practices.ServiceLocation;

namespace EditorModule
{
    //Name Module
    [Module(ModuleName = "J'écris", OnDemand = true)]
    public class EditorModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        /// <summary>
        /// You must load the views in order for the navigation to resolve
        /// </summary>
        /// <typeparam name="TViewType"></typeparam>
        /// <param name="regionName"></param>
        void LoadViewInRegion<TViewType>(string regionName)
        {
            IRegion region = regionManager.Regions[regionName];
            string viewName = typeof(TViewType).Name;

            object view = region.GetView(viewName);
            if (view == null)
            {
                 view = container.Resolve<TViewType>();

                region.Add(view, viewName);
            }
        }

        /// <summary>
        /// Initialize regions in order to load them
        /// </summary>
        public void Initialize()
        {

            string ribbonRegionName = ConfigurationManager.AppSettings["RibbonRegionName"];
            string workspaceRegionName = ConfigurationManager.AppSettings["WorkspaceRegionName"];

            LoadViewInRegion<EditorControl>("b");
            var moduleAView = new Uri("EditorControl", UriKind.Relative);
            regionManager.RequestNavigate("b", moduleAView);
        }


        /// <summary>
        /// Create the Editor Module and his container
        /// </summary>
        /// <param name="_regionManager"></param>
        /// <param name="_container"></param>
        /// <param name="_taskbar"></param>
        /// <param name="_catalog"></param>
        public EditorModule(IRegionManager _regionManager, IUnityContainer _container, TaskBarRegistry _taskbar, IModuleCatalog _catalog)
        {
            this.regionManager = _regionManager;
            this.container = _container;
            this.container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            container.RegisterType<Object, EditorControl>("EditorControl");
            IEnumerable<ModuleInfo> module = _catalog.Modules;
            string Imgpath = "";
            foreach (ModuleInfo cur in module)
            {
                if (cur.ModuleName == "J'écris")
                    Imgpath = cur.Ref;

            }

            Imgpath = System.IO.Path.ChangeExtension(Imgpath, "png");
            _taskbar.RegisterMenuItem("EditorControl", "J'écris", Imgpath);
        }
    }
}
