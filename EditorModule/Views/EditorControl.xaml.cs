﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EditorModule.Services;
using CommonModule.BaseTypes;

namespace EditorModule.Views
{
    /// <summary>
    /// Logique d'interaction pour EditorControl.xaml
    /// </summary>
    public partial class EditorControl : WorkspaceViewBase
    {
        private DocumentManager _documentmanager;

        public EditorControl()
        {
            InitializeComponent();
            _documentmanager = new DocumentManager(body);
            UserControl_Loaded();
            body.Focus();
        }

        /// <summary>
        /// Fill the combox boxes with values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UserControl_Loaded()
        {
            for (double i = 8; i < 49; i += 2)
            {
                fontSize.Items.Add(i);
            }
            fontSize.SelectedIndex = 2;
            fontColor.SelectedIndex = 0; 
            fontColor.Items.Add("White"); 
            fontColor.Items.Add("Black");
            fontColor.Items.Add("Red");
            fontColor.Items.Add("Orange");
            fontColor.Items.Add("Green");
            fontColor.Items.Add("Blue");
            fontColor.Items.Add("Yellow");
            fontColor.Items.Add("Purple");
        }

        /// <summary>
        /// Executed the function associated with the selected combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxChanged(object sender, RoutedEventArgs e)
        {
            ComboBox source = e.OriginalSource as ComboBox;

            if (source == null) return;

            switch (source.Name)
            {
                case "fonts": // Change the font family
                    _documentmanager.ApplyToSelection(TextBlock.FontFamilyProperty, source.SelectedItem);
                    break;

                case "fontSize": // Change the font size
                    _documentmanager.ApplyToSelection(TextBlock.FontSizeProperty, source.SelectedItem);
                    break;

                case "fontColor":
                    _documentmanager.ApplyToSelection(TextBlock.ForegroundProperty, source.SelectedItem);
                    break;
            }
            body.Focus();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            _documentmanager.NewDocument();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            _documentmanager.OpenDocument();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            _documentmanager.SaveDocument();
        }

        private void Cut_Click(object sender, RoutedEventArgs e)
        {
            _documentmanager.Cut_Click();
            body.Focus();
        }

        private void Copy_Click(object sender, RoutedEventArgs e)
        {
            _documentmanager.Copy_Click();
            body.Focus();
        }

        private void Paste_Click(object sender, RoutedEventArgs e)
        {
            _documentmanager.Paste_Click();
            body.Focus();
        }
        private void Print_Click(object sender, RoutedEventArgs e)
        {
            _documentmanager.PrintDocument();
        }
    }
}
